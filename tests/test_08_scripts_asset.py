from src.application import Application


def test_list_asset(config, db):
	kwargs = {
		"command": ["asset-list"]
	}
	result = Application.run(config["workdir"], **kwargs)
	assert result is None


def test_register_asset(config, db, symbol=None):
	symbol = symbol or "LTC"
	count = db.execute("SELECT count(*) FROM asset").fetchone()[0]
	kwargs = {
		"command": ["asset-register", symbol]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(*) FROM asset").fetchone()[0] == count + 1
	assert db.execute("SELECT count(*) FROM asset WHERE symbol='%s'" % symbol).fetchone()[0] == 1


def test_list_pair(config, db):
	kwargs = {
		"command": ["pair-list"]
	}
	result = Application.run(config["workdir"], **kwargs)
	assert result is None


def test_register_pair(config, db):
	test_register_asset(config, db, "LTC")
	test_register_asset(config, db, "XMR")

	assert db.execute("SELECT count(*) FROM pair").fetchone()[0] == 3

	kwargs = {
		"command": ["pair-register", "XMR", "LTC", "XMRLTC"]
	}
	Application.run(config["workdir"], **kwargs)

	assert db.execute("SELECT count(*) FROM pair").fetchone()[0] == 4

	kwargs = {
		"command": ["pair-register", "XMR", "BTC", "XMRBTC"]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(*) FROM pair").fetchone()[0] == 5

	kwargs = {
		"command": ["pair-register", "LTC", "BTC", "LTCBTC"]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(*) FROM pair").fetchone()[0] == 6


def test_disable_pair(config, db):
	assert db.execute("SELECT active FROM pair WHERE symbol='ETHBTC'").fetchone()[0] == True
	count = db.execute("SELECT count(*) FROM pair WHERE active=TRUE").fetchone()[0]

	kwargs = {
		"command": ["pair-disable", "ETHBTC"]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT active FROM pair WHERE symbol='ETHBTC'").fetchone()[0] == False
	assert db.execute("SELECT count(*) FROM pair WHERE active=TRUE").fetchone()[0] == count - 1


def test_enable_pair(config, db):
	test_register_pair(config, db)
	assert db.execute("SELECT active FROM pair WHERE symbol='XMRBTC'").fetchone()[0] == False
	count = db.execute("SELECT count(*) FROM pair WHERE active=TRUE").fetchone()[0]

	kwargs = {
		"command": ["pair-enable", "XMRBTC"]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT active FROM pair WHERE symbol='XMRBTC'").fetchone()[0] == True
	assert db.execute("SELECT count(*) FROM pair WHERE active=TRUE").fetchone()[0] == count + 1


def test_list_fund(config, db):
	kwargs = {
		"command": ["fund-list"]
	}
	result = Application.run(config["workdir"], **kwargs)
	assert result is None


def test_add_fund(config, db):
	count = db.execute("SELECT count(*) FROM fund").fetchone()[0]
	test_register_pair(config, db)
	kwargs = {
		"command": ["fund-add", "XMR", "binance", 4.3]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(*) FROM fund").fetchone()[0] == count + 1
	query = """
		SELECT asset.symbol, amount, active FROM fund
		LEFT JOIN asset ON asset.id=fund.asset_id
		WHERE asset.symbol='%s';
	"""
	fund = db.execute(query % "XMR").fetchone()
	assert fund[0] == "XMR"
	assert fund[1] == 4.3
	assert fund[2] == False


def test_disable_fund(config, db, skip_pair=False):
	count = db.execute("SELECT count(*) FROM fund WHERE active=TRUE").fetchone()[0]
	kwargs = {
		"command": ["fund-disable", 1]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT active FROM fund WHERE id=1").fetchone()[0] == False
	assert db.execute("SELECT count(*) FROM fund WHERE active=TRUE").fetchone()[0] == count - 1


def test_enable_fund(config, db):
	test_disable_fund(config, db)
	count = db.execute("SELECT count(*) FROM fund WHERE active=TRUE").fetchone()[0]
	assert db.execute("SELECT active FROM fund WHERE id=1").fetchone()[0] == False
	kwargs = {
		"command": ["fund-enable", 1]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT active FROM fund WHERE id=1").fetchone()[0] == True
	assert db.execute("SELECT count(*) FROM fund WHERE active=TRUE").fetchone()[0] == count + 1
