from datetime import datetime
from src.module import Pair


def test_pair_initialization(config, db, exchange):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)

	assert pair.symbol == symbol
	assert pair.base_asset == "ETH"
	assert pair.quote_asset == "BTC"

	assert pair.exchange["id"] == exchange.id
	assert pair.exchange["name"] == exchange.name
	assert pair.exchange["fee"] == exchange.fee


def test_pair_load(config, db, exchange, graph):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)

	assert pair.graph == graph


def test_pair_update(config, db, exchange, graph):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update({
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	})

	assert pair.graph != graph
	for interval in pair.graph:
		assert pair.graph[interval][-1]["close"] == close
		assert pair.graph[interval][-1]["high"] == close


def test_pair_candle(config, db, exchange, graph):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update({
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	})
	candle = pair.candle("1m")
	assert candle["close"] == close
	assert candle["high"] == close
	candle = pair.candle("1h")
	assert candle["close"] == close
	assert candle["high"] == close
	candle = pair.candle("1d")
	assert candle["close"] == close
	assert candle["high"] == close


def test_pair_candles(config, db, exchange, graph):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update({
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	})
	candles = pair.candles("1m")
	assert len(candles) == 100
	assert candles[-1]["close"] == close
	assert candles[-1]["high"] == close
	candles = pair.candles("1h")
	assert len(candles) == 100
	assert candles[-1]["close"] == close
	assert candles[-1]["high"] == close
	candles = pair.candles("1d")
	assert len(candles) == 100
	assert candles[-1]["close"] == close
	assert candles[-1]["high"] == close


def test_pair_interval(config, db, exchange, graph):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update({
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	})

	candles = pair.interval("1m")
	assert len(candles) == 1000
	assert candles[-1]["close"] == close
	assert candles[-1]["high"] == close
	candles = pair.interval("1h")
	assert len(candles) == 1000
	assert candles[-1]["close"] == close
	assert candles[-1]["high"] == close
	candles = pair.interval("1d")
	assert len(candles) == 1000
	assert candles[-1]["close"] == close
	assert candles[-1]["high"] == close


def test_pair_properties(config, db, exchange, graph):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update({
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	})
	assert pair.price == close
	assert pair.time == datetime.fromtimestamp(1601216773619 / 1000)
