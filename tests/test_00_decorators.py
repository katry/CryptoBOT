from src.lib.decorators import classproperty, hybridproperty, hybridmethod


class TClass:
	__cla_prop_ro = 8
	__hy_prop_ro = 10

	def __init__(self):
		self.__hy_prop_ro = 5
		self.__cla_prop_ro = 4

	@hybridmethod
	def both_callable(target):
		return "BOTH", target

	@classmethod
	def cls_callable(cls):
		return "CLS", cls

	def ins_callable(self):
		return "INS", self

	@classproperty
	def cla_prop_ro(cls):
		return cls.__cla_prop_ro

	@hybridproperty
	def hy_prop_ro(target):
		return target.__hy_prop_ro


def test_hybrid_method():
	assert TClass.both_callable() == ("BOTH", TClass)
	assert TClass.cls_callable() == ("CLS", TClass)
	try:
		TClass.ins_callable()
		err = None
	except Exception as e:
		err = e
	assert type(err) == TypeError

	ins = TClass()
	assert ins.both_callable() == ("BOTH", ins)
	assert ins.ins_callable() == ("INS", ins)
	assert ins.cls_callable() == ("CLS", TClass)


def test_class_property():
	assert TClass.cla_prop_ro == 8
	ins = TClass()
	assert ins.cla_prop_ro == 8


def test_hybrid_property():
	assert TClass.hy_prop_ro == 10

	ins = TClass()
	assert ins.hy_prop_ro == 5
	assert TClass.hy_prop_ro == 10


def test_setter():
	try:
		class FCSClass:
			__cla_prop_rw = 3

			@classproperty
			def cla_prop_rw(cls):
				return cls.__cla_prop_rw

			@cla_prop_rw.setter
			def cla_prop_rw(cls, v):
				cls.__cla_prop_rw = v

		err = None
	except Exception as e:
		err = e

	assert type(err) == NotImplementedError

	try:
		class FCDClass:
			__cla_prop_rw = 3

			@classproperty
			def cla_prop_rw(cls):
				return cls.__cla_prop_rw

			@cla_prop_rw.deleter
			def cla_prop_rw(cls):
				cls.__cla_prop_rw = None

		err = None
	except Exception as e:
		err = e

	assert type(err) == NotImplementedError

	try:
		class FHSClass:
			__hy_prop_rw = 3

			@hybridproperty
			def hy_prop_rw(cls):
				return cls.__hy_prop_rw

			@hy_prop_rw.setter
			def hy_prop_rw(cls, v):
				cls.__hy_prop_rw = v

		err = None
	except Exception as e:
		err = e

	assert type(err) == NotImplementedError

	try:
		class FHDClass:
			__hy_prop_rw = 3

			@hybridproperty
			def hy_prop_rw(cls):
				return cls.__hy_prop_rw

			@hy_prop_rw.deleter
			def hy_prop_rw(cls):
				cls.__hy_prop_rw = None

		err = None
	except Exception as e:
		err = e

	assert type(err) == NotImplementedError
