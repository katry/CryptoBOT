from datetime import datetime
from multiprocessing import Queue
from src.module import Pair, Position
from src.model import (
	Asset as AssetModel,
	Fund as FundModel,
	PositionType,
	PositionStatus
)


def prepare(db, exchange, type=PositionType.LONG, price=0.1):
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	fund = (
		FundModel.query
		.join(AssetModel)
		.filter(FundModel.locked == False)
		.filter(AssetModel.symbol == "BTC")
		.first()
	)
	raw = {
		"type": type,
		"amount": fund.amount,
		"price": price,
		"pair_id": pair.id,
		"fund_id": fund.id,
		"exchange_id": exchange.id,
	}
	return raw, pair, fund.id


def test_position_initialization(config, db, exchange, strategy):
	raw, pair, fund = prepare(db, exchange)
	s = {
		"model": strategy,
		"runner": None
	}
	position = Position(config, raw, pair, fund, exchange, s, Queue())

	assert position.open_price == raw["price"]
	assert position.status == PositionStatus.PENDING


def test_position_dict(config, db, exchange, strategy):
	raw, pair, fund = prepare(db, exchange)
	s = {
		"model": strategy,
		"runner": None
	}
	position = Position(config, raw, pair, fund, exchange, s, Queue())

	dct = dict(position)
	assert dct["amount"] == 1.0
	assert dct["type"] == PositionType.LONG
	assert dct["status"] == PositionStatus.PENDING
	assert dct["pair_id"] == 1


def test_position_open(config, db, exchange, strategy):
	receiver = Queue()
	buffer = []
	raw, pair, fund = prepare(db, exchange)
	s = {
		"model": strategy,
		"runner": None
	}
	position = Position(config, raw, pair, fund, exchange, s, receiver, buffer)

	response = receiver.get(timeout=1)
	assert response["action"] == "create-order"
	assert response["order"]["symbol"] == "ETHBTC"
	assert response["order"]["price"] == 0.1
	assert response["order"]["amount"] == 10.0
	assert response["order"]["side"] == "BUY"
	assert response["exchange"] == "binance"

	buffer.append({"status": PositionStatus.OPEN})
	position.refresh()

	assert position.status == PositionStatus.OPEN
	query = "SELECT status FROM position WHERE id=%s" % position.id
	assert int(PositionStatus.OPEN) == db.execute(query).fetchone()[0]


def test_position_close(config, db, exchange, strategy):
	receiver = Queue()
	buffer = []
	raw, pair, fund = prepare(db, exchange)
	position = Position(config, raw, pair, fund, exchange, strategy, receiver, buffer)

	receiver.get(timeout=1)
	buffer.append({"status": PositionStatus.OPEN})
	position.refresh()
	assert position.status == PositionStatus.OPEN

	position.close({"price": 0.2})
	receiver.get(timeout=1)
	buffer.append({"status": PositionStatus.CLOSED})
	position.refresh()
	assert position.status == PositionStatus.CLOSED


def test_position_properties(config, db, exchange, strategy):
	raw, pair, fund = prepare(db, exchange)
	position = Position(config, raw, pair, fund, exchange, strategy, Queue())

	assert type(position.open_time) == datetime
	assert position.close_time is None
	assert position.close_price is None
	assert position.skip == False
	assert position.order_id is None
	assert position.strategy == strategy
