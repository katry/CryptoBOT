from os import makedirs
from src.application import Application


def test_list_indicator(config, db):
	kwargs = {
		"command": ["indicator-list"]
	}
	result = Application.run(config["workdir"], **kwargs)
	assert result is None


def test_register_indicator(config, db):
	name = "name-of-indicator"
	version = "1.2.3"
	kwargs = {
		"command": ["indicator-register", name, version]
	}
	indicators_count_query = "SELECT count(*) FROM indicator;"
	indicator_count = db.execute(indicators_count_query).fetchone()[0]
	makedirs("%s/indicators/%s/v%s" % (config["workdir"], name, version.replace(".", "_")))
	indicator_file = open(
		"%s/indicators/%s/v%s/indicator.py" % (config["workdir"], name, version.replace(".", "_")),
		"w"
	)
	indicator_file.write("\n")
	indicator_file.close()
	Application.run(config["workdir"], **kwargs)
	assert db.execute(indicators_count_query).fetchone()[0] == indicator_count + 1
	indicator = db.execute("SELECT name, version FROM indicator WHERE name='%s'" % name).fetchone()
	assert indicator[0] == name
	assert indicator[1] == version
