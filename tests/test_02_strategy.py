from multiprocessing import Queue
from logging import getLogger
from src.module import Loader, Strategy, Pair, Position
from .test_02_position import prepare as prepare_position


def load(config, strategy, container):
	logger = getLogger()
	Loader.init(config["workdir"], config["allowed_modules"], logger)
	try:
		s = Strategy.load(
			strategy,
			Loader.strategy(strategy.name, strategy.version)(),
			logger
		)
	except Exception as e:
		print("Invalid strategy '%s - %s' (%s)" % (strategy.name, strategy.version, strategy.id))
		print("Exception:", e)
		s = None
	if s:
		container.append(s)


def candle(close=0.123):
	return {
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	}


def test_strategy_load(config, db, strategy):
	container = []
	load(config, strategy, container)
	assert len(container) == 1
	assert callable(container[0].check_open_wrapper) == True
	assert callable(container[0].check_close_wrapper) == True
	assert isinstance(container[0], Strategy)


def test_strategy_load_invalid(config, db, strategy_invalid):
	container = []
	load(config, strategy_invalid, container)
	assert len(container) == 0


def test_strategy_open(config, db, strategy, graph, exchange):
	container = []
	load(config, strategy, container)
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update(candle(close))
	result = container[0].check_open_wrapper(pair)
	assert result == False
	result = container[0].check_open_wrapper(pair)
	assert type(result) == dict
	assert result["price"] == 0.121
	assert result["type"] == "long"


def test_strategy_close(config, db, strategy, graph, exchange):
	container = []
	load(config, strategy, container)
	symbol = "ETHBTC"
	pair = Pair(symbol, exchange)
	pair.load(graph)
	close = 0.123
	pair.update(candle(close))
	raw, pair, fund = prepare_position(db, exchange)
	s = {
		"model": strategy,
		"runner": None
	}
	position = Position(config, raw, pair, fund, exchange, s, Queue())
	result = container[0].check_close_wrapper(pair, position)
	assert result == False
	result = container[0].check_close_wrapper(pair, position)
	assert type(result) == dict
	assert result["price"] == 0.131
