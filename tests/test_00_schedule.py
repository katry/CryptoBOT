import time
from datetime import datetime, timedelta
from uuid import UUID
import pytest

# import mock
from freezegun import freeze_time

from src.lib.schedule import Schedule


@pytest.fixture
def clear_schedule():
	yield None
	Schedule.clear()


def test_delay(clear_schedule):
	def callback():
		assert True

	delay = 1
	uuid = Schedule.delay(delay, callback)
	assert isinstance(uuid, UUID)
	time.sleep(delay + 0.1)  # Wait for the scheduler to execute the callback
	assert uuid not in Schedule._Schedule__queue  # Ensure the callback has been removed from the queue


def test_clear():
	def callback():
		assert True

	delay = 1
	Schedule.delay(delay, callback)
	assert len(Schedule._Schedule__queue) == 1
	Schedule.clear()
	assert len(Schedule._Schedule__queue) == 0


def test_at(clear_schedule):
	callback_called = {
		"return_value": False
	}

	def callback():
		callback_called["return_value"] = True

	target_time = datetime.now() + timedelta(seconds=1)
	uuid = Schedule.at(target_time, callback)
	assert isinstance(uuid, UUID)
	assert callback_called["return_value"] is False
	time.sleep(1.1)  # Wait for the scheduler to execute the callback
	assert uuid not in Schedule._Schedule__queue  # Ensure the callback has been removed from the queue
	assert callback_called["return_value"] is True


def test_cancel():
	def callback():
		assert True

	delay = 1
	uuid = Schedule.delay(delay, callback)
	assert len(Schedule._Schedule__queue) == 1
	Schedule.cancel(uuid)
	assert len(Schedule._Schedule__queue) == 0
	try:
		Schedule.cancel(uuid)
		assert False  # Should not reach this line
	except ReferenceError:
		assert True


@freeze_time("2022-01-01 01:00:00")
def test_every(clear_schedule):
	callback_called = {
		"return_value": 0
	}

	def callback():
		callback_called["return_value"] += 1

	target_kwargs = {"second": 1}
	uuid = Schedule.every(callback, **target_kwargs)

	assert callback_called["return_value"] == 0
	time.sleep(1.1)
	assert callback_called["return_value"] == 1
	time.sleep(1.1)
	assert callback_called["return_value"] == 2
	time.sleep(1.1)
	assert callback_called["return_value"] == 3
	Schedule.cancel(uuid)
	time.sleep(2.1)
	assert callback_called["return_value"] == 3
