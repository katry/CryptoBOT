from os import path
from json import loads, dumps
from src.application import Application
from .test_08_scripts_indicator import test_register_indicator
from .test_08_scripts_strategy import test_register_strategy

f = open(path.realpath(__file__).replace("test_08_scripts_settings.py", "data/exported_data.json"), "r")
exported_data = loads(f.read())
f.close()


def test_export_settings(config, db):
	test_register_indicator(config, db)
	test_register_strategy(config, db)

	kwargs = {
		"command": ["settings-export", "%s/backup-test.json" % config["workdir"]]
	}
	Application.run(config["workdir"], **kwargs)

	f = open("%s/backup-test.json" % config["workdir"])
	# print("expd", dumps(exported_data))
	dummy = loads(f.read())
	assert dummy["exchanges"] == exported_data["exchanges"]
	assert dummy["strategies"] == exported_data["strategies"]
	assert dummy["assets"] == exported_data["assets"]
	assert dummy["pairs"] == exported_data["pairs"]
	assert dummy["funds"] == exported_data["funds"]
	f.close()


def test_import_settings(config, empty_db):
	f = open("%s/backup-test.json" % config["workdir"], "w")
	f.write(dumps(exported_data))
	f.close()

	indicator_count = empty_db.execute("SELECT count(*) FROM indicator").fetchone()[0]

	assert empty_db.execute("SELECT count(*) FROM exchange").fetchone()[0] == 1
	assert empty_db.execute("SELECT count(*) FROM asset").fetchone()[0] == 0
	assert empty_db.execute("SELECT count(*) FROM pair").fetchone()[0] == 0
	assert empty_db.execute("SELECT count(*) FROM fund").fetchone()[0] == 0
	assert empty_db.execute("SELECT count(*) FROM strategy").fetchone()[0] == 0

	kwargs = {
		"command": ["settings-import", "%s/backup-test.json" % config["workdir"]]
	}
	Application.run(config["workdir"], **kwargs)

	assert empty_db.execute("SELECT count(*) FROM exchange").fetchone()[0] == len(exported_data["exchanges"])
	assert empty_db.execute("SELECT count(*) FROM asset").fetchone()[0] == len(exported_data["assets"])
	assert empty_db.execute("SELECT count(*) FROM pair").fetchone()[0] == len(exported_data["pairs"])
	assert empty_db.execute("SELECT count(*) FROM fund").fetchone()[0] == len(exported_data["funds"])
	assert empty_db.execute("SELECT count(*) FROM indicator").fetchone()[0] == len(exported_data["indicators"]) + indicator_count
	assert empty_db.execute("SELECT count(*) FROM strategy").fetchone()[0] == len(exported_data["strategies"])

	assert empty_db.execute("SELECT name FROM exchange").fetchone()[0] == "binance"
