from os import path, mkdir
from json import loads
from shutil import rmtree
import sqlite3
from alembic import command
from alembic.config import Config
import mock

from src import __version__
from src.system import System


workdir = "/tmp/cb-install-test"


def prepare():
	if path.isdir(workdir):
		rmtree(workdir)
	mkdir(workdir)


def clear():
	rmtree(workdir)


def test_migrations():
	prepare()

	alembic_cfg = Config()
	pth = path.realpath(__file__).replace("/tests/test_01_install.py", "/src/migrations")
	alembic_cfg.set_main_option("sqlalchemy.url", "sqlite:///%s/data.db" % workdir)
	alembic_cfg.set_main_option("script_location", pth)
	command.upgrade(alembic_cfg, 'head')
	file = open("%s/.CryptoBOT.ver" % workdir, "w")
	file.write(__version__)
	file.close()

	connection = sqlite3.connect("/%s/data.db" % workdir)
	c = connection.cursor()
	indicators = c.execute("""
		SELECT id, name, version FROM indicator
	""").fetchall()
	exchanges = c.execute("""
		SELECT name, fee FROM exchange
	""").fetchall()
	assert len(indicators) == 5
	assert len(exchanges) == 1
	connection.commit()
	connection.close()

	command.downgrade(alembic_cfg, 'base')
	clear()


def test_system_init():
	prepare()

	answers = [
		"2",
		"y",
		"0.0.0.0",
		"6666",
		"abcdefgABCDEFG",
		"1234567890asdf",
		"n",
		"",
	]

	def mock_input(prompt, *args, **kwargs):
		return answers.pop(0)

	with mock.patch('builtins.input', mock_input):
		with mock.patch('os.get_terminal_size', mock.Mock(return_value=[10, 10])):
			System(["init"], "", "head", workdir, local=True)

			fd = open("%s/config/robot.json" % workdir)
			config = loads(fd.read())
			fd.close()
			assert config == {
				'debug': False,
				'autostart': True,
				'workdir': '/tmp/cb-install-test',
				'allowed_modules': ['urllib'],
				'type': 'notifier',
				'server': {
					'host': '0.0.0.0',
					'port': 6666,
					'secret': 'abcdefgABCDEFG',
					'client_secrets': ['1234567890asdf']
				}
			}

	clear()


# def test_system_update():
# 	prepare()
# 	# TODO
# 	clear()
