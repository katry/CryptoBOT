from os import makedirs
from src.application import Application


def test_list_strategy(config, db):
	kwargs = {
		"command": ["strategy-list"]
	}
	result = Application.run(config["workdir"], **kwargs)
	assert result is None


def test_register_strategy(config, db, name=None, version=None):
	name = name or "name-of-strategy"
	version = version or "3.2.1"
	kwargs = {
		"command": ["strategy-register", name, version]
	}
	strategies_count_query = "SELECT count(*) FROM strategy;"
	count = db.execute(strategies_count_query).fetchone()[0]
	try:
		makedirs("%s/strategies/%s/v%s" % (config["workdir"], name, version.replace(".", "_")))
	except Exception:
		pass
	strategy_file = open(
		"%s/strategies/%s/v%s/strategy.py" % (config["workdir"], name, version.replace(".", "_")),
		"w"
	)
	strategy_file.write("\n")
	strategy_file.close()
	Application.run(config["workdir"], **kwargs)
	assert db.execute(strategies_count_query).fetchone()[0] == count + 1
	count = db.execute("SELECT count(*) FROM strategy WHERE name='%s' AND version='%s'" % (name, version)).fetchone()[0]
	assert count == 1


def test_change_status_strategy(config, db):
	name = "name-of-strategy"
	version_1 = "3.2.1"
	version_2 = "3.2.2"

	test_register_strategy(config, db, name, version_1)
	test_register_strategy(config, db, name, version_2)

	kwargs = {
		"command": ["strategy-enable", name]
	}
	Application.run(config["workdir"], **kwargs)
	av = db.execute("SELECT version FROM strategy WHERE name='%s' AND active=TRUE" % name).fetchone()[0]
	assert av == version_2

	kwargs = {
		"command": ["strategy-enable", name, version_1]
	}
	Application.run(config["workdir"], **kwargs)
	av = db.execute("SELECT version FROM strategy WHERE name='%s' AND active=TRUE" % name).fetchone()[0]
	assert av == version_1
	kwargs = {
		"command": ["strategy-disable", name, version_1]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(*) FROM strategy WHERE name='%s' AND active=TRUE" % name).fetchone()[0] == 0

	kwargs = {
		"command": ["strategy-enable", name, version_2]
	}
	Application.run(config["workdir"], **kwargs)
	av = db.execute("SELECT version FROM strategy WHERE name='%s' AND active=TRUE" % name).fetchone()[0]
	assert av == version_2
	kwargs = {
		"command": ["strategy-disable", name]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(*) FROM strategy WHERE name='%s' AND active=TRUE" % name).fetchone()[0] == 0
