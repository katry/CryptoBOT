class Strategy:
	_counter = 0

	@classmethod
	def check_open(cls, pair):
		cls._counter += 1
		if cls._counter % 2 == 0:
			return {
				"type": "long",
				"price": 0.121
			}
		else:
			return False

	@classmethod
	def check_close(cls, pair, position):
		cls._counter += 1
		if cls._counter % 2 == 0:
			return {
				"price": 0.131
			}
		else:
			return False
