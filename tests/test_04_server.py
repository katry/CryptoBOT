from os import makedirs
from datetime import datetime
from time import sleep
from socket import socket, AF_UNIX, AF_INET, SOCK_STREAM
from multiprocessing import Queue
from threading import Thread
from weesocket import Client
from weesocket.abstract import Socket, logger
from src.module.server import Server


def init_server(config):
	sender = Queue()
	receiver = Queue()
	process = Thread(
		target=Server,
		args=(config, sender, receiver),
		daemon=True
	)
	process.start()
	return process, sender, receiver


def init_server_not_initialized(config):
	sender = Queue()
	receiver = Queue()
	process = Thread(
		target=Server,
		args=(config, sender, receiver, False),
		daemon=True
	)
	process.start()
	return process, sender, receiver


class TClient(Client):
	def start(self):
		self.__enabled = True
		if self._file:
			self._socket = socket(AF_UNIX, SOCK_STREAM)
			self._socket.connect(self._file)
		else:
			self._socket = socket(AF_INET, SOCK_STREAM)
			self._socket.connect((self._host, self._port))
		self._socket.settimeout(5)
		self._thread = Thread(target=self.__on_message, args=())
		self._thread.start()

	def __on_message(self):
		data = None
		bytes = b""
		while self.__enabled:
			try:
				bytes += self._socket.recv(1024)
			except Exception:
				logger.warning("Connection reset by peer - client")
				break
			if not bytes:
				break
			data, bytes = Socket._process_stream(bytes)
			secret = self._server_secret and [self._server_secret]
			for item in data:
				json = Socket._load_data(item, self._key)
				self._process_data(json, None, secret)
				data = None
		self._socket.close()


def client(trigger, name="test", addr="127.0.0.1", port=6666, secret="BCA", server_secret="ABC"):
	c = TClient(
		alias=name, host=addr, port=port,
		secret=secret, server_secret=server_secret,
		trigger=trigger
	)
	return c


def test_server_init(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	msg = receiver.get(timeout=30)
	assert type(msg) == dict
	assert msg == {"action": "ready", "target": None}
	sender.put({"action": "stop"})
	msg = receiver.get(timeout=30)


def test_server_stop(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)

	sender.put({"action": "status"})
	msg = receiver.get(timeout=30)
	assert msg == {"action": "status", "status": "running", "target": None}

	sender.put({"action": "stop"})
	msg = receiver.get(timeout=30)
	assert msg == {"action": "stopped", "target": None}



def test_server_send(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)
	called = []

	def trigger(data):
		assert data["data"] == {"test": "vole"}
		called.append(1)

	c = client(trigger)
	sleep(3)
	sender.put({
		"action": "send",
		"type": "pivo",
		"client": c._name,
		"payload": {"test": "vole"}
	})
	sleep(3)
	sender.put({"action": "stop"})

	assert len(called) > 0


def test_server_connect_not_initialized(server_config, db, exchange):
	server_process, receiver, sender = init_server_not_initialized(server_config)
	called = []
	sleep(3)

	def trigger(data):
		called.append(1)

	refused = False
	try:
		client(trigger)
	except ConnectionRefusedError:
		refused = True

	assert refused == True


def test_server_send_not_initialized(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	sleep(3)
	sender.put({
		"action": "send",
		"type": "pivo",
		"client": "test",
		"payload": {"test": "vole"}
	})
	sleep(3)
	sender.put({"action": "stop"})


def test_server_send_invalid_request(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)
	called = []

	def trigger(data):
		called.append(1)

	c = client(trigger)
	sleep(3)
	sender.put({
		"action": "send",
		"client": c._name,
		"payload": {"test": "vole"}
	})
	sleep(3)
	sender.put({"action": "stop"})
	assert len(called) == 0


def test_server_send_invalid_data(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)
	called = []

	def trigger(data):
		called.append(1)

	c = client(trigger)
	sleep(3)
	sender.put({
		"action": "send",
		"type": "pivo",
		"client": c._name,
	})
	sleep(3)
	sender.put({"action": "stop"})
	assert len(called) == 0


def test_server_general_actions(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)

	def trigger(data):
		pass

	c = client(trigger)
	sleep(3)

	timestamp = datetime.utcnow().isoformat()
	c.send({"type": "ping", "timestamp": timestamp})

	msg = receiver.get(timeout=30)
	assert timestamp == msg["timestamp"]
	assert "ping" == msg["action"]

	sender.put({"action": "stop"})


def test_server_cli_actions(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)

	count = db.execute(
		"SELECT count(id) FROM strategy WHERE name='%s' AND version='%s'"
		% ("tester_strategy", "1.0.0")
	).fetchall()
	assert 0 == count[0][0]

	pth = "%s/strategies/tester_strategy/v1_0_0/" % server_config["workdir"]
	makedirs(pth)
	f = open("%s/strategy.py" % pth, "w")
	f.write("print(1)\n")
	f.close()

	def trigger(data):
		pass

	c = client(trigger)
	sleep(3)
	c.send({
		"type": "strategy-register",
		"argument_list": [
			"tester_strategy", "1.0.0"
		]
	})
	sleep(3)
	sender.put({"action": "stop"})

	count = db.execute(
		"SELECT count(id) FROM strategy WHERE name='%s' AND version='%s'"
		% ("tester_strategy", "1.0.0")
	).fetchall()
	assert 1 == count[0][0]


def test_server_invalid_action(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)

	def trigger(data):
		pass

	c = client(trigger)
	sleep(3)
	c.send({
		"type": "invalid-type",
	})
	sleep(3)

	sender.put({"action": "stop"})


def test_server_broadcast(server_config, db, exchange):
	server_process, receiver, sender = init_server(server_config)
	receiver.get(timeout=30)
	called = []

	def trigger(data):
		assert data["data"] == {"test": "vole"}
		called.append(1)

	client(trigger)
	sleep(3)
	sender.put({
		"action": "broadcast",
		"type": "pivo",
		"payload": {"test": "vole"}
	})
	sleep(3)
	sender.put({"action": "stop"})

	assert len(called) > 0


def test_server_broadcast_not_initialized(server_config, db, exchange):
	server_process, receiver, sender = init_server_not_initialized(server_config)
	sleep(3)
	sender.put({
		"action": "broadcast",
		"type": "pivo",
		"client": "test",
		"payload": {"test": "vole"}
	})
	sleep(3)
	sender.put({"action": "stop"})
