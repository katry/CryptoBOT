from multiprocessing import Queue
from src.module import Manager


def test_request_funds_valid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	count_query = "SELECT count(id) AS c FROM fund WHERE locked=FALSE AND active=TRUE"
	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "request-fund", "asset": "BTC", "id": 1343, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-accept"
	assert "fund" in response
	assert type(response["fund"]["id"]) == int
	assert response["fund"]["id"] > 0
	assert type(response["fund"]["amount"]) == float
	assert response["fund"]["amount"] > 0
	assert response["fund"]["asset"] == "BTC"
	assert "id" in response
	assert response["id"] == 1343
	assert db.execute(count_query).fetchone()[0] == 5

	sender.put({"action": "request-fund", "asset": "BTC", "id": 1456, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-accept"
	assert "fund" in response
	assert type(response["fund"]["id"]) == int
	assert response["fund"]["id"] > 0
	assert type(response["fund"]["amount"]) == float
	assert response["fund"]["amount"] > 0
	assert response["fund"]["asset"] == "BTC"
	assert "id" in response
	assert response["id"] == 1456
	assert db.execute(count_query).fetchone()[0] == 4

	sender.put({"action": "request-fund", "asset": "BTC", "id": 1111, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-reject"
	assert db.execute(count_query).fetchone()[0] == 4

	sender.put({"action": "request-fund", "asset": "BNB", "id": 2456, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-accept"
	assert "fund" in response
	assert type(response["fund"]["id"]) == int
	assert response["fund"]["id"] > 0
	assert type(response["fund"]["amount"]) == float
	assert response["fund"]["amount"] > 0
	assert response["fund"]["asset"] == "BNB"
	assert "id" in response
	assert response["id"] == 2456
	assert db.execute(count_query).fetchone()[0] == 3

	sender.put({"action": "request-fund", "asset": "ETH", "id": 3456, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-accept"
	assert "fund" in response
	assert type(response["fund"]["id"]) == int
	assert response["fund"]["id"] > 0
	assert type(response["fund"]["amount"]) == float
	assert response["fund"]["amount"] > 0
	assert response["fund"]["asset"] == "ETH"
	assert "id" in response
	assert response["id"] == 3456
	assert db.execute(count_query).fetchone()[0] == 2

	sender.put({"action": "stop"})


def test_request_funds_invalid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	count_query = "SELECT count(id) AS c FROM fund WHERE locked=FALSE AND active=TRUE"

	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "request-fund", "asset": "BTC", "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-reject"
	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "request-fund", "id": 2111, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-reject"
	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "request-fund", "sender": "test", "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-request-reject"
	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "stop"})


def test_return_funds_valid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	count_query = "SELECT count(id) AS c FROM fund WHERE locked=FALSE AND active=TRUE"

	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "request-fund", "asset": "BTC", "id": 1343, "sender": "test"})
	response = receiver.get(timeout=10)
	assert db.execute(count_query).fetchone()[0] == 5
	new_amount = response["fund"]["amount"] + 1
	fund_id = response["fund"]["id"]
	asset = response["fund"]["asset"]
	sender.put({"action": "return-fund", "id": 123, "sender": "test", "fund": {
		"id": fund_id,
		"amount": new_amount,
		"asset": asset
	}})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-return-accept"
	assert response["id"] == 123
	assert db.execute(count_query).fetchone()[0] == 6
	u_query = "SELECT amount FROM fund WHERE id=%s"
	assert db.execute(u_query % fund_id).fetchone()[0] == new_amount

	sender.put({"action": "return-fund", "id": 124, "sender": "test", "fund": {
		"id": fund_id,
		"amount": new_amount,
		"asset": asset
	}})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-return-reject"
	assert response["id"] == 124
	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "stop"})


def test_return_funds_invalid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	count_query = "SELECT count(id) AS c FROM fund WHERE locked=FALSE AND active=TRUE"

	assert db.execute(count_query).fetchone()[0] == 6

	sender.put({"action": "request-fund", "asset": "BTC", "id": 1343, "sender": "test"})
	response = receiver.get(timeout=10)
	new_amount = response["fund"]["amount"]
	fund_id = response["fund"]["id"]
	asset = response["fund"]["asset"]
	assert db.execute(count_query).fetchone()[0] == 5

	sender.put({"action": "return-fund", "sender": "test", "fund": {
		"id": fund_id,
		"amount": new_amount,
		"asset": asset
	}})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-return-reject"
	assert db.execute(count_query).fetchone()[0] == 5

	sender.put({"action": "return-fund", "id": 124, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-return-reject"
	assert db.execute(count_query).fetchone()[0] == 5

	sender.put({"action": "return-fund", "id": 124, "sender": "test", "fund": {
		"id": fund_id + 1000,
		"amount": new_amount,
		"asset": asset
	}})
	response = receiver.get(timeout=10)
	assert response["action"] == "funds-return-reject"
	assert db.execute(count_query).fetchone()[0] == 5

	sender.put({"action": "stop"})


def test_init_test_valid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	status_query = "SELECT status FROM test WHERE id=1"
	assert db.execute(status_query).fetchone()[0] == 0

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	sender.put({"action": "test-init", "id": 1, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "test-accept"
	assert db.execute(status_query).fetchone()[0] == 1


def test_init_test_invalid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	status_query = "SELECT status FROM test WHERE id=1"
	assert db.execute(status_query).fetchone()[0] == 0

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	sender.put({"action": "test-init", "id": 1000, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "test-reject"

	sender.put({"action": "test-init", "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "test-reject"
	assert db.execute(status_query).fetchone()[0] == 0


def test_finish_test_valid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	status_query = "SELECT status FROM test WHERE id=1"
	assert db.execute(status_query).fetchone()[0] == 0

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	sender.put({"action": "test-init", "id": 1, "sender": "test"})
	response = receiver.get(timeout=10)
	assert db.execute(status_query).fetchone()[0] == 1
	sender.put({"action": "test-finish", "id": 1, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "test-finish-accept"
	assert db.execute(status_query).fetchone()[0] == 2


def test_finish_test_invalid(config, db, process):
	sender = Queue()
	receiver = Queue()
	process(target=Manager, args=(config, receiver, sender))

	status_query = "SELECT status FROM test WHERE id=1"
	assert db.execute(status_query).fetchone()[0] == 0

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"

	sender.put({"action": "test-finish", "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "test-finish-reject"
	assert db.execute(status_query).fetchone()[0] == 0

	sender.put({"action": "test-finish", "id": 1, "sender": "test"})
	response = receiver.get(timeout=10)
	assert response["action"] == "test-finish-reject"
	assert db.execute(status_query).fetchone()[0] == 0
