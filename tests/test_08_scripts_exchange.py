from src.application import Application
from json import loads, dumps


def test_list_exchange(config, db):
	kwargs = {
		"command": ["exchange-list"]
	}
	result = Application.run(config["workdir"], **kwargs)
	assert result is None


def test_update_fee_exchange(config, db):
	kwargs = {
		"command": ["exchange-set-fee", "binance", "0.321"]
	}
	fee_query = "SELECT fee FROM exchange WHERE name='binance'"
	assert db.execute(fee_query).fetchone()[0] == 0.1
	Application.run(config["workdir"], **kwargs)
	assert db.execute(fee_query).fetchone()[0] == 0.321


def test_update_api_exchange(config, db):
	kwargs = {
		"command": ["exchange-set-api", "binance", dumps({"sec": "JOO", "pub": "NEE"})]
	}
	api_query = "SELECT api FROM exchange WHERE name='binance'"
	assert db.execute(api_query).fetchone()[0] is None
	Application.run(config["workdir"], **kwargs)
	assert loads(db.execute(api_query).fetchone()[0]) == {"sec": "JOO", "pub": "NEE"}


def test_change_status_exchange(config, db):
	kwargs = {
		"command": ["exchange-disable", "binance"]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(id) FROM exchange WHERE active=TRUE").fetchone()[0] == 0
	assert db.execute("SELECT count(id) FROM exchange WHERE active=FALSE").fetchone()[0] == 1
	kwargs = {
		"command": ["exchange-enable", "binance"]
	}
	Application.run(config["workdir"], **kwargs)
	assert db.execute("SELECT count(id) FROM exchange WHERE active=TRUE").fetchone()[0] == 1
	assert db.execute("SELECT count(id) FROM exchange WHERE active=FALSE").fetchone()[0] == 0
