from time import sleep
from datetime import datetime
from multiprocessing import Queue
from src.module import Trader
from src.model import Fund as FundModel, Asset as AssetModel, PositionStatus, PositionType


def candle(close=0.123):
	return {
		"time": 1601216773619,
		"open_time": 1601216760000,
		"close_time": 1601216819999,
		"open": 0.03294800,
		"close": close,
		"high": close,
		"low": 0.03291500,
		"volume": 204.74900000,
		"closed": False
	}


def init(config, process, exchange, symbol):
	sender = Queue()
	receiver = Queue()
	process(target=Trader, args=(config, symbol, [exchange.id], receiver, sender))
	return sender, receiver, process


def test_trader_init(config, db, process, exchange):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)

	response = receiver.get(timeout=10)
	assert response["action"] == "ready"
	assert response["symbol"] == "ETHBTC"


def test_trader_graph(config, db, process, exchange, graph):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)

	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	assert response["action"] == "loaded"
	assert response["symbol"] == "ETHBTC"


def test_trader_candle(config, db, process, exchange, graph, mocker):
	trader_process = Trader._process

	def fake_process(self, data):
		trader_process(self, data)
		self._send({
			"action": "candle-response",
			"candle": data["candle"],
			"graph": self._pair[data["exchange_id"]].graph
		})

	mocker.patch('src.module.trader.Trader._process', fake_process)
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	sender.put({
		"action": "candle",
		"candle": candle(0.123),
		"exchange_id": exchange.id
	})
	response = receiver.get(timeout=10)
	assert response["action"] == "candle-response"
	test_candle = candle(0.123)
	test_candle.pop("closed")
	assert response["candle"] == test_candle
	for interval in response["graph"]:
		assert response["graph"][interval][-1]["close"] == 0.123
		assert response["graph"][interval][-1]["high"] == 0.123


def test_trader_lock(config, db, process, exchange, graph):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	sender.put({"action": "lock"})
	response = receiver.get(timeout=10)
	assert response["action"] == "locked"
	sender.put({"action": "lock"})
	response = receiver.get(timeout=10)
	assert response["action"] == "already-locked"


def test_trader_unlock(config, db, process, exchange, graph):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	sender.put({"action": "unlock"})
	response = receiver.get(timeout=10)
	assert response["action"] == "already-unlocked"
	sender.put({"action": "lock"})
	response = receiver.get(timeout=10)
	sender.put({"action": "unlock"})
	response = receiver.get(timeout=10)
	assert response["action"] == "unlocked"
	sender.put({"action": "unlock"})
	response = receiver.get(timeout=10)
	assert response["action"] == "already-unlocked"


def test_trader_funds_request_accept(config, db, process, exchange, graph, strategy, mocker):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	response = receiver.get(timeout=10)
	assert response["action"] == "request-fund"
	assert "asset" in response
	asset = response["asset"]
	assert asset == "BTC"
	fund = (
		FundModel.query
		.join(AssetModel)
		.filter(AssetModel.symbol == response["asset"])
		.first()
	)
	sender.put({
		"action": "fund-request-accept",
		"fund": dict(fund),
		"id": response["id"]
	})
	response = receiver.get(timeout=10)
	assert response["action"] == "create-order"
	assert "order" in response
	assert response["order"]["symbol"] == symbol
	assert response["order"]["price"] == 0.121
	assert response["order"]["side"] == "BUY"
	assert response["order"]["action"] == "OPEN"


def test_trader_funds_request_reject(config, db, process, exchange, graph, strategy, mocker):
	fund_reject = Trader._fund_reject

	def fake_reject(self, data):
		fund_reject(self, data)
		self._send({
			"action": "reject-response",
			"positions": self._positions,
			"waiters": self._waiters,
			"requests": self._requests,
		})
	mocker.patch('src.module.trader.Trader._fund_reject', fake_reject)
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	response = receiver.get(timeout=10)
	assert response["action"] == "request-fund"
	assert "asset" in response
	sender.put({
		"action": "fund-request-reject",
		"id": response["id"]
	})
	response = receiver.get(timeout=10)
	assert response["action"] == "reject-response"
	assert len(response["requests"]) == 0
	assert len(response["waiters"].keys()) == 0
	assert len(response["positions"].keys()) == 0


def test_trader_funds_return(config, db, process, exchange, graph, strategy):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	response = receiver.get(timeout=10)
	fund = (
		FundModel.query
		.join(AssetModel)
		.filter(AssetModel.symbol == response["asset"])
		.first()
	)
	sender.put({
		"action": "fund-request-accept",
		"fund": dict(fund),
		"id": response["id"]
	})
	response = receiver.get(timeout=10)
	sender.put({
		"action": "position-update",
		"position": {
			"id": response["order"]["id"],
			"status": PositionStatus.OPEN
		},
		"exchange_id": exchange.id
	})
	sleep(0.1)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	response = receiver.get(timeout=10)
	assert response["action"] == "create-order"
	assert response["order"]["side"] == "SELL"
	assert response["order"]["symbol"] == "ETHBTC"
	assert response["order"]["price"] == 0.131
	assert response["order"]["action"] == "CLOSE"
	sender.put({
		"action": "position-update",
		"position": {
			"id": response["order"]["id"],
			"status": PositionStatus.CLOSED
		},
		"exchange_id": exchange.id
	})
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	while response["action"] == "create-order":
		response = receiver.get(timeout=10)
	assert response["action"] == "return-fund"
	assert response["fund_id"] == fund.id


def test_trader_open_position(config, db, process, exchange, graph):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	fund = (
		FundModel.query
		.join(AssetModel)
		.filter(AssetModel.symbol == "BTC")
		.first()
	)
	position = {
		"type": PositionType.LONG,
		"amount": fund.amount,
		"price": 0.12,
		"open_time": datetime.utcnow(),
		"modified": datetime.utcnow().isoformat(),
		"fund_id": fund.id,
		"exchange_id": exchange.id,
	}
	sender.put({
		"action": "position-open",
		"position": position,
		"fund_id": fund.id,
		"exchange_id": exchange.id
	})
	response = receiver.get(timeout=10)
	assert response["action"] == "create-order"
	assert response["order"]["symbol"] == "ETHBTC"
	assert response["order"]["side"] == "BUY"
	assert response["order"]["price"] == 0.12
	assert response["order"]["action"] == "OPEN"


def test_trader_close_position(config, db, process, exchange, graph):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	fund = (
		FundModel.query
		.join(AssetModel)
		.filter(AssetModel.symbol == "BTC")
		.first()
	)
	position = {
		"type": PositionType.LONG,
		"amount": fund.amount,
		"price": 0.12,
		"open_time": datetime.utcnow(),
		"modified": datetime.utcnow().isoformat(),
		"fund_id": fund.id,
		"exchange_id": exchange.id,
	}
	sender.put({
		"action": "position-open",
		"position": position,
		"fund_id": fund.id,
		"exchange_id": exchange.id
	})
	response = receiver.get(timeout=10)
	id = response["order"]["id"]
	sender.put({
		"action": "position-update",
		"position": {
			"id": response["order"]["id"],
			"status": PositionStatus.OPEN
		},
		"exchange_id": exchange.id
	})
	sleep(0.1)
	sender.put({
		"action": "position-close",
		"id": id,
		"price": 0.21,
		"exchange_id": exchange.id
	})
	response = receiver.get(timeout=10)
	assert response["action"] == "create-order"
	assert response["order"]["action"] == "CLOSE"


def test_trader_cancel_position(config, db, process, exchange, graph):
	symbol = "ETHBTC"
	sender, receiver, process = init(config, process, exchange, symbol=symbol)
	response = receiver.get(timeout=10)
	sender.put({"action": "graph", "graph": graph, "exchange_id": exchange.id})
	response = receiver.get(timeout=10)
	for i in range(2):
		sender.put({
			"action": "candle",
			"candle": candle(0.123),
			"exchange_id": exchange.id
		})
	fund = (
		FundModel.query
		.join(AssetModel)
		.filter(AssetModel.symbol == "BTC")
		.first()
	)
	position = {
		"type": PositionType.LONG,
		"amount": fund.amount,
		"price": 0.12,
		"open_time": datetime.utcnow(),
		"modified": datetime.utcnow().isoformat(),
		"fund_id": fund.id,
		"exchange_id": exchange.id,
	}
	sender.put({
		"action": "position-open",
		"position": position,
		"fund_id": fund.id,
		"exchange_id": exchange.id
	})
	response = receiver.get(timeout=10)
	id = response["order"]["id"]
	sender.put({
		"action": "position-update",
		"position": {
			"id": response["order"]["id"],
			"status": PositionStatus.OPEN
		},
		"exchange_id": exchange.id
	})
	sleep(0.1)
	sender.put({
		"action": "position-cancel",
		"id": id,
		"exchange_id": exchange.id
	})
	response = receiver.get(timeout=10)
	assert response["action"] == "create-order-market"
	assert response["order"]["action"] == "CLOSE"
