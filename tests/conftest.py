from os import path, mkdir, makedirs, remove
from multiprocessing import Process
from shutil import rmtree, copyfile
from orjson import dumps, loads
import sys
import pytest


pytest_plugins = ['pytester']

base_path = path.realpath(__file__).replace("tests/conftest.py", "")
if base_path not in sys.path:
	sys.path.insert(0, base_path)

try:
	tr = None
	for i in sys.path:
		if i[10:] == "tests/data":
			tr = i
			break
	sys.path.remove(tr)
	print("Pytest - Removed:", tr)
except Exception:
	pass


@pytest.fixture
def process():
	process = []

	def wrapper(*args, **kwargs):
		process.append(Process(*args, **kwargs))
		process[-1].start()
		return process[-1]

	yield wrapper

	for p in process:
		p.terminate()


@pytest.fixture
def config(config={}):
	from src.system import System
	workdir = "/tmp/cb-test"
	if path.exists(workdir):
		rmtree(workdir)
	mkdir(workdir)
	config = config or {
		"workdir": workdir,
		"debug": True,
		"autostart": True,
		"type": "trader",
		"allowed_modules": ["urllib"]
	}
	mkdir(workdir + "/config")
	mkdir(workdir + "/log")
	mkdir(workdir + "/data")
	mkdir(workdir + "/data/strategies")
	mkdir(workdir + "/data/indicators")
	mkdir(workdir + "/strategies")
	mkdir(workdir + "/indicators")
	cf = open(workdir + "/config/robot.json", "wb")
	cf.write(dumps(config))
	cf.close()
	System(["update"], "", "", workdir, True, True)
	return config


@pytest.fixture
def server_config(config):
	config["server"] = {
		'host': '0.0.0.0',
		'port': 6666,
		"secret": "ABC",
		"client_secrets": ["BCA"]
	}
	return config


@pytest.fixture
def exchange():
	from src.model import Database, Exchange
	Database.init("/tmp/cb-test/data.db")
	return Exchange.query.first()


@pytest.fixture
def exchanges():
	from src.model import Database, Exchange
	Database.init("/tmp/cb-test/data.db")
	return Exchange.query.all()


@pytest.fixture
def empty_trigger():
	def empty(data):
		pass
	return empty


@pytest.fixture
def graph():
	file = open(path.realpath(__file__).replace("conftest.py", "data/ETHBTC_graph"))
	raw = loads(file.read())
	file.close()
	graph = {}
	for interval in raw:
		graph[interval] = []
		for item in raw[interval]:
			graph[interval].append({
				"open_time": item[0],
				"close_time": item[6],
				"open": float(item[1]),
				"close": float(item[4]),
				"high": float(item[2]),
				"low": float(item[3]),
				"volume": float(item[5])
			})
	return graph


@pytest.fixture
def strategy():
	from src.model import Database, Strategy
	Database.init("/tmp/cb-test/data.db")
	strategy = Strategy.query.filter_by(name='test_strategy').first()
	strategy.active = True
	strategy.update()
	return strategy


@pytest.fixture
def strategy_invalid():
	from src.model import Database, Strategy
	Database.init("/tmp/cb-test/data.db")
	strategy = Strategy.query.filter_by(name='test_strategy_invalid').first()
	strategy.active = True
	strategy.update()
	return strategy


@pytest.fixture
def empty_db():
	import sqlite3
	from alembic import command  # type: ignore
	if path.exists("/tmp/cb-test/data.db"):
		remove("/tmp/cb-test/data.db")
	from src.system import System
	config = System._alembic_config({"workdir": "/tmp/cb-test"}, True, True)
	command.upgrade(config, "head")
	connection = sqlite3.connect("/tmp/cb-test/data.db")
	db = connection.cursor()
	return db


@pytest.fixture
def db():
	import sqlite3
	connection = sqlite3.connect("/tmp/cb-test/data.db")
	db = connection.cursor()
	db.execute("DELETE FROM position;")
	db.execute("DELETE FROM fund;")
	db.execute("DELETE FROM asset;")
	db.execute("DELETE FROM exchange;")
	db.execute("INSERT INTO asset (symbol) VALUES ('BTC');")
	db.execute("INSERT INTO asset (symbol) VALUES ('ETH');")
	db.execute("INSERT INTO asset (symbol) VALUES ('BNB');")

	BTC = db.execute("SELECT id FROM asset where symbol='BTC'").fetchone()[0]
	ETH = db.execute("SELECT id FROM asset where symbol='ETH'").fetchone()[0]
	BNB = db.execute("SELECT id FROM asset where symbol='BNB'").fetchone()[0]

	db.execute("""
		INSERT INTO strategy (id, external_id, name, version, active, priority) VALUES
		(1, 1, 'test_strategy', '1.0.0', FALSE, 0)
	""")
	db.execute("""
		INSERT INTO strategy (id, external_id, name, version, active, priority) VALUES
		(2, 2, 'test_strategy_invalid', '1.0.0', FALSE, 0)
	""")

	with open("/tmp/cb-test/strategies/__init__.py", "w") as fp:
		# fp.write("from . import *\n")
		fp.close()

	makedirs("/tmp/cb-test/strategies/test_strategy/v1_0_0")
	copyfile(
		"%sdata/example_test_strategy.py" % path.realpath(__file__).replace("conftest.py", ""),
		"/tmp/cb-test/strategies/test_strategy/v1_0_0/strategy.py"
	)
	with open("/tmp/cb-test/strategies/test_strategy/__init__.py", "w") as fp:
		fp.close()
	with open("/tmp/cb-test/strategies/test_strategy/v1_0_0/__init__.py", "w") as fp:
		fp.close()
	makedirs("/tmp/cb-test/strategies/test_strategy_invalid/v1_0_0")
	copyfile(
		"%sdata/example_test_strategy_invalid.py" % path.realpath(__file__).replace("conftest.py", ""),
		"/tmp/cb-test/strategies/test_strategy_invalid/v1_0_0/strategy.py"
	)
	with open("/tmp/cb-test/strategies/test_strategy_invalid/__init__.py", "w") as fp:
		fp.close()
	with open("/tmp/cb-test/strategies/test_strategy_invalid/v1_0_0/__init__.py", "w") as fp:
		fp.close()

	intervals = dumps([
		"1m", "3m", "5m", "15m", "30m", "1h", "2h", "4h",
		"6h", "8h", "12h", "1d", "3d", "1w", "1M"
	]).decode()
	db.execute("""
		INSERT INTO exchange (id, name, fee, watch_intervals, graph_length, active) VALUES
		(1, 'binance', 0.1, '%s', 1000, TRUE)
	""" % intervals)

	db.execute("""
		INSERT INTO pair (symbol, base_asset_id, quote_asset_id, active) VALUES
		('ETHBTC', %s, %s, TRUE);
	""" % (ETH, BTC))
	db.execute("""
		INSERT INTO pair (symbol, base_asset_id, quote_asset_id, active) VALUES
		('BNBBTC', %s, %s, TRUE);
	""" % (BNB, BTC))
	db.execute("""
		INSERT INTO pair (symbol, base_asset_id, quote_asset_id, active) VALUES
		('BNBETH', %s, %s, TRUE);
	""" % (BNB, ETH))

	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(1, '%s', 1, FALSE, TRUE);
	""" % BTC)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(1, '%s', 1, FALSE, TRUE);
	""" % BTC)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(10, '%s', 1, FALSE, TRUE);
	""" % BNB)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(10, '%s', 1, FALSE, TRUE);
	""" % BNB)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(3, '%s', 1, FALSE, TRUE);
	""" % ETH)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(3, '%s', 1, FALSE, TRUE);
	""" % ETH)

	db.execute("""
		INSERT INTO test (id, external_id, status, config, started) VALUES
		(1, 1, 0, '{}', datetime('now'));
	""")

	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(1, '%s', 1, FALSE, FALSE);
	""" % BTC)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(1, '%s', 1, FALSE, FALSE);
	""" % ETH)
	db.execute("""
		INSERT INTO fund (amount, asset_id, exchange_id, locked, active) VALUES
		(1, '%s', 1, FALSE, FALSE);
	""" % BNB)

	test_funds = db.execute("SELECT id FROM fund where active=FALSE").fetchall()
	for tf in test_funds:
		db.execute("INSERT INTO test_fund (fund_id, test_id) VALUES (%s, 1)" % tf[0])
	test_pairs = db.execute("SELECT id FROM pair").fetchall()
	for tp in test_pairs:
		db.execute("INSERT INTO test_pair (pair_id, test_id) VALUES (%s, 1)" % tp[0])

	connection.commit()
	return db
