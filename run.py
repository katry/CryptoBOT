#!/usr/bin/env python

def version_check(args, v):
	try:
		wd = args.workdir
		file = open("%s/.CryptoBOT.ver" % wd, "r")
		version = file.read().replace("\n", "")
		file.close()
	except Exception:
		version = None
	if not version:
		print(
			">>> CryptoBOT was not initialized in directory:\n" +
			"  > %s\n\n" % args.workdir +
			">>> RUN:  CryptoBOT system init --workdir %s\n"
			% args.workdir
		)
		return False
	if version != v:
		print(
			">>> CryptoBOT package was updated (%s -> %s).\n" % (version, v) +
			"  > Update initialized data structures before running service.\n\n" +
			">>> RUN COMMAND:  CryptoBOT system update --workdir %s\n"
			% args.workdir
		)
		return False
	return True


def entrypoint(local=False):
	from os import getcwd, name
	from argparse import ArgumentParser
	if local:
		from src.application import Application
		from src import __version__
	else:
		from CryptoBOT.application import Application  # type: ignore
		from CryptoBOT import __version__  # type: ignore

	parser = ArgumentParser(description="Trading automation interface")
	parser.add_argument(
		"command",
		nargs="*",
		default=["start"],
		help="CLI command (for more information check readme)"
	)
	parser.add_argument(
		"--workdir",
		nargs="?",
		default=getcwd(),
		help="CryptoBOT working directory (default actual directory)"
	)
	parser.add_argument("-v", "--version", action="store_true")
	parser.add_argument("-d", "--daemon", action="store_true")

	parser.add_argument("--exchange", nargs="?", default="binance")
	parser.add_argument("--nograph", action="store_false", default=True, help="Prevents collector create graph")
	parser.add_argument("--repair-integrity", action="store_true", help="Repair wrong sorted testing data from collector")
	parser.add_argument("--repair", action="store_true", help="Try download (aggregated) data missing from exchange outage")
	parser.add_argument("--data", nargs="?", default="_data_basic_2020_09_27")
	parser.add_argument("--archive-dir", nargs="?", default=None, help="Select archive directory")
	parser.add_argument("--weeks", nargs="?", type=int, default=0, help="Given weeks until now")

	parser.add_argument("--debug", action="store_true", help="only for developers")
	parser.add_argument("--message", nargs=1, default=[""], help="only for developers")
	parser.add_argument("--revision", nargs=1, default=[""], help="only for developers")

	args = parser.parse_args()

	if args.version:
		print(">>> CryptoBOT %s" % __version__)
	elif args.command[0] == "system":
		if local:
			from src.system import System
		else:
			from CryptoBOT.system import System  # type: ignore
		System(
			args.command[1:],
			args.message[0],
			args.revision[0],
			args.workdir or args.command[0],
			debug=args.debug,
			local=local
		)
	elif version_check(args, __version__):
		if args.daemon:
			if name == "posix":
				from os import (
					fork, closerange, sysconf, open as os_open, devnull,
					O_RDWR, O_WRONLY, O_TRUNC, O_CREAT
				)
				pid = fork()
				closerange(0, sysconf("SC_OPEN_MAX") or 1024)
				os_open(devnull, O_RDWR)
				os_open(devnull, O_WRONLY | O_TRUNC | O_CREAT)
				if pid > 0:
					return
			if name == "nt":
				from os import path
				from sys import argv
				from subprocess import Popen
				while "--daemon" in argv:
					argv.remove("--daemon")
				while "-d" in argv:
					argv.remove("-d")
				Popen("pythonw %s %s" % (path.realpath(__file__), " ".join(argv[1:])))
				return
		Application.run(**vars(args).copy())


if __name__ == "__main__":
	entrypoint(local=True)
