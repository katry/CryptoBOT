from time import time, sleep
from threading import Thread
from multiprocessing import Process, Queue
from asyncio import get_event_loop
from os import path, makedirs, stat, walk, mkdir, rmdir, remove, chdir, getcwd
from orjson import loads, dumps
from shutil import move
from tarfile import open as topen
from .lib.logging import Logging
from .lib.schedule import Schedule
from .module.exchanges import exchanges
from .model import (
	Database,
	Pair as PairModel,
	Exchange as ExchangeModel
)


class Collector(Logging):
	__buffer: list = []

	def __init__(self, config, exchange_name, graph=True):
		super().__init__(config)
		Database.init("%s/data.db" % config["workdir"], check_same_thread=False)
		exchange = ExchangeModel.query.filter_by(name=exchange_name).first()
		if exchange_name not in exchanges or not exchange:
			self.logger.error("Unrecognized exchange '%s'" % exchange_name)
			return
		self.config = config
		self.exchange = exchanges[exchange_name](config, exchange)
		self.symbols = []
		self.data = {}
		self.intervals = [
			"1m", "3m", "5m", "15m", "30m", "1h", "2h", "4h", "6h", "8h", "12h", "1d", "3d", "1w", "1M"
		]
		self.load_graph = graph
		self.path = "%s/data/%s/collector" % (config["workdir"], exchange_name)

		self.__queue = Queue()
		self.__schedule = None
		self.__writing = False
		self.__archiving = False
		self.__pending_archivation = False
		if not path.exists(self.path):
			makedirs(self.path)

		self.set_symbols()
		self.start()

	def set_symbols(self):
		pairs = PairModel.query.filter_by(active=True).all()
		for pair in pairs:
			self.symbols.append(pair.symbol)

	def start(self):
		if "archiving" in self.config:
			sh = {
				"minute": self.config["archiving"].get("minute", 0)
			}
			if self.config["archiving"]["interval"] in ["daily", "weekly"]:
				sh["hour"] = self.config["archiving"].get("hour", 0)
			if self.config["archiving"]["interval"] == "weekly":
				sh["weekday"] = self.config["archiving"].get("weekday", "sunday")
			self.__schedule = Schedule.every(self._archiver, **sh)
		ch = Thread(target=self._checker)
		ch.start()
		self.process()

	def _checker(self):
		while True:
			signal = self.__queue.get()
			if signal == "written":
				self.__writing = False
				if self.__pending_archivation:
					self._archiver()
			if signal == "archived":
				self.__archiving = False

	def process(self):
		progress = []
		if self.load_graph:
			for symbol in self.symbols:
				t = Thread(target=Collector.write_graph, args=(
					symbol, self.intervals, self.path, self.exchange, progress
				))
				t.start()
			while len(progress) != len(self.symbols):
				sleep(0.2)
		loop = get_event_loop()
		loop.run_until_complete(self.exchange.subscribe(self.symbols, self.write_stream, raw=True))

	def write_stream(self, data):
		self.__buffer.append(data)
		t = time() % 3600 % 60
		if (len(self.__buffer) == 200 or (t > 57 and t < 60)) and not self.__archiving:
			if self.__writing:
				sleep(0.1)
				self.__buffer = []
				self.write_stream(data)
			else:
				self.__writing = True
				p = Process(target=self.write, args=(self.__buffer, self.path, self.__queue))
				p.start()
				self.__buffer = []

	@staticmethod
	def write(data, p, queue):
		sorted = {}
		for item in data:
			symbol = loads(item.encode())["data"]["s"]
			if not sorted.get(symbol):
				sorted[symbol] = []
			sorted[symbol].append(item)

		for symbol in sorted:
			if path.isfile("%s/%s_stream.txt" % (p, symbol)):
				file_size = stat("%s/%s_stream.txt" % (p, symbol)).st_size
			else:
				file_size = 0
			txt = ",".join(sorted[symbol])

			file = open("%s/%s_stream.txt" % (p, symbol), "ab")
			file.write((",%s" % txt if file_size else txt).encode())
			file.close()
		queue.put("written")

	@staticmethod
	def write_graph(symbol, intervals, p, exchange, progress):
		graph = exchange.get_graph(symbol, intervals, 1000, True)
		data = dumps(graph)
		file = open("%s/%s_graph.json" % (p, symbol), "wb+")
		file.write(data)
		file.close()
		progress.append(symbol)

	def _archiver(self):
		if not self.__writing:
			self.__pending_archivation = False
			self.__archiving = True
			archiver = Process(
				target=Collector.pack,
				args=(self.config["workdir"], self.exchange.name, self.__queue)
			)
			archiver.start()
		else:
			self.__pending_archivation = True

	@staticmethod
	def pack(pth, exchange, queue):
		src = "%s/data/%s/collector/" % (pth, exchange)
		dest = "%s/data/%s/packed/" % (pth, exchange)
		temp = "%s/data/%s/temp/" % (pth, exchange)
		if not path.exists(dest):
			mkdir(dest)
		if not path.exists(temp):
			mkdir(temp)

		timestamp = round(time())
		graph_files = []
		for (dir_path, dir_name, file_names) in walk(src):
			for file_name in file_names:
				if "graph" not in file_name:
					continue
				graph_files.append(file_name)
				move(dir_path + file_name, temp + file_name)
		if len(graph_files):
			tar = topen("%s/%s_graph.tar.gz" % (dest.replace("packed/", ""), timestamp), "w:gz")
			cwd = getcwd()
			chdir(temp)
			for file in graph_files:
				tar.add(file)
			tar.close()
			for file in graph_files:
				remove(file)
			chdir(cwd)

		stream_files = []
		for (dir_path, dir_name, file_names) in walk(src):
			for file_name in file_names:
				if "stream" not in file_name:
					continue
				stream_files.append(file_name.replace(".txt", ".json"))
				file = open(dir_path + file_name, "r")
				txt = file.read()
				file.close()
				remove(dir_path + file_name)
				while len(txt) and txt[0] != "{":
					txt = txt[1:]
				if len(txt):
					json = "[" + txt + "]"
				else:
					json = "[]"
				file = open(temp + file_name.replace(".txt", ".json"), "w")
				txt = file.write(json)
				file.close()
		if len(stream_files):
			tar = topen("%s/%s_stream.tar.gz" % (dest, timestamp), "w:gz")
			cwd = getcwd()
			chdir(temp)
			for file in stream_files:
				tar.add(file)
			tar.close()
			for file in stream_files:
				remove(file)
			chdir(cwd)
		rmdir(temp)
		queue.put("archived")
