from typing import Callable
from .validator import Validator


class SocketWrap:
	__send = None

	def __init__(self, response_action="", validator=None):
		self.__response_action = response_action
		self.__validator = validator

	def __call__(self, fn, *args, **kwargs):
		def wrapper(request, *args, **kwargs):
			status = True
			response = {}

			if self.__validator and not Validator.check(request.get("data", {}), self.__validator):
				response["status"] = False
				response["message"] = "Invalid request"
				return self.send(data={"response": response}, target="server")
			data = fn(request.get("data", None), *args, **kwargs)
			if type(data) == tuple:
				status = data[0]
				data = data[1]

			if status:
				response = data
			else:
				response["message"] = data
			if "id" in request:
				response["request_id"] = request["id"]
			if "client" in request:
				self.send(
					data={
						"action": "send",
						"type": self.__response_action,
						"status": status,
						"client": request["client"],
						"payload": response
					},
					target="server"
				)

		return wrapper

	@property
	def send(self):
		if not SocketWrap.__send:
			raise NotImplementedError("Unitialized send method")
		return SocketWrap.__send

	@send.setter
	def send(self, value: Callable):
		self.__SocketWrap = value
