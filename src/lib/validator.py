class Validator:
	@staticmethod
	def check(structure: dict, validator: dict, exact_keys: bool = False) -> bool:
		if exact_keys:
			for key in structure:
				if key not in validator.keys():
					print("Invalid key", key)
					return False
		for key in validator:
			if key not in structure:
				if type(validator[key][0]) in (list, tuple) and "optional" in validator[key][0]:
					continue
				else:
					print("Key not found", key)
					return False
			value = structure[key]
			if type(validator[key][0]) in (list, tuple):
				found = False
				for t in validator[key][0]:
					if Validator._type(t, value):
						found = True
						break
				if not found:
					return False
			elif not Validator._type(validator[key][0], value):
				return False
			if type(validator[key][1]) == dict:
				if validator[key][0] == list or list in validator[key][0]:
					for item in value:
						if (
							type(item) != dict or
							not Validator.check(item, validator[key][1], exact_keys=exact_keys)
						):
							return False
				elif (
					type(value) != dict or
					not Validator.check(value, validator[key][1], exact_keys=exact_keys)
				):
					return False
			elif (
				type(validator[key][1]) == type and
				(
					validator[key][0] == list or (validator[key][0] in (list, tuple) and
					list in validator[key][0])
				)
			):
				for item in value:
					if type(item) != validator[key][1]:
						print("3")
						return False
			elif not Validator._condition(validator[key][1], value, structure):
				print("4")
				return False
		return True

	@staticmethod
	def _type(validator: dict, value) -> bool:
		if type(validator) in (list, tuple):
			if type(value) not in validator:
				return False
		elif type(value) != validator:
			return False
		return True

	@staticmethod
	def _condition(validator, value, structure: dict):
		if callable(validator) and not bool(validator(value, structure)):
			return False
		return True
