class Dedicated:
	_receive_thread = None

	def __init__(self, sender, receiver, init_message=None):
		self._sender = sender
		self._receiver = receiver
		self._running = True
		if init_message:
			self._send(init_message)
		self._receive()

	def _send(self, data, target=None):
		data["target"] = target
		self._sender.put(data)

	def _receive(self):
		while self._running:
			try:
				data = self._receiver.get(timeout=1)
			except Exception:
				data = None
			if data is not None:
				self._trigger(data)

	def _trigger(self, data):
		pass
