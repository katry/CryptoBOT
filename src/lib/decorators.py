from functools import wraps


class classproperty:
	def __init__(self, fget):
		if not isinstance(fget, (classmethod, staticmethod)):
			fget = classmethod(fget)
		self.__fget = fget

	def __get__(self, obj, objtype):
		return self.__fget.__get__(obj, objtype)()

	def setter(self, func):
		raise NotImplementedError("classproperty can only be read-only; use-metaclass")

	def deleter(self, func):
		raise NotImplementedError("classproperty can only be read-only; use-metaclass")


class hybridmethod:
	def __init__(self, call):
		self.__call = call
		self.__context = None

		@wraps(self.__call)
		def hybrid(*args, **kw):
			return self.__call(self.__context, *args, **kw)
		self.__hybrid = hybrid

	def __get__(self, obj, cls):
		self.__context = obj if obj is not None else cls
		return self.__hybrid


class hybridproperty(property):
	def __init__(self, fget):
		self.__fget = fget
		self.__context = None

		@wraps(self.__fget)
		def hybrid():
			return self.__fget(self.__context)
		self.__hybrid = hybrid

	def __get__(self, obj, cls):
		self.__context = obj if obj is not None else cls
		return self.__hybrid()

	def setter(self, func):
		raise NotImplementedError("hybridbproperty can only be read-only; use-metaclass")

	def deleter(self, func):
		raise NotImplementedError("hybridbproperty can only be read-only; use-metaclass")
