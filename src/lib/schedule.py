from time import time, sleep
from datetime import datetime, timedelta
from threading import Thread
from uuid import UUID, uuid4
from sched import scheduler
from typing import Callable
from .decorators import hybridmethod
from .validator import Validator


class Schedule:
	__sched = scheduler(time, sleep)
	__queue: dict = {}
	__weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
	__validator: dict = {
		"weekday": (
			[int, str, "optional"],
			lambda x, *a: x in Schedule.__weekdays or (type(x) == int and x >= 0 and x < 7)
		),
		"day": ([int, "optional"], lambda x, *a: x >= 0 and x < 32),
		"hour": ([int, "optional"], lambda x, *a: x >= 0 and x < 24),
		"minute": ([int, "optional"], lambda x, *a: x >= 0 and x < 60),
		"second": ([int, "optional"], lambda x, *a: x >= 0 and x < 60),
	}
	__running = False
	__thread = None

	def __init__(self):
		self.__sched = scheduler(time, sleep)

	@hybridmethod
	def delay(target, delay, callback: Callable, args=()) -> UUID:
		return target.__create(delay, callback, args)

	@hybridmethod
	def at(target, date: datetime, callback: Callable, args=()) -> UUID:
		delay = (date - datetime.now()) / timedelta(seconds=1)
		if delay <= 0:
			raise ValueError("Time has passed")
		return target.__create(delay, callback, args)

	@hybridmethod
	def every(target, callback: Callable, args=(), **kwargs) -> UUID:
		if template := target.__check(kwargs):
			return target.__create(target.__next(template), callback, args, template)
		raise ValueError("Invalid parameters only [day,weekday,hour,minute,second] possible")

	@hybridmethod
	def clear(target):
		queue_copy = target.__queue.copy()
		for key in queue_copy:
			target.__sched.cancel(target.__queue.pop(key))

	@hybridmethod
	def cancel(target, event):
		if event := target.__queue.pop(event, None):
			target.__sched.cancel(event)
		else:
			raise ReferenceError("Invalid cancel target")

	@hybridmethod
	def __wrap(target, uuid: UUID, callback: Callable, template: dict):
		def wrap(*args, **kwargs):
			callback(*args, **kwargs)
			target.__queue.pop(uuid)

		def wrap_every(*args, **kwargs):
			def empty(*args, **kwargs):
				pass

			if uuid not in target.__queue:
				return empty(*args, **kwargs)
			target.__queue[uuid] = target.__sched.enter(
				target.__next(template), 1, wrap_every, argument=args, kwargs=kwargs
			)
			callback(*args, **kwargs)
		return wrap_every if template else wrap

	@hybridmethod
	def __create(target, delay, callback, args, template=None):
		uuid = uuid4()
		target.__queue[uuid] = None
		wrap = target.__wrap(uuid, callback, template)
		op = target.__sched.enter(delay, 1, wrap, argument=args)
		target.__queue[uuid] = op
		if not target.__running:
			target.__thread = Thread(target=target.__run, args=(True,))
			target.__thread.start()
		return uuid

	@hybridmethod
	def __run(target, *args):
		target.__running = True
		target.__sched.run(*args)
		target.__running = False

	@staticmethod
	def __check(template: dict) -> dict | None:
		if (
			not len(template.keys()) or
			not Validator.check(template, Schedule.__validator, exact_keys=True)
		):
			return None
		if "day" in template and "weekday" in template:
			raise ValueError("Only one of parameters [day,weekday] can be specified at the time")
		template["day"] = template.get("day", None)
		wd = template.get("weekday", None)
		template["weekday"] = Schedule.__weekdays.index(wd) if type(wd) == str else wd
		template["hour"] = template.get("hour", None)
		template["minute"] = template.get("minute", None)
		template["second"] = template.get("second", 0)
		return template

	@staticmethod
	def __next(template: dict):
		now = datetime.now()
		month = None
		if "weekday" in template and type(template["weekday"]) == int:
			da = template["weekday"] - now.weekday()
			if da < 0:
				da += 7
			weekday = now + timedelta(da if da > 0 else da + 7)
			template["day"], month = weekday.day, weekday.month
		run_date = datetime(
			now.year,
			now.month if month is None else month,
			now.day if template["day"] is None else template["day"],
			now.hour if template["hour"] is None else template["hour"],
			now.minute if template["minute"] is None else template["minute"],
			template["second"],
		)
		while run_date <= datetime.now():
			if template["weekday"] is not None:
				run_date += timedelta(days=7)
			elif template["day"] is not None:
				run_date = datetime(
					run_date.year, run_date.month + 1, run_date.day,
					run_date.hour, run_date.minute, run_date.second
				)
			elif template["hour"] is not None:
				run_date += timedelta(days=1)
			elif template["minute"] is not None:
				run_date += timedelta(hours=1)
			else:
				run_date += timedelta(minutes=1)
		return (run_date - datetime.now()).total_seconds()
