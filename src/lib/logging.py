from os import path, makedirs
from datetime import datetime
from logging import (
	getLogger,
	Formatter,
	StreamHandler,
	DEBUG, INFO
)
from logging.handlers import TimedRotatingFileHandler


class Logging:
	__loggers: list = []
	__path = ""

	def __init__(self, config: dict | None = None, logger: str = "", debug: bool = False):
		if not self.__path and config:
			Logging.__path = "%s/log" % config["workdir"]
		if not logger:
			logger = self.__class__.__name__.lower()
		if logger not in self.__loggers:
			self.__set_logger(logger, level=DEBUG if debug else INFO)
		else:
			self.__logger = getLogger(logger)

	def __set_logger(self, name: str, level: int):
		formatter = Formatter('%(asctime)s|%(levelname)s: %(message)s')
		stream_formatter = Formatter('%(asctime)s|%(name)s|%(levelname)s: %(message)s')
		today = datetime.utcnow().isoformat().split("T")[0]
		p = self.__path or "/var/log"
		if not path.exists(p):
			makedirs(p)
		self.__logger = getLogger(name)
		self.__logger.setLevel(INFO)
		fh = TimedRotatingFileHandler(
			"%s/%s-%s.log" % (p, name, today),
			when="midnight",
			utc=True
		)
		sh = StreamHandler()
		fh.setLevel(level)
		sh.setLevel(level)
		fh.setFormatter(formatter)
		sh.setFormatter(stream_formatter)
		self.__logger.addHandler(fh)
		self.__logger.addHandler(sh)
		self.__loggers.append("logger")

	@property
	def logger(self):
		return self.__logger
