from sqlalchemy import Column, Integer, BigInteger, String, UniqueConstraint
from .base import Base


class Indicator(Base):
	__tablename__ = "indicator"
	__table_args__ = (
		UniqueConstraint("name", "version", name="uq_indicator_name_version"),
	)

	id = Column(Integer, primary_key=True, autoincrement=True)
	external_id = Column(BigInteger, nullable=True, index=True, unique=True)
	name = Column(String, nullable=False, index=True)
	version = Column(String, nullable=False, index=True)
