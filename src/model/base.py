from typing import Type
from sqlalchemy import create_engine, MetaData, Integer, text
from sqlalchemy.types import TypeDecorator
from sqlalchemy.orm import Session, sessionmaker, reconstructor, declarative_base
from sqlalchemy.orm.decl_api import registry
from sqlalchemy.orm.query import Query
from .lib.decorators import classproperty, hybridmethod, hybridproperty


class IntEnumSQL(TypeDecorator):
	impl = Integer
	cache_ok = True

	def __init__(self, enumtype, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._enumtype = enumtype

	def process_bind_param(self, value, dialect):
		return value

	def process_result_value(self, value, dialect):
		return self._enumtype(value)


class Database:
	__engine = None
	__session = None

	def __init__(self, path):
		self.init(path, target=self)

	@staticmethod
	def init(path, target=None, check_same_thread: bool = True):
		target = target or Database
		target.__engine = create_engine(
			"sqlite:///%s?check_same_thread=%s" % (path, str(check_same_thread).lower())
		)
		target.__session = sessionmaker(bind=target.engine)()
		# target.execute("pragma journal_mode = WAL")
		target.execute("pragma synchronous = normal")
		target.execute("pragma temp_store = memory")
		target.execute("pragma mmap_size = 30000000000")

	@hybridmethod
	def get(self, pk_value):
		return self.__session.get(self, pk_value)

	@hybridmethod
	def query(self, *args, **kwargs):
		return self.__session.query(*args, **kwargs)

	@hybridmethod
	def add(self, *args, **kwargs):
		return self.__session.add(*args, **kwargs)

	@hybridmethod
	def flush(self, *args, **kwargs):
		return self.__session.flush(*args, **kwargs)

	@hybridmethod
	def commit(self, *args, **kwargs):
		return self.__session.commit(*args, **kwargs)

	@hybridmethod
	def execute(self, query):
		with self.__engine.begin() as cursor:
			return cursor.execute(text(query))

	@hybridproperty
	def session(self):
		return self.__session

	@hybridproperty
	def engine(self):
		return self.__engine


class Model(Database):
	__target: Type[Database] | Database = Database

	@reconstructor
	def __constructor(self, *args, **kwargs):
		self.__session = Session.object_session(self)
		if not self.__session:
			self.__session = sessionmaker(bind=self.__target.engine)()

	def __iter__(self):
		for key in [i for i in dir(self) if i[0] != "_"]:
			attr = getattr(self, key)
			if not callable(attr) and not isinstance(attr, (MetaData, registry, Query, Model)):
				yield (key, attr)

	@classproperty
	def query(cls):
		if cls.__target.engine:
			session = sessionmaker(bind=cls.__target.engine)()
			return session.query(cls)
		else:
			return None

	def create(self):
		self.__session.add(self)
		self.__session.commit()

	def update(self):
		Session.object_session(self).commit()

	def delete(self):
		Session.object_session(self).delete(self)
		Session.object_session(self).commit()

	def flush(self):
		Session.object_session(self).flush()

	@staticmethod
	def bind_db(target: Database):
		Model.__target = Database

	@staticmethod
	def _constructor(cls, *args, **kwargs):
		cls.__constructor(*args, **kwargs)

	@staticmethod
	def add():
		raise NotImplementedError

	@staticmethod
	def commit():
		raise NotImplementedError

	@staticmethod
	def execute():
		raise NotImplementedError

	@staticmethod
	def session():
		raise NotImplementedError

	@staticmethod
	def engine():
		raise NotImplementedError


Base = declarative_base(
	cls=Model,
	constructor=Model._constructor,
	metadata=MetaData(naming_convention={
		"ix": "ix_%(column_0_label)s",
		"uq": "uq_%(table_name)s_%(column_0_name)s",
		"ck": "ck_%(table_name)s_%(constraint_name)s",
		"fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
		"pk": "pk_%(table_name)s"
	})
)
