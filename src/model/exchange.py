from sqlalchemy import (
	Column, Boolean, Integer, BigInteger, Float, String, Float, SmallInteger
)
from sqlalchemy.types import JSON
from .base import Base


class Exchange(Base):
	__tablename__ = "exchange"

	id = Column(Integer, primary_key=True, autoincrement=True)
	external_id = Column(BigInteger, nullable=True)
	name = Column(String, nullable=False, index=True, unique=True)
	fee = Column(Float, nullable=False, index=True)
	watch_intervals = Column(JSON, nullable=False)
	graph_length = Column(SmallInteger, nullable=False, default=1000)
	active = Column(Boolean, nullable=False, default=False)
	api = Column(JSON, nullable=True)
