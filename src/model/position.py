from enum import IntEnum
from datetime import datetime
from sqlalchemy import (
	Column, ForeignKey, UniqueConstraint, Index, DDL, Text,
	Integer, BigInteger, Float, String, DateTime, Boolean,
)
from sqlalchemy.types import JSON
from sqlalchemy.orm import relationship
from .lib.decorators import classproperty
from . import Base, IntEnumSQL


class PositionType(IntEnum):
	SHORT = 0
	LONG = 1


class PositionStatus(IntEnum):
	PENDING = 0
	PARTIALY_FILLED = 1
	OPEN = 2
	CLOSED = 3
	REJECTED = 4
	CANCELED = 5
	FILLED = 6

	@classproperty
	def active(cls):
		return [cls.PENDING, cls.PARTIALY_FILLED, cls.OPEN]

	@classproperty
	def inactive(cls):
		return [cls.CLOSED, cls.REJECTED, cls.CANCELED]


class Pair(Base):
	__tablename__ = "pair"
	__table_args__ = (
		UniqueConstraint("base_asset_id", "quote_asset_id", name="uq_pair_base_quote_id"),
	)

	id = Column(Integer, primary_key=True, autoincrement=True)
	symbol = Column(String, nullable=False, index=True, unique=True)
	base_asset_id = Column(Integer, ForeignKey("asset.id"), nullable=False)
	quote_asset_id = Column(Integer, ForeignKey("asset.id"), nullable=False)
	active = Column(Boolean, nullable=False, default=False)

	base_asset = relationship("Asset", foreign_keys=[base_asset_id])
	quote_asset = relationship("Asset", foreign_keys=[quote_asset_id])


class TestPair(Base):
	__tablename__ = "test_pair"

	pair_id = Column(Integer, ForeignKey("pair.id"), primary_key=True)
	test_id = Column(Integer, ForeignKey("test.id"), primary_key=True)


class Position(Base):
	__tablename__ = "position"

	id = Column(Integer, primary_key=True, autoincrement=True)
	type = Column(IntEnumSQL(PositionType), nullable=False)
	status = Column(IntEnumSQL(PositionStatus), nullable=False, index=True)
	amount = Column(Float, nullable=False)
	open_price = Column(Float, nullable=False)
	close_price = Column(Float, nullable=True)
	open_time = Column(DateTime, nullable=False)
	close_time = Column(DateTime, nullable=True)
	order_id = Column(BigInteger, nullable=True, index=True)
	data = Column(JSON, nullable=True)
	check = Column(Boolean, nullable=False, default=True)
	close_trigger = Column(Text, nullable=True)
	modified = Column(DateTime, nullable=False, default=datetime.utcnow())
	pair_id = Column(Integer, ForeignKey("pair.id"), nullable=False, index=True)
	fund_id = Column(Integer, ForeignKey("fund.id"), nullable=False)
	exchange_id = Column(Integer, ForeignKey("exchange.id"), nullable=False, index=True)
	strategy_id = Column(Integer, ForeignKey("strategy.id"), nullable=True)

	pair = relationship("Pair")
	fund = relationship("Fund")
	exchange = relationship("Exchange")
	strategy = relationship("Strategy")

	__table_args__ = (
		UniqueConstraint("order_id", "exchange_id", name="uq_position_order_exchange_id"),
		Index("ix_position_open_time_desc", open_time.desc()),
		Index("ix_position_close_time_desc", close_time.desc()),
		Index("ix_position_modified_desc", modified.desc()),
	)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	@classmethod
	def from_dict(cls, data):
		pos = cls()
		pos.type = data["type"]
		pos.status = data.get("status", PositionStatus.PENDING)
		pos.amount = data["amount"]
		pos.open_price = data["price"]
		pos.open_time = data.get("open_time", datetime.utcnow())
		pos.order_id = data.get("order_id")
		pos.pair_id = data["pair_id"]
		pos.fund_id = data["fund_id"]
		pos.exchange_id = data["exchange_id"]
		pos.strategy_id = data.get("strategy_id")
		pos.create()
		return pos

	def overwrite(self, data):
		return True


class TestPosition(Base):
	__tablename__ = "test_position"

	position_id = Column(Integer, ForeignKey("position.id"), primary_key=True)
	test_id = Column(Integer, ForeignKey("test.id"), primary_key=True)


tg_position_update = DDL(
	"CREATE TRIGGER tg_position_modify AFTER UPDATE ON position "
	"BEGIN "
	"UPDATE position SET modified=datetime('now') WHERE id=new.id; "
	"END;"
)

tg_position_clear = DDL((
	"CREATE TRIGGER tg_position_clear BEFORE UPDATE OF status ON position "
	"WHEN new.close_trigger NOT NULL AND new.status IN %s "
	"BEGIN "
	"UPDATE position SET close_trigger=NULL "
	"WHERE id=old.id; "
	"END;"
) % str((int(PositionStatus.CLOSED), int(PositionStatus.REJECTED), int(PositionStatus.CANCELED))))

