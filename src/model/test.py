from enum import IntEnum
from datetime import datetime
from sqlalchemy import Column, Integer, BigInteger, Date, DateTime, Index
from sqlalchemy.types import JSON
from sqlalchemy.orm import relationship
from .lib.validator import Validator
from . import (
	Database as db,
	Base, IntEnumSQL, Asset, Fund, Pair, Strategy,
	TestFund, TestPair, TestPosition, TestStrategy
)


class TestStatus(IntEnum):
	PENDING = 0
	RUNNING = 1
	DONE = 2
	CANCELED = 3


class Test(Base):
	__tablename__ = "test"

	id = Column(Integer, primary_key=True, autoincrement=True)
	external_id = Column(BigInteger, nullable=False, index=True, unique=True)
	status = Column(IntEnumSQL(TestStatus), nullable=False, index=True)
	config = Column(JSON, nullable=False)
	trading_start = Column(Date, nullable=True)
	trading_stop = Column(Date, nullable=True)
	started = Column(DateTime, nullable=False, default=datetime.utcnow)
	stopped = Column(DateTime, nullable=True)

	funds = relationship("Fund", secondary=TestFund.__table__, lazy="select")
	pairs = relationship("Pair", secondary=TestPair.__table__, lazy="select")
	positions = relationship("Position", secondary=TestPosition.__table__, lazy="select")
	strategies = relationship(
		"Strategy",
		back_populates="tests",
		secondary=TestStrategy.__table__,
		lazy="select"
	)

	__table_args__ = (
		Index("ix_test_started_desc", started.desc()),
		Index("ix_test_stopped_desc", stopped.desc()),
	)

	@classmethod
	def from_config(cls, config, exchange_id):
		if type(config) != dict:
			return False
		assets = {a.symbol: a.id for a in Asset.query.all()}
		pairs = {p.symbol: p.id for p in Pair.query.all()}
		strategies = {s.external_id: s.id for s in Strategy.query.all()}

		def check_now(x):
			try:
				return datetime.fromtimestamp(x) < datetime.utcnow()
			except Exception:
				return False
		validator = {
			"external_id": (int, None),
			"trading_start": ([int, "optional"], lambda x, *a: x and check_now(x)),
			"trading_stop": (
				[int, "optional"],
				lambda x, *a: x and check_now(x) and (not a.get("trading_start") or a["trading_start"] < x)
			),
			"funds": (
				list,
				lambda x, *a: len(x) == len([i for i in x if i in assets.keys() and type(x[i]) in [int, float] and x[i] > 0])
			),
			"pairs": (list, lambda x, *a: x in pairs.keys()),
			"strategies": (list, lambda x: x in strategies.keys())
		}
		if not Validator.check(config, validator, exact_keys=True):
			return False

		c = config.copy()
		if "trading_start" in config:
			config["trading_start"] = datetime.fromtimestamo(config["trading_start"])
		if "trading_stop" in config:
			config["trading_stop"] = datetime.fromtimestamo(config["trading_stop"])

		funds = [{assets[a]: f} for (a, f) in config.pop("funds").items()]
		pairs = [pairs[p] for p in config.pop("pairs")]
		strategies = [strategies[s] for s in config.pop("strategies")]

		test = cls(**config, config=c)
		db.add(test)
		db.flush()
		for asset_id in funds:
			fund = Fund(amount=funds[asset_id], asset_id=asset_id, exchange_id=exchange_id)
			test.funds.append(fund)
			# db.add(fund)
			# db.flush()
			# db.add(TestFund(fund_id=fund.id, test_id=test.id))
		for pair in pairs:
			db.add(TestPair(pair_id=pair, test_id=test.id))
		for strategy in strategies:
			db.add(TestStrategy(strategy_id=strategy, test_id=test.id))
		db.commit()
		return test

	@classmethod
	def check_or_create(cls, test, exchange_id):
		if isinstance(test, cls):
			return test
		else:
			return cls.from_config(test, exchange_id)
