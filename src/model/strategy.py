from sqlalchemy import (
	Column, Integer, SmallInteger, BigInteger, String, Boolean,
	UniqueConstraint, ForeignKey
)
from sqlalchemy.orm import relationship
from .base import Base


class TestStrategy(Base):
	__tablename__ = "test_strategy"

	strategy_id = Column(Integer, ForeignKey("strategy.id"), primary_key=True)
	test_id = Column(Integer, ForeignKey("test.id"), primary_key=True)


class Strategy(Base):
	__tablename__ = "strategy"
	__table_args__ = (
		UniqueConstraint("name", "version", name="uq_strategy_name_version"),
	)

	id = Column(Integer, primary_key=True, autoincrement=True)
	external_id = Column(BigInteger, nullable=True, index=True, unique=True)
	name = Column(String, nullable=False, index=True)
	version = Column(String, nullable=False)
	active = Column(Boolean, nullable=False, default=False)
	priority = Column(SmallInteger, nullable=False, default=0)

	tests = relationship("Test", back_populates="strategies", secondary=TestStrategy.__table__)
