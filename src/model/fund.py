from sqlalchemy import (
	Column, Boolean, Integer, Float, String, ForeignKey, BigInteger
)
from sqlalchemy.orm import relationship
from .base import Base


class Asset(Base):
	__tablename__ = "asset"

	id = Column(Integer, primary_key=True, autoincrement=True)
	symbol = Column(String, nullable=False, index=True, unique=True)


class Fund(Base):
	__tablename__ = "fund"

	id = Column(Integer, primary_key=True, autoincrement=True)
	external_id = Column(BigInteger, nullable=True, index=True, unique=True)
	amount = Column(Float, nullable=False)
	asset_id = Column(Integer, ForeignKey("asset.id"), nullable=False, index=True)
	exchange_id = Column(Integer, ForeignKey("exchange.id"), nullable=False, index=True)
	locked = Column(Boolean, nullable=False, default=False)
	active = Column(Boolean, nullable=False, default=True)

	asset = relationship("Asset")
	exchange = relationship("Exchange")


class TestFund(Base):
	__tablename__ = "test_fund"

	fund_id = Column(Integer, ForeignKey("fund.id"), primary_key=True)
	test_id = Column(Integer, ForeignKey("test.id"), primary_key=True)
