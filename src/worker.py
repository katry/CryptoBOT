from time import sleep
from asyncio import get_event_loop
from threading import Thread
from multiprocessing import Process, Queue
from .lib.logging import Logging
from .lib.socketwrap import SocketWrap
from .model import (
	Database,
	Exchange as ExchangeModel,
	Pair as PairModel
)
from .module.exchanges import exchanges
from .module.server import Server
from .module.manager import Manager
from .module.trader import Trader


class Worker(Logging):
	_config: dict = {}
	_traders: dict = {}
	_manager = None
	_exchange = None
	_server = None
	_thread = None
	__loop = None

	def __init__(self, config, autostart=True):
		super().__init__(config, logger=config["type"])
		self._config = config
		Database.init("%s/data.db" % config["workdir"])
		self._queue = Queue()
		self._thread = Thread(target=self._receive)
		self._thread.start()

		if "server" in self._config:
			self.logger.info("Starting server")
			SocketWrap.send = self.send
			send_queue = Queue()
			self._server = {
				"process": Process(target=Server, args=(self._config, self._queue, send_queue)),
				"queue": send_queue
			}
			self._server["process"].start()

		send_queue = Queue()
		self._manager = {
			"process": Process(target=Manager, args=(
				self._config,
				self._queue,
				send_queue
			)),
			"queue": send_queue,
			"ready": False
		}
		self._manager["process"].start()

		if autostart and self._config["autostart"]:
			self.start()

	def start(self):
		exchange = ExchangeModel.query.filter_by(name="binance").one()
		self._exchange = exchanges[exchange.name](self._config, exchange=exchange)

		pairs = PairModel.query.filter_by(active=True).all()
		for pair in pairs:
			send_queue = Queue()
			self._traders[pair["symbol"]] = {
				"process": Process(target=Trader, args=(
					self._config,
					pair.symbol,
					[self._exchange.id],
					self._queue,
					send_queue,
				)),
				"queue": send_queue,
				"ready": False
			}
			self._traders[pair.symbol]["process"].start()

		self.logger.info("READY")
		self.__loop = get_event_loop()
		self.__loop.run_until_complete(self._exchange.subscribe(self._traders.keys(), self._send_candle))

	def stop(self):
		if self.__loop:
			self.__loop.stop()
			while not self.__loop.is_closed():
				sleep(0.1)
		for symbol in self._traders.keys():
			self._queue.send({"action": "stop"}, target=symbol)
		while True:
			ready = False
			for trader in self._traders.values():
				if trader["ready"]:
					ready = True
					break
				elif "process" in self._traders[trader]:
					self._traders[trader]["process"].terminate()
					self._traders[trader].pop("process")
			if not ready:
				break
			sleep(0.1)

		self._traders = {}
		self._exchange = None
		self.__loop = None

	def _send_candle(self, candle, symbol):
		self.send({"action": "candle", "candle": candle}, target=symbol)

	def _order(self, data):
		if "order" not in data or "exchange_id" not in data:
			return
		# validity checker
		order = self._exchange.create_order(**data["order"])
		if "order-id" not in order:
			return
		request = {
			"action": "update-position",
			"order": {**order},
			"exchange_id": self._exchange.id
		}
		self.send(request, data["symbol"])
		self.send(request, "manager")

	def request_refresh_positions(self, data):
		if "symbol" not in data or "exchanges" not in data:
			return
			update = self._exchange.check_order(data["symbol"])
		if not update:
			return
		request = {
			"action": "update-positions",
			"positions": update,
			"exchange_id": self._exchange.id
		}
		self.send(request, data["symbol"])
		self.send(request, "manager")

	def send(self, data, target):
		if target == "server":
			self._server.queue.put(data)
		elif target == "manager":
			self._managet.send(data)
		else:
			for trader in self._traders:
				if target == trader:
					self._traders[trader]["queue"].put(data)

	def _receive(self):
		while True:
			self._trigger(self._queue.get())

	def _trigger(self, data, skip_log=False):
		actions = {
			"ready": self._ready,
			"stopped": self._stopped,

			"create-order": self._order,
			"refresh-positions-request": self._request_refresh_positions,

			"sync": lambda x: x,
		}
		action = actions.get(data["action"])
		if action:
			action(data)
		elif not skip_log:
			self.logger.warning("Invalid trigger operation - %s" % repr(data))

	def _ready(self, data):
		if data["alias"] == "manager":
			self._manager["ready"] = True
		elif data["alias"] in self._traders:
			self._traders[data["alias"]]["ready"] = True
			self.send({
				"action": "graph",
				"graph": self._exchange.get_graph(data["alias"])
			}, target=data["alias"])

	def _stopped(self, data):
		if data["alias"] == "manager":
			self._manager["ready"] = False
		elif data["alias"] in self._traders:
			self._traders[data["alias"]]["ready"] = False
