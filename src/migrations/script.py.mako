"""
Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Created: ${create_date}
Message: ${message}
"""

from os import path
from re import sub
import sys
from alembic import op
import sqlalchemy as sa

sys.path.insert(0, sub(r"migrations/.+\.py", "", path.realpath(__file__)))
import model  # noqa
from model import (
	PositionStatus,
	PositionType,
	TestStatus
)
${imports if imports else ""}

revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


def upgrade() -> None:
	${upgrades if upgrades else "pass"}


def downgrade() -> None:
	${downgrades if downgrades else "pass"}

