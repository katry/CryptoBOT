import re
import sys
from os import path
from logging.config import fileConfig
from sqlalchemy import engine_from_config
from sqlalchemy import pool
from alembic.script import write_hooks
from alembic import context

sys.path.insert(0, path.realpath(__file__).replace("/migrations/env.py", ""))

import model


class CustomUpdate:
	version = ""
	table = ""
	trigger_counter = 0
	updates = {
		"test": (
			(
				"'status', model.base.IntEnumSQL()",
				"'status', model.base.IntEnumSQL(TestStatus)",
			),
		),
		"position": (
			(
				"'type', model.base.IntEnumSQL()",
				"'type', model.base.IntEnumSQL(PositionType)"
			),
			(
				"'status', model.base.IntEnumSQL()",
				"'status', model.base.IntEnumSQL(PositionStatus)",
			)
		)
	}

	@staticmethod
	def check(line):
		match = re.search(r".*op\.create_table\([\"\']([a-zA-Z0-9_]+)[\"\'],", line)
		if match:
			CustomUpdate.table = match.groups(0)[0]
		elif len(line) > 1 and line[0:2] == "\t)":
			CustomUpdate.table = ""
		if CustomUpdate.table in CustomUpdate.updates:
			for i in CustomUpdate.updates[CustomUpdate.table]:
				line = line.replace(i[0], i[1])
		return line

	@staticmethod
	def triggers(line):
		version = re.search(r"Revision ID: (.*)", line)
		if version:
			CustomUpdate.version = version.groups(0)[0]
		if (
			CustomUpdate.version and
			(CustomUpdate.version[:5] == "1.0.0" or
			CustomUpdate.version[:6] == "v1.0.0") and
			"end Alembic commands" in line
		):
			if not CustomUpdate.trigger_counter:
				CustomUpdate.trigger_counter = 1
				triggers = (
					"\tmodel.tg_position_update(target=None, bind=op.get_bind())\n"
					"\tmodel.tg_position_clear(target=None, bind=op.get_bind())\n"
				)
				return triggers + line
		return line


@write_hooks.register("customize")
def customize(filename, options):
	lines = []
	add_tabs = 0
	with open(filename) as file_:
		for line in file_:
			new_line = re.sub(r"^(    )+", lambda m: "\t" * (len(m.group(1)) // 4), line)
			tab = 0
			if new_line.count("(") < new_line.count(")"):
				add_tabs -= 1
			while tab < add_tabs:
				new_line = "\t" + new_line
				tab += 1
			if new_line.count("(") > new_line.count(")"):
				add_tabs += 1
			new_line = CustomUpdate.check(new_line)
			new_line = CustomUpdate.triggers(new_line)
			lines.append(new_line.replace("\t   ", "\t\t"))
	with open(filename, "w") as to_write:
		to_write.write("".join(lines))


config = context.config
if config.config_file_name is not None:
	fileConfig(config.config_file_name)

target_metadata = model.base.Base.metadata  # noqa


def run_migrations_offline():
	url = config.get_main_option("sqlalchemy.url")
	context.configure(
		url=url,
		target_metadata=target_metadata,
		literal_binds=True,
		dialect_opts={"paramstyle": "named"},
	)

	with context.begin_transaction():
		context.run_migrations()


def run_migrations_online():
	connectable = engine_from_config(
		config.get_section(config.config_ini_section),
		prefix="sqlalchemy.",
		poolclass=pool.NullPool,
	)

	with connectable.connect() as connection:
		context.configure(
			connection=connection, target_metadata=target_metadata
		)

		with context.begin_transaction():
			context.run_migrations()


if context.is_offline_mode():
	run_migrations_offline()
else:
	run_migrations_online()
