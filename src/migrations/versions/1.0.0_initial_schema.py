"""
Revision ID: 1.0.0
Revises: 
Created: 2022-09-09 22:07:56.353363
Message: initial_schema
"""

from os import path
from re import sub
import sys
from json import dumps
from alembic import op
import sqlalchemy as sa

sys.path.insert(0, sub(r"migrations/.+\.py", "", path.realpath(__file__)))
import model  # noqa
from model import (  # noqa
		PositionStatus,
		PositionType,
		TestStatus
)


revision = '1.0.0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
	op.create_table('asset',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('symbol', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_asset'))
	)
	op.create_index(op.f('ix_asset_symbol'), 'asset', ['symbol'], unique=True)
	op.create_table('exchange',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('external_id', sa.BigInteger(), nullable=True),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('fee', sa.Float(), nullable=False),
		sa.Column('watch_intervals', sa.JSON(), nullable=False),
		sa.Column('graph_length', sa.SmallInteger(), nullable=False),
		sa.Column('active', sa.Boolean(), nullable=False),
		sa.Column('api', sa.JSON(), nullable=True),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_exchange'))
	)
	op.create_index(op.f('ix_exchange_fee'), 'exchange', ['fee'], unique=False)
	op.create_index(op.f('ix_exchange_name'), 'exchange', ['name'], unique=True)
	op.create_table('indicator',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('external_id', sa.BigInteger(), nullable=True),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('version', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_indicator')),
		sa.UniqueConstraint('name', 'version', name='uq_indicator_name_version')
	)
	op.create_index(op.f('ix_indicator_external_id'), 'indicator', ['external_id'], unique=True)
	op.create_index(op.f('ix_indicator_name'), 'indicator', ['name'], unique=False)
	op.create_index(op.f('ix_indicator_version'), 'indicator', ['version'], unique=False)
	op.create_table('strategy',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('external_id', sa.BigInteger(), nullable=True),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('version', sa.String(), nullable=False),
		sa.Column('active', sa.Boolean(), nullable=False),
		sa.Column('priority', sa.SmallInteger(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_strategy')),
		sa.UniqueConstraint('name', 'version', name='uq_strategy_name_version')
	)
	op.create_index(op.f('ix_strategy_external_id'), 'strategy', ['external_id'], unique=True)
	op.create_index(op.f('ix_strategy_name'), 'strategy', ['name'], unique=False)
	op.create_table('test',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('external_id', sa.BigInteger(), nullable=False),
		sa.Column('status', model.base.IntEnumSQL(TestStatus), nullable=False),
		sa.Column('config', sa.JSON(), nullable=False),
		sa.Column('trading_start', sa.Date(), nullable=True),
		sa.Column('trading_stop', sa.Date(), nullable=True),
		sa.Column('started', sa.DateTime(), nullable=False),
		sa.Column('stopped', sa.DateTime(), nullable=True),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_test'))
	)
	op.create_index(op.f('ix_test_external_id'), 'test', ['external_id'], unique=True)
	op.create_index('ix_test_started_desc', 'test', [sa.text('started DESC')], unique=False)
	op.create_index(op.f('ix_test_status'), 'test', ['status'], unique=False)
	op.create_index('ix_test_stopped_desc', 'test', [sa.text('stopped DESC')], unique=False)
	op.create_table('fund',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('external_id', sa.BigInteger(), nullable=True),
		sa.Column('amount', sa.Float(), nullable=False),
		sa.Column('asset_id', sa.Integer(), nullable=False),
		sa.Column('exchange_id', sa.Integer(), nullable=False),
		sa.Column('locked', sa.Boolean(), nullable=False),
		sa.Column('active', sa.Boolean(), nullable=False),
		sa.ForeignKeyConstraint(['asset_id'], ['asset.id'], name=op.f('fk_fund_asset_id_asset')),
		sa.ForeignKeyConstraint(['exchange_id'], ['exchange.id'], name=op.f('fk_fund_exchange_id_exchange')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_fund'))
	)
	op.create_index(op.f('ix_fund_external_id'), 'fund', ['external_id'], unique=True)
	op.create_index(op.f('ix_fund_asset_id'), 'fund', ['asset_id'], unique=False)
	op.create_index(op.f('ix_fund_exchange_id'), 'fund', ['exchange_id'], unique=False)
	op.create_table('pair',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('symbol', sa.String(), nullable=False),
		sa.Column('base_asset_id', sa.Integer(), nullable=False),
		sa.Column('quote_asset_id', sa.Integer(), nullable=False),
		sa.Column('active', sa.Boolean(), nullable=False),
		sa.ForeignKeyConstraint(['base_asset_id'], ['asset.id'], name=op.f('fk_pair_base_asset_id_asset')),
		sa.ForeignKeyConstraint(['quote_asset_id'], ['asset.id'], name=op.f('fk_pair_quote_asset_id_asset')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_pair')),
		sa.UniqueConstraint('base_asset_id', 'quote_asset_id', name='uq_pair_base_quote_id')
	)
	op.create_index(op.f('ix_pair_symbol'), 'pair', ['symbol'], unique=True)
	op.create_table('test_strategy',
		sa.Column('strategy_id', sa.Integer(), nullable=False),
		sa.Column('test_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['strategy_id'], ['strategy.id'], name=op.f('fk_test_strategy_strategy_id_strategy')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_strategy_test_id_test')),
		sa.PrimaryKeyConstraint('strategy_id', 'test_id', name=op.f('pk_test_strategy'))
	)
	op.create_table('position',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('type', model.base.IntEnumSQL(PositionType), nullable=False),
		sa.Column('status', model.base.IntEnumSQL(PositionStatus), nullable=False),
		sa.Column('amount', sa.Float(), nullable=False),
		sa.Column('open_price', sa.Float(), nullable=False),
		sa.Column('close_price', sa.Float(), nullable=True),
		sa.Column('open_time', sa.DateTime(), nullable=False),
		sa.Column('close_time', sa.DateTime(), nullable=True),
		sa.Column('order_id', sa.BigInteger(), nullable=True),
		sa.Column('data', sa.JSON(), nullable=True),
		sa.Column('check', sa.Boolean(), nullable=False),
		sa.Column('close_trigger', sa.Text(), nullable=True),
		sa.Column('modified', sa.DateTime(), nullable=False),
		sa.Column('pair_id', sa.Integer(), nullable=False),
		sa.Column('fund_id', sa.Integer(), nullable=False),
		sa.Column('exchange_id', sa.Integer(), nullable=False),
		sa.Column('strategy_id', sa.Integer(), nullable=True),
		sa.ForeignKeyConstraint(['exchange_id'], ['exchange.id'], name=op.f('fk_position_exchange_id_exchange')),
		sa.ForeignKeyConstraint(['fund_id'], ['fund.id'], name=op.f('fk_position_fund_id_fund')),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_position_pair_id_pair')),
		sa.ForeignKeyConstraint(['strategy_id'], ['strategy.id'], name=op.f('fk_position_strategy_id_strategy')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_position')),
		sa.UniqueConstraint('order_id', 'exchange_id', name='uq_position_order_exchange_id')
	)
	op.create_index('ix_position_close_time_desc', 'position', [sa.text('close_time DESC')], unique=False)
	op.create_index(op.f('ix_position_exchange_id'), 'position', ['exchange_id'], unique=False)
	op.create_index('ix_position_modified_desc', 'position', [sa.text('modified DESC')], unique=False)
	op.create_index('ix_position_open_time_desc', 'position', [sa.text('open_time DESC')], unique=False)
	op.create_index(op.f('ix_position_order_id'), 'position', ['order_id'], unique=False)
	op.create_index(op.f('ix_position_pair_id'), 'position', ['pair_id'], unique=False)
	op.create_index(op.f('ix_position_status'), 'position', ['status'], unique=False)
	op.create_table('test_fund',
		sa.Column('fund_id', sa.Integer(), nullable=False),
		sa.Column('test_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['fund_id'], ['fund.id'], name=op.f('fk_test_fund_fund_id_fund')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_fund_test_id_test')),
		sa.PrimaryKeyConstraint('fund_id', 'test_id', name=op.f('pk_test_fund'))
	)
	op.create_table('test_pair',
		sa.Column('pair_id', sa.Integer(), nullable=False),
		sa.Column('test_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_test_pair_pair_id_pair')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_pair_test_id_test')),
		sa.PrimaryKeyConstraint('pair_id', 'test_id', name=op.f('pk_test_pair'))
	)
	op.create_table('test_position',
		sa.Column('position_id', sa.Integer(), nullable=False),
		sa.Column('test_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['position_id'], ['position.id'], name=op.f('fk_test_position_position_id_position')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_position_test_id_test')),
		sa.PrimaryKeyConstraint('position_id', 'test_id', name=op.f('pk_test_position'))
	)
	model.tg_position_update(target=None, bind=op.get_bind())
	model.tg_position_clear(target=None, bind=op.get_bind())

	intervals = [
			"1m", "3m", "5m", "15m", "30m", "1h", "2h", "4h",
			"6h", "8h", "12h", "1d", "3d", "1w", "1M"
	]

	# exchanges
	op.execute("""
		INSERT INTO exchange
		(name, fee, watch_intervals, graph_length, active, api)
		VALUES ('binance', 0.1, '%s', 1000, TRUE, NULL)
	""" % dumps(intervals))

	# indicators
	op.execute("""
		INSERT INTO indicator (id, name, version) VALUES
		(1, 'ma', '1.0.0');
	""")
	op.execute("""
		INSERT INTO indicator (id, name, version) VALUES
		(2, 'rsi', '1.0.0');
	""")
	op.execute("""
		INSERT INTO indicator (id, name, version) VALUES
		(3, 'macd', '1.0.0');
	""")
	op.execute("""
		INSERT INTO indicator (id, name, version) VALUES
		(4, 'stoch', '1.0.0');
	""")
	op.execute("""
		INSERT INTO indicator (id, name, version) VALUES
		(5, 'mom', '1.0.0');
	""")


def downgrade() -> None:
	op.drop_table('test_position')
	op.drop_table('test_pair')
	op.drop_table('test_fund')
	op.drop_index(op.f('ix_position_status'), table_name='position')
	op.drop_index(op.f('ix_position_pair_id'), table_name='position')
	op.drop_index(op.f('ix_position_order_id'), table_name='position')
	op.drop_index('ix_position_open_time_desc', table_name='position')
	op.drop_index('ix_position_modified_desc', table_name='position')
	op.drop_index(op.f('ix_position_exchange_id'), table_name='position')
	op.drop_index('ix_position_close_time_desc', table_name='position')
	op.drop_table('position')
	op.drop_table('test_strategy')
	op.drop_index(op.f('ix_pair_symbol'), table_name='pair')
	op.drop_table('pair')
	op.drop_index(op.f('ix_fund_exchange_id'), table_name='fund')
	op.drop_index(op.f('ix_fund_asset_id'), table_name='fund')
	op.drop_table('fund')
	op.drop_index('ix_test_stopped_desc', table_name='test')
	op.drop_index(op.f('ix_test_status'), table_name='test')
	op.drop_index('ix_test_started_desc', table_name='test')
	op.drop_index(op.f('ix_test_external_id'), table_name='test')
	op.drop_table('test')
	op.drop_index(op.f('ix_strategy_name'), table_name='strategy')
	op.drop_index(op.f('ix_strategy_external_id'), table_name='strategy')
	op.drop_table('strategy')
	op.drop_index(op.f('ix_indicator_version'), table_name='indicator')
	op.drop_index(op.f('ix_indicator_name'), table_name='indicator')
	op.drop_index(op.f('ix_indicator_external_id'), table_name='indicator')
	op.drop_table('indicator')
	op.drop_index(op.f('ix_exchange_name'), table_name='exchange')
	op.drop_index(op.f('ix_exchange_fee'), table_name='exchange')
	op.drop_table('exchange')
	op.drop_index(op.f('ix_asset_symbol'), table_name='asset')
	op.drop_table('asset')

