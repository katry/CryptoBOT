from datetime import datetime, timedelta
from ..lib.logging import Logging
from ..lib.validator import Validator
from ..lib.decorators import hybridproperty
from ..model import (
	Fund as FundModel,
	Position as PositionModel,
	PositionStatus as Status,
	PositionType as Type,
)


class Position(Logging):
	__request_validator: dict = {
		"type": (
			[str, Type, "optional"],
			lambda x, *a: x.lower() in ["long", "short"] if type(x) == str else True
		),
		"price": ([float, "optional"], lambda x, *a: x > 0)
	}
	__model_validator: dict = {}

	def __init__(
		self, config, model, pair, fund_id, exchange, strategy,
		sender, buffer=[], skip_open=False
	):
		super().__init__(config)
		if isinstance(model, PositionModel):
			self.__model = model
		elif type(model) == dict and Validator.check(model, self.__model_validator):
			self.__model = PositionModel.from_dict(model)
		else:
			raise ValueError("Invalid position model")
		self.__pair = pair
		self.__fund = FundModel.get(fund_id)
		self.__exchange = exchange
		self.__strategy = strategy
		self.__sender = sender
		self.__buffer = buffer
		if not skip_open and self.__model.status == Status.PENDING:
			self.__open()

	def __iter__(self):
		model = dict(self.__model)
		for key in model:
			yield (key, model[key])

	def __open(self):
		if self.type == Type.LONG:
			amount = self.__fund.amount / self.open_price
		else:
			amount = self.__fund.amount
		order = {
			"action": "create-order",
			"order": {
				"symbol": self.__pair.symbol,
				"side": "BUY" if self.type == Type.LONG else "SELL",
				"price": self.open_price,
				"amount": amount,
				"id": self.id,
				"action": "OPEN"
			},
			"exchange": self.__exchange.name
		}
		self.__send(order, target="main")
		self.logger.info("OPEN REQUEST: %s" % repr(order))

	def __close(self, request=None, cancel=False):
		if self.type == Type.LONG:
			amount = self.__fund.amount / self.open_price  # TODO count fee
		else:
			amount = self.__fund.amount
		if cancel:
			if self.__model.status == Status.OPEN:
				order = {
					"action": "create-order-market",
					"order": {
						"symbol": self.__pair.symbol,
						"side": "SELL" if self.type == Type.LONG else "BUY",
						"amount": amount,
						"id": self.id,
						"action": "CLOSE"
					},
					"symbol": self.__pair.symbol,
					"exchange_id": self.__exchange.id
				}
				self.__model.close_price = self.__pair.price
			else:
				order = None
			self.__model.status = Status.CANCELED
		else:
			price = request.get("price", self.__pair.price)
			order = {
				"action": "create-order",
				"order": {
					"symbol": self.__pair.symbol,
					"side": "SELL" if self.type == Type.LONG else "BUY",
					"price": price,
					"amount": amount,
					"id": self.id,
					"action": "CLOSE"
				},
				"symbol": self.__pair.symbol,
				"exchange_id": self.__exchange.id
			}
			self.__model.close_price = price
			self.__model.status = Status.PENDING
		self.__model.update()
		if order:
			self.__send(order, target="main")
		self.logger.info("CLOSE REQUEST: %s" % repr(order))

	def __refresh(self):
		if len(self.__buffer) == 1:
			data = self.__buffer.pop()
			if "status" in data:
				self.__model.status = data["status"]
			self.__model.update()
			if data["status"] == Status.CLOSED:
				pass
		else:
			self.logger.error("Wrong buffer data %s" % repr(self.__buffer))

	@staticmethod
	def __parse_request(request, pair, close=False) -> dict | bool:
		if (
			type(request) != dict or
			not Validator.check(request, Position.__request_validator) or
			(not close and "type" not in request)
		):
			return False
		if "price" not in request:
			request["price"] = pair.price
		if close:
			return request
		if type(request["type"]) != Type:
			request["type"] = (
				Type.LONG if request["type"].lower() == "long" else Type.SHORT
			)
		if request["type"] == Type.LONG:
			request["asset"] = pair.quote_asset
		else:
			request["asset"] = pair.base_asset
		request["type"] = int(request["type"])
		request["time"] = pair.time
		return request

	def __send(self, data, target=None):
		data["target"] = target
		self.__sender.put(data)

	@property
	def close(self):
		return self.__close

	@property
	def refresh(self):
		return self.__refresh

	@hybridproperty
	def parse_request(target):
		return target.__parse_request

	@property
	def id(self):
		return self.__model.id

	@property
	def type(self):
		return self.__model.type

	@property
	def status(self):
		return self.__model.status

	@property
	def exchange(self):
		return dict(self.__exchange)

	@property
	def strategy(self):
		return self.__strategy

	@property
	def open_time(self):
		return self.__model.open_time

	@property
	def close_time(self):
		return self.__model.close_time

	@property
	def open_price(self):
		return self.__model.open_price

	@property
	def close_price(self):
		return self.__model.close_price

	@property
	def fund(self):
		return dict(self.__fund)

	@property
	def order_id(self):
		return self.__model.order_id

	@property
	def skip(self):
		return not self.__model.check

	@property
	def close_trigger(self):
		try:
			props = {
				"position": self,
				"time": datetime.now(),
				"datetime": datetime,
				"timedelta": timedelta,
			}
			result = eval(self.__model.close_trigger, {}, props)
		except Exception as e:
			self.logger.error("Close_trigger exception (position id: %s) %s" % (repr(e), self.id))
			result = False  # TODO: log
		return result
