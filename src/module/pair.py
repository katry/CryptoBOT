from datetime import datetime
from copy import deepcopy
from ..model import Pair as PairModel


class Pair:
	__intervals: list = []
	__graph: dict = {}

	def __init__(self, symbol, exchange):
		self.__exchange = {
			"id": exchange.id,
			"name": exchange.name,
			"fee": exchange.fee,
		}
		self.__graph_length = exchange.graph_length
		self.__model = PairModel.query.filter_by(symbol=symbol).one()
		self.__price = None

	def __update(self, candle):
		if not self.__graph:
			return
		self.__price = candle["close"]
		self.__time = datetime.fromtimestamp(candle["time"] / 1000)
		closed = candle.pop("closed", False)
		for interval in self.__intervals:
			last = self.__graph[interval][-1]
			if candle["open_time"] > last["close_time"] and not closed:
				self.__graph[interval].append(self.__create_candle(candle))
				if interval == self.__intervals[0]:
					self.__update_volume(last)
				if len(self.__graph[interval]) > self.__graph_length:
					self.__graph[interval] = self.__graph[interval][1:]
			else:
				self.__update_candle(last, candle, interval)

	@staticmethod
	def __create_candle(last, new):
		candle = new.copy()
		interval = last["close_time"] - last["open_time"]
		candle["open_time"] = new["open_time"]
		candle["close_time"] = new["open_time"] + interval
		return candle

	def __update_candle(self, last, new, interval):
		if interval == self.__intervals[0]:
			last["volume"] = new["volume"]
		last["close"] = new["close"]
		if new["close"] > last["high"]:
			last["high"] = new["close"]
		elif new["close"] < last["low"]:
			last["low"] = new["close"]

	def __update_volume(self, last):
		for i in self.__intervals:
			if i == self.__intervals[0]:
				continue
			self.__graph[i][-1]["volume"] += last["volume"]

	def __load(self, graph: dict):
		if self.__graph:
			return
		self.__intervals = list(graph.keys())
		self.__intervals.sort(
			key=lambda x: int(x[:-1]) * {"m": 1, "h": 60, "d": 1440, "w": 7 * 1440, "M": 30 * 1440}[x[-1]]
		)
		self.__graph = deepcopy(graph)

	def __candle(self, interval):
		if interval in self.__graph and len(self.__graph[interval]):
			return deepcopy(self.__graph[interval])[-1]
		return False

	def __candles(self, interval, count: int = 100):
		if interval in self.__graph and count > 0:
			return deepcopy(self.__graph[interval])[-count:]
		return False

	def __interval(self, interval):
		if interval in self.__graph:
			return deepcopy(self.__graph[interval])
		return False

	@property
	def load(self):
		return self.__load

	@property
	def update(self):
		return self.__update

	@property
	def candle(self):
		return self.__candle

	@property
	def candles(self):
		return self.__candles

	@property
	def interval(self):
		return self.__interval

	@property
	def last(self):
		values = {}
		for interval in self.__graph:
			if len(self.__graph[interval]):
				values[interval] = deepcopy(self.__graph[interval][-1])
		return values

	@property
	def graph(self):
		return deepcopy(self.__graph)

	@property
	def price(self):
		return self.__price

	@property
	def time(self):
		return self.__time

	@property
	def exchange(self):
		return self.__exchange.copy()

	@property
	def symbol(self):
		return self.__model.symbol

	@property
	def id(self):
		return self.__model.id

	@property
	def base_asset(self):
		return self.__model.base_asset.symbol

	@property
	def quote_asset(self):
		return self.__model.quote_asset.symbol

	@property
	def base_asset_id(self):
		return self.__model.base_asset_id

	@property
	def quote_asset_id(self):
		return self.__model.quote_asset_id
