from random import randint
from time import sleep
from datetime import datetime
from math import floor
from threading import Thread
from os import listdir, path
from urllib.request import Request, urlopen
from orjson import loads, dumps
from ...model import Position as PositionModel, PositionStatus as Status
from .abstract import AbstractExchange


class TestingExchange(AbstractExchange):
	__pairs: dict = {}
	__orders: dict = {}
	__slowers: dict = {}
	__slow = False

	def __init__(self, config, alias, dataset="_data_basic_2020_09_27"):
		super().__init__(config, alias)
		self.dataset = dataset
		self._date = None

	def clear(self):
		self._account = {}
		self.__pairs = {}
		self.__orders = {}

	def account_status(self, assets={}):
		self._account_status(assets)

	def _account_status(self, assets={}):
		for asset in assets:
			if type(assets[asset]) in (int, float) and asset[asset] > 0:
				self._account[asset] = assets[asset]

	def _set_exchange_limits(self):
		req = Request("https://www.binance.com/api/v1/exchangeInfo")
		with urlopen(req) as response:
			data = loads(response.read())
		for pair in data["symbols"]:
			if pair["filters"][2]["filterType"] != "LOST_SIZE":
				continue
			minval = pair["filters"][2]["stepSize"]
			self._steps[pair["symbol"]] = len(minval.replace(".", "").split("1")[0])
			self._minimums[pair["symbol"]] = float(minval)

	@staticmethod
	def _parse_status(status, action):
		if status == "NEW":
			return Status.PENDING
		elif status == "PARTIALY_FILLED":
			return Status.PARTIALY_FILLED
		elif status == "FILLED" and action == "OPEN":
			return Status.OPEN
		elif status == "FILLED" and action == "CLOSE":
			return Status.CLOSE
		elif status == ["REJECTED", "EXPIRED"]:
			return Status.REJECTED
		else:
			return Status.CANCELED

	def create_order(self, symbol, amount, price, side, id=None, action="OPEN"):
		data = {
			"symbol": symbol,
			"side": side,
			"type": "LIMIT",
			"timeInForce": "GTC",
			"quantity": floor(amount * (10 ** self._steps[symbol])) / (10 ** self._steps[symbol]),
			"price": price,
		}

		if symbol not in self.__pairs or data["quantity"] > self._account.get(symbol, 0):
			status = self._parse_status("REJECTED", action)
		if price < self.__pairs[symbol] if side == "BUY" else price > self.__pairs[symbol]:
			status = self._parse_status("FILLED", action)
		elif price == self.__pairs[symbol]:
			status = self._parse_status("PARTIALY_FILLED", action)
		else:
			status = self._parse_status("NEW", action)
		while (
			PositionModel.query
			.filter_by(exchange_id=self._model.id)
			.filter_by(order_id=(order_id := randint(1E3, 1E12)))
			.count()
		):
			order_id = randint(1E3, 1E12)
		order = {
			"order-id": order_id,
			"status": status,
			"amount": amount,
			"price": price,
			"side": side
		}
		if status in [Status.PENDING, Status.PARTIALY_FILLED]:
			if symbol not in self.__orders:
				self.__orders[symbol] = []
			self.__orders[symbol].append({**order, "action": action, "id": id, "status": int(status)})
		return order

	def check_orders(self, symbol):
		updated = []
		for o in self.__orders[symbol].copy():
			if o["status"] not in [Status.PENDING, Status.PARTIALY_FILLED]:
				self.__orders[symbol].pop(o)
				updated.append({"id": o["id"], "status": int(o["status"])})
		return updated

	def check_order(self, order):
		for o in self.__orders[order["symbol"]]:
			if o["order-id"] == order["order-id"]:
				return o["status"]
		return Status.FILLED

	def cancel_order(self, order):
		for o in self.__orders[order["symbol"]]:
			if o["order-id"] == order["order-id"]:
				self.__orders[order["symbol"]].remove(o)
				return True
		return False

	def get_graph_interval(self, symbol, interval, length=1000, raw=False):
		tpl = (self._config["workdir"], self._model.name, self.dataset, symbol)
		file = open("%s/data/%s/%s/%s_graph.json" % tpl, "rb")
		data = loads(file.read())
		file.close()
		graph = []
		for c in data[interval]:
			graph.append({
				"open_time": c[0],
				"close_time": c[6],
				"open": float(c[1]),
				"close": float(c[4]),
				"high": float(c[2]),
				"low": float(c[3]),
				"volume": float(c[5])
			})
		return graph

	async def subscribe(self, symbols, callback, raw=False, start=None, end=None):
		queue = []
		loaded = {}
		files = {}
		for symbol in symbols:
			if symbol not in files:
				tpl = (self._config["workdir"], self._model.name, self.dataset, symbol)
				pth = "%s/data/%s/%s/%s/" % tpl
				files[symbol] = [pth + f for f in listdir(pth) if path.isfile(pth + f)]
				files[symbol].sort()
				loaded[symbol] = self._parse_stream_file(files[symbol].pop(0))
				queue.append(loaded[symbol].pop(0))

		trade_from = datetime.timestamp(start) if start else None
		trade_to = datetime.timestamp(end) + 24 * 3600 if end else None
		previous = None
		while True:
			low = None
			to_send = None
			new_queue = queue.copy()
			for data in queue:
				if not low or data["data"]["E"] < low:
					low = data["data"]["E"]
					if to_send:
						new_queue.insert(0, to_send)
					to_send = data
					new_queue.remove(data)
			if self.__slow and previous:
				sleep(low - previous["data"]["E"])
			u = Thread(target=self._update, args=(to_send,))
			u.start()
			if trade_from and low / 1000 < trade_from:
				continue
			elif trade_to and low / 1000 > trade_to:
				break
			if raw:
				t = Thread(target=callback, args=(dumps(to_send),))
			else:
				t = Thread(target=self._process, args=(to_send, callback))
			t.start()
			low = None
			to_send = None
			queue = new_queue
			symbol = to_send["data"]["s"]
			if not len(loaded[symbol]):
				if len(files[symbol]):
					loaded[symbol] = self._parse_stream_file(files[symbol].pop(0))
				else:
					empty = [True for s in loaded if not len(loaded[s])]
					if len(empty) == len(loaded.keys()):
						break
			if len(loaded[symbol]):
				queue.append(loaded[symbol].pop(0))
			previous = to_send

		callback(None, None)

	@staticmethod
	def _parse_stream_file(path):
		file = open(path, "rb")
		txt = file.read()
		file.close()
		return loads(txt)

	@staticmethod
	def _process(data, callback):
		symbol = data["s"]
		data = data["k"]
		candle = {
			"open_time": data["t"],
			"close_time": data["T"],
			"open": float(data["o"]),
			"close": float(data["c"]),
			"high": float(data["h"]),
			"low": float(data["l"]),
			"volume": float(data["v"]),
			"closed": data["x"]
		}
		callback(candle, symbol)

	def _update(self, data):
		symbol = data["s"]
		price = float(data["k"]["c"])
		self.__pairs[symbol] = price
		if symbol in self.__orders:
			for order in self.__orders[symbol]:
				if order["status"] not in [Status.PENDING, Status.PARTIALY_FILLED]:
					continue
				if order["price"] < price if order["side"] == "BUY" else order["price"] > price:
					order["status"] = Status.OPEN if order["action"] == "OPEN" else Status.CLOSED

	def slow_down(self, sender):
		self.__slow = True
		self.logger.info("SLOWING due to %s at %s" % (sender, datetime.utcnow().strftime("%H:%M:%S")))
		if sender in self.__slowers:
			self.__slowers[sender] += 1
		else:
			self.__slowers[sender] = 1

	def speed_up(self, sender):
		if sender not in self.__slowers or not self.__slowers["slower"]:
			return
		self.logger.info("REMOVE SLOWING for %s at %s" % (sender, datetime.utcnow().strftime("%H:%M:%S")))
		self.__slowers[sender] -= 1
		for key in self.__slowers:
			if self.__slowers[key]:
				return
		self.__slow = False

	@property
	def date(self):
		return self._date
