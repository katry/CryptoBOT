from .binance import Binance
from .testing import TestingExchange

exchanges = {
	"binance": Binance,
	"testing": TestingExchange
}
