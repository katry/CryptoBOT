from ...lib.logging import Logging
from copy import deepcopy


class AbstractExchange(Logging):
	_api: dict = {}
	_skip_request = False
	_endpoint = ""
	_steps: dict = {}
	_minimums: dict = {}
	_account: dict = {}
	_model = None

	def __init__(self, config, exchange):
		super().__init__(config, logger=exchange.name)
		self._config = config
		self._model = exchange
		if config["type"] in ("tester", "collector", "notifier"):
			self._skip_request = True

		if exchange.api:
			self._api = exchange.api
			self._account_status()
		else:
			self._account = {}
		self._set_exchange_limits()

	def _account_status(self):
		raise NotImplementedError

	def _set_exchange_limits(self):
		raise NotImplementedError

	def buy(self, symbol, price, amount=None, fromAmount=None):
		if fromAmount:
			amount = self.long_amount_convert(price, fromAmount)
		return self.create_order(symbol, amount, price, "BUY")

	def sell(self, symbol, price, amount=None, fromAmount=None):
		if fromAmount:
			amount = self.long_amount_convert(price, fromAmount)
		return self.create_order(symbol, amount, price, "SELL")

	def long_amount_convert(self, price, amount):
		return amount / price

	def create_order(self, symbol, amount, price, side, id=None, action="OPEN"):
		raise NotImplementedError

	def check_orders(self, order):
		raise NotImplementedError

	def check_order(self, order):
		raise NotImplementedError

	def cancel_order(self, order):
		raise NotImplementedError

	def get_graph_interval(self, symbol, interval, length=1000, raw=False):
		raise NotImplementedError

	def get_graph(self, symbol, intervals, length=1000, raw=False):
		graph = {}
		for interval in intervals:
			graph[interval] = self.get_graph_interval(symbol, interval, length=length, raw=raw)
		return graph

	async def subscribe(self, symbols, callback, raw=False):
		raise NotImplementedError

	def get_asset(self, asset):
		return deepcopy(self._account.get(asset))

	@property
	def id(self):
		return self._model.id

	@property
	def name(self):
		return self._model.name

	@property
	def steps(self):
		return deepcopy(self._steps)

	@property
	def minimums(self):
		return deepcopy(self._minimums)

	@property
	def account(self):
		return deepcopy(self._account)
