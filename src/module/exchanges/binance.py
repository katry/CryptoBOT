from math import floor
from time import time, sleep
from datetime import datetime
from orjson import loads
from urllib.request import Request, urlopen
from threading import Thread
import hmac
import hashlib
import websockets
from ...model import PositionStatus as Status
from .abstract import AbstractExchange


class Binance(AbstractExchange):
	_orders: dict = {}
	__last_sync = int(datetime.utcnow().strftime("%Y%m%d%H%M%S")) * 1000  # TODO: save and load last sync

	def __init__(self, config, exchange):
		self._endpoint = "https://api.binance.com/api/v3/"
		super().__init__(config, exchange)

	def _account_status(self):
		url = "https://api.binance.com/sapi/v1/accountSnapshot"
		try:
			data = self.request("", {"type": "SPOT"}, method="GET", uri=url)
		except Exception:
			self.logger.info("Account status at %s try again for 1s" % url)
			sleep(1)
			self.account_status()
		for part in data["snapshotVos"]:
			for asset in part["data"]["balances"]:
				self._account[asset["asset"]] = {
					"free": float(asset["free"]),
					"locked": float(asset["locked"]),
				}

	def _set_exchange_limits(self):
		req = Request("https://www.binance.com/api/v1/exchangeInfo")
		with urlopen(req) as response:
			data = loads(response.read())
		for pair in data["symbols"]:
			if pair["filters"][2]["filterType"] != "LOST_SIZE":
				continue
			minval = pair["filters"][2]["stepSize"]
			self._steps[pair["symbol"]] = len(minval.replace(".", "").split("1")[0])
			self._minimums[pair["symbol"]] = float(minval)

	@staticmethod
	def _parse_status(status, action):
		if status == "NEW":
			return Status.PENDING
		elif status == "PARTIALY_FILLED":
			return Status.PARTIALY_FILLED
		elif status == "FILLED" and action == "OPEN":
			return Status.OPEN
		elif status == "FILLED" and action == "CLOSE":
			return Status.CLOSE
		elif status == "FILLED":
			return Status.FILLED
		elif status == ["REJECTED", "EXPIRED"]:
			return Status.REJECTED
		else:
			return Status.CANCELED

	def create_order(self, symbol, amount, price, side, id=None, action="OPEN"):
		action = "order/test" if self._skip_request else "order"
		data = {
			"symbol": symbol,
			"side": side,
			"type": "LIMIT",
			"timeInForce": "GTC",
			"quantity": floor(amount * (10 ** self._steps[symbol])) / (10 ** self._steps[symbol]),
			"price": price,
		}
		resp = self.request(action, data)
		if resp == {} or not self._skip_request:
			order = {"order-id": 1, "status": Status.PENDING, "amount": amount, "price": price, "side": side}
		if not resp.get("code"):
			order = {"order-id": resp["order-id"], "status": int(self._parse_status(resp["status"]), action)}
		else:
			return False
		self._orders[order["order-id"]] = order.copy()
		self._orders[order["order-id"]]["action"] = action
		self._orders[order["order-id"]]["id"] = id
		self._orders[order["order-id"]]["status"] = self._parse_status(resp["status"])
		return order

	def check_orders(self, symbol):
		if self._skip_request:
			return True

		action = "allOrders"
		data = {
			"symbol": symbol,
			"startTime": self.__last_sync,
		}
		resp = self.request(action, data, method="GET")
		if resp and not resp.get("code"):
			self.__last_sync = int(datetime.utcnow().strftime("%Y%m%d%H%M%S")) * 1000
			update = {}
			for item in resp:
				if item["order-id"] not in self._orders:
					continue
				status = self._parse_status(item["status"])
				if status == self._orders[item["order-id"]]["status"]:
					if status in [Status.OPEN, Status.CLOSED, Status.CANCELED, Status.REJECTED]:
						self._orders.pop(item["order-id"])
					continue
				update.append({"id": self._orders[item["order-id"]]["id"], "status": int(status)})
				if status in [Status.OPEN, Status.CLOSED, Status.CANCELED, Status.REJECTED]:
					self._orders.pop(item["order-id"])
				else:
					self._orders[item["order-id"]]["status"] = status
			return update
		else:
			return False

	def check_order(self, order):
		if self._skip_request:
			return True

		action = "order"
		data = {
			"symbol": order["symbol"],
			"orderId": order["order-id"]
		}
		resp = self.request(action, data, method="GET")
		if resp and not resp.get("code"):
			return self._parse_status(resp["status"], action=None)
		else:
			return False

	def cancel_order(self, order):
		if self._skip_request:
			return True

		action = "order"
		data = {
			"symbol": order["symbol"],
			"orderId": order["order-id"],
		}
		resp = self.request(action, data, method="DELETE")
		return True if resp else False

	def request(self, action="", data=None, method="POST", signed=True, url=None):
		params = ""
		if not url:
			url = self._endpoint + action
			data = {}
		if data:
			data["recvWindow"] = 60000
			data["timestamp"] = int(time() * 1000)
			params = "?"
			for key in data:
				params += "%s=%s&" % (key, data[key])
			if signed:
				signature = hmac.new(
					self.__api["SECRET_KEY"],
					params[1:-1].encode(),
					hashlib.sha256
				).hexdigest()
				params += "signature=%s" % signature
			else:
				params = params[:-1]
		try:
			req = Request(url + params, method=method)
			if signed:
				req.add_header("X-MBX-APIKEY", self.__api["API_KEY"])
			with urlopen(req) as response:
				return loads(response.read())
		except Exception:
			self.logger.warning("Unable to connect Binance REST API at URL: %s" % (url + params))
			return False

	def get_graph_interval(self, symbol, interval, length=1000, raw=False):
		try:
			tpl = (self._endpoint, symbol, interval, length)
			data = self.request(
				url="%sklines?symbol=%s&interval=%s&limit=%s" % tpl,
				method="GET",
				signed=False
			)
			if not data:
				raise Exception
		except Exception:
			sleep(1)
			return self.get_graph_interval(symbol, interval, length, raw)

		if raw:
			return data
		graph = []
		for c in data:
			graph.append({
				"open_time": c[0],
				"close_time": c[6],
				"open": float(c[1]),
				"close": float(c[4]),
				"high": float(c[2]),
				"low": float(c[3]),
				"volume": float(c[5])
			})
		return graph

	async def subscribe(self, symbols, callback, raw=False):
		streams = ""
		for symbol in symbols:
			streams += "/" if len(streams) else ""
			streams += "%s@kline_1m" % (symbol.lower())
		uri = "wss://stream.binance.com:9443/stream?streams=%s" % streams
		connected = False
		while True:
			try:
				if not connected:
					ws = await websockets.connect(uri)
					self.logger.info("Stream connected - %s" % symbols)
					connected = True
				while True:
					txt = await ws.recv()
					if raw:
						t = Thread(target=callback, args=(txt,))
					else:
						t = Thread(target=Binance._process, args=(txt, callback))
					t.start()
			except Exception as e:
				self.logger.warning("Exception from subscribe: %s" % e)
				self.logger.warning("Unable to connect %s stream at uri: %s (next attempt in 10s)" % (symbols, uri))
				sleep(10)
				connected = False

	@staticmethod
	def _process(txt, callback):
		data = loads(txt.encode())["data"]
		symbol = data["s"]
		time = data["E"]
		data = data["k"]
		candle = {
			"time": time,
			"open_time": data["t"],
			"close_time": data["T"],
			"open": float(data["o"]),
			"close": float(data["c"]),
			"high": float(data["h"]),
			"low": float(data["l"]),
			"volume": float(data["v"]),
			"closed": data["x"]
		}
		callback(candle, symbol)
