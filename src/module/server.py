from weesocket import Server as Socket
from ..lib.logging import Logging
from ..lib.dedicated import Dedicated
from ..application import Application


class Server(Logging, Dedicated):
	__general_actions = [
		"ping", "pong", "request-master-status",
		"indicator-create", "indicator-list",
		"strategy-create", "strategy-enable", "strategy-disable", "strategy-list",
		"backup", "clear", "sync"
	]
	__cli_actions = [
		"indicator-register", "strategy-register", "strategy-enable", "strategy-disable",
		"exchange-enable", "exchange-disable",
		"fund-add", "fund-enable", "fund-disable",
		"asset-register", "pair-register", "pair-enable", "pair-disable"
	]
	__trader_actions = [
		"open-position", "close-position", "cancel-position",
		"open-trade", "close-trade", "cancel-trade"
	]
	__tester_actions = ["run", "status", "resuls", "stop"]

	_port: int = 0
	_server: Socket | None = None

	def __init__(self, config, sender, receiver, autostart=True):
		Logging.__init__(self, config)
		self._config = config
		self._running = False
		self._sender = sender
		self._receiver = receiver
		if autostart:
			self.start()

	def start(self, *args):
		if self._config["server"]["client_secrets"] and self._config["server"]["secret"]:
			self._port = self._config["server"]["port"]
			self._server = Socket(
				trigger=self.__request_trigger,
				host=self._config["server"]["host"],
				port=self._config["server"]["port"],
				secrets=self._config["server"]["client_secrets"],
				server_secret=self._config["server"]["secret"]
			)
			self._running = True
			Dedicated.__init__(
				self, self._sender, self._receiver,
				init_message={"action": "ready"}
			)
		else:
			self.logger.error("Error: Server cannot be started due to missing client/server secrets")

	def stop(self, *args):
		if self._server:
			self._server.stop()
			self._running = False
			self._send({"action": "stopped"})

	def status(self, *args):
		self._send({
			"action": "status",
			"status": "running" if self._running else "stopped"
		})

	def send(self, data):
		if self._server and (payload := self.__prepare_payload(data)):
			self._server.send(payload, target_id=data["client"])
		else:
			self.logger.error("Server is not initialized!")

	def broadcast(self, data):
		if self._server and (payload := self.__prepare_payload(data, broadcast=True)):
			self._server.send(payload)
		else:
			self.logger.error("Server is not initialized!")

	def _trigger(self, data):
		actions = {
			"start": self.start,
			"stop": self.stop,
			"status": self.status,
			"send": self.send,
			"broadcast": self.broadcast
		}
		action = data.pop("action", "")
		if action in actions:
			actions[action](data)

	def __request_trigger(self, data):
		if type(data) == dict and "type" in data:
			request_type = data.pop("type")
			if (
				request_type in self.__general_actions or
				(self._config["type"] == "tester" and request_type in self.__tester_actions) or
				(self._config["type"] == "trader" and request_type in self.__trader_actions)
			):
				data["action"] = request_type
				data["client"] = data["sender"]
				self._send(data)
			elif request_type in self.__cli_actions:
				args = data.get("argument_list", [])
				Application.command(self._config, [request_type, *args])
			else:
				self.logger.warning(
					"Invalid network action '%s' for '%s' service type"
					% (request_type, self._config["type"])
				)
		else:
			self.logger.warning("Invalid request '%s'" % str(data))

	def __prepare_payload(self, data, broadcast=False):
		if "payload" not in data or "type" not in data or ("client" not in data and not broadcast):
			self.logger.error("Invalid data format '%s'" % repr(data))
			return
		payload = {
			"data": data["payload"],
		}
		if "type" in data:
			payload["action"] = data["type"]
		return payload
