import sys
from os import path
from copy import copy
from types import ModuleType
from ..model import Indicator as IndicatorModel, Strategy as StrategyModel


class Loader:
	__loaded = False
	__workdir = None

	@staticmethod
	def init(workdir, allowed_modules, logger):
		if Loader.__loaded:
			return
		Loader.__loaded = True
		Loader.__workdir = workdir
		Loader.__allowed_modules = allowed_modules
		Loader.__logger = logger

	@staticmethod
	def strategy(name: str, version: str = ""):
		if not Loader.__workdir:
			return
		if not version:
			model = (
				StrategyModel.query
				.filter_by(name=name)
				.order_by(StrategyModel.version.desc())
				.one_or_none()
			)
			if not model:
				return
			version = model.version

		module_path = "%s/strategies" % (Loader.__workdir)
		if module_path not in sys.path:
			sys.path.insert(0, module_path)
		data_path = "%s/data/strategies/%s/" % (Loader.__workdir, name)
		if path.isdir(module_path):
			module = Loader.__load(
				"%s.v%s" % (name, version.replace(".", "_")),
				data_path,
				fromlist=["strategy"]
			)
			if hasattr(module.strategy, "Strategy"):
				return module.strategy.Strategy

	@staticmethod
	def indicator(name: str, version: str = ""):
		if not Loader.__workdir:
			return lambda: False
		if not version:
			model = (
				IndicatorModel.query
				.filter_by(name=name)
				.order_by(IndicatorModel.version.desc())
				.one_or_none()
			)
			if not model:
				return
			version = model.version

		module_path = "%s/indicators" % (Loader.__workdir)
		if module_path not in sys.path:
			sys.path.insert(0, module_path)
		data_path = "%s/data/indicators/%s/" % (Loader.__workdir, name)
		if path.isdir(module_path):
			module = Loader.__load(
				"%s.v%s" % (name, version.replace(".", "_")),
				data_path,
				fromlist=["indicator"]
			).indicator
			if hasattr(module, "Indicator"):
				return module.Indicator
			elif type(module) == ModuleType:
				return module
			return None

	@staticmethod
	def __load(module, data_path, fromlist=[]):
		try:
			return Loader.__import(module, data_path=data_path, fromlist=fromlist)
		except Exception as e:
			Loader.__logger("External module error %s" % e)
			return None

	@staticmethod
	def __import(module_name, path=None, data_path=None, fromlist=[], **kwargs):
		sys_path = copy(list(set(sys.path)))
		if path:
			try:
				sys.path.remove("")
			except Exception:
				pass
			sys.path.insert(0, path)

		module = __import__(
			module_name,
			globals=Loader.__globals(path, data_path),
			locals=(),
			fromlist=fromlist,
			**kwargs
		)
		sys.path = sys_path
		return module

	@staticmethod
	def __wrap_import(path, data_path):
		def _import(module_name, **kwargs):
			if kwargs.get("level", 0) > 1:
				return None
			module = Loader.__import(module_name, path=path, data_path=data_path, **kwargs)
			if module.__name__ == "os":
				wrapped_module = ModuleType("os", "Emulated os module")
				wrapped_module.listdir = Loader.__wrap_fs(data_path, module.listdir)
				wrapped_module.mkdir = Loader.__wrap_fs(data_path, module.mkdir)
				wrapped_module.makedirs = Loader.__wrap_fs(data_path, module.makedirs)
				wrapped_module.rmdir = Loader.__wrap_fs(data_path, module.rmdir)
				wrapped_module.remove = Loader.__wrap_fs(data_path, module.removedirs)
				wrapped_module.removedirs = Loader.__wrap_fs(data_path, module.removedirs)
				wrapped_module.lstat = Loader.__wrap_fs(data_path, module.lstat)
				wrapped_module.stat = Loader.__wrap_fs(data_path, module.stat)
				wrapped_module.mknod = Loader.__wrap_fs(data_path, module.mknod)
				wrapped_module.path = ModuleType("path", "Emulated os.path module")
				wrapped_module.path.isdir = Loader.__wrap_fs(data_path, module.path.isidir)
				wrapped_module.path.isfile = Loader.__wrap_fs(data_path, module.path.isfile)
				wrapped_module.path.exists = Loader.__wrap_fs(data_path, module.path.exists)
				module = wrapped_module
			elif module.__name__ in ["json", "orjson", "ujson"]:
				wrapped_module = ModuleType(module.__name__, "Emulated json library")
				wrapped_module.load = Loader.__wrap_fs(data_path, module.load)
				wrapped_module.dump = Loader.__wrap_fs(data_path, module.dump)
				wrapped_module.loads = module.loads
				wrapped_module.dumps = module.dumps
			elif module.__name__ in [
				"multiprocessing", "sys", "subprocess", "pip", "importlib",
				"setuptools", "builtins"
			] or module.__name__ not in Loader.__allowed_modules:
				module = None
			return module

	@staticmethod
	def __wrap_fs(prefix, callable):
		def _wrapper(pth, *args, **kwargs):
			pth = prefix + (pth if pth[0] == "/" else "/" + pth)
			if path.realpath(pth).startswith(prefix):
				return callable(pth, *args, **kwargs)
			else:
				raise PermissionError("Access outside sandboxed directory denied!")

	@staticmethod
	def __globals(import_path, data_path):
		if type(__builtins__) == dict:  # due to pytest stupid __builtins__ type change
			builtins = copy(__builtins__)
		else:
			builtins = copy(__builtins__.__dict__)
		builtins["__import__"] = Loader.__wrap_import(import_path, data_path)
		builtins["open"] = Loader.__wrap_fs(data_path, open)

		class B(object):
			def __init__(self, dct):
				self.__dict__.update(dct)

		return {
			"__builtins__": B(builtins),
			"indicator_loader": Loader.indicator,
		}
