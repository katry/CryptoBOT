from datetime import datetime
from orjson import dumps
from ..lib.logging import Logging
from ..lib.dedicated import Dedicated
from ..model import (
	Database,
	PositionStatus as Status,
	Position as PositionModel,
	Exchange as ExchangeModel,
	Strategy as StrategyModel,
	Test as TestModel
)
from .loader import Loader
from .pair import Pair
from .position import Position
from .strategy import Strategy


class Trader(Logging, Dedicated):
	_requests: list = []
	_strategies: list = []
	_waiters: dict = {}
	_running = False
	_test = None
	__locked = False
	__terminated = False

	def __init__(self, config, symbol, exchange_ids, sender, receiver, test_id=False):
		Logging.__init__(self, config)
		Loader.init(config["workdir"], config["allowed_modules"], self.logger)
		self._config = config
		self._symbol = symbol
		self.__buffer = []
		Database.init("%s/data.db" % config["workdir"])
		self._positions = {}
		self._exchanges = {
			e.id: e for e in [ExchangeModel.query.get(id) for id in exchange_ids]
		}
		self._pair = {i: Pair(self._symbol, self._exchanges[i]) for i in self._exchanges}
		if test_id:
			self._test = TestModel.query.get(test_id)
			self.__counter = 0
			for strategy in self._test.strategies:
				self.__load_strategy(strategy)
		else:
			for strategy in StrategyModel.query.filter_by(active=True).all():
				self.__load_strategy(strategy)
		self.__load()
		Dedicated.__init__(
			self, sender, receiver,
			init_message={
				"action": "ready",
				"symbol": symbol
			}
		)

	def __stop(self):
		self.__terminated = True
		for t in self.__threads:
			t._stop()
		self._send({"action": "stopped", "symbol": self._symbol}, target="main")

	def __load(self):
		if self._test:
			models = (
				PositionModel.query
				.filter(PositionModel.id.in_([i.id for i in self._test.positions]))
				.filter(PositionModel.status.in_(Status.PENDING, Status.PARTIALY_FILLED, Status.OPEN))
				.all()
			)
		else:
			models = (
				PositionModel.query
				.filter(PositionModel.status.in_((Status.PENDING, Status.PARTIALY_FILLED, Status.OPEN)))
				.all()
			)
		for model in models:
			strategy = None
			for s in self._strategies:
				if s.id == model.strategy:
					strategy = s
					break
			position = Position(
				self._config, model, self._pair[model.exchang_id].id, model.fund_id,
				self._exchanges[model.exchang_id], strategy, self._sender, self.__buffer, True
			)
			self._positions[model.exchange_id].append(position)

	def _process(self, data):
		if "candle" not in data or "exchange_id" not in data:
			return
		self._pair[data["exchange_id"]].update(data["candle"])
		if self._running:
			if self._test:
				if self.__counter == 10:
					self.__counter = 0
				else:
					self.__counter += 1
			self._check_pending(self._exchanges[data["exchange_id"]])
			self._check_open(self._exchanges[data["exchange_id"]])
			self._check_close(self._exchanges[data["exchange_id"]])

	def _check_open(self, exchange):
		if self.__locked:
			return
		for strategy in self._strategies:
			pair = self._pair[exchange.id]
			pos = strategy.check_open_wrapper(pair)
			if pos := Position.parse_request(pos, pair):
				if self._test:
					self._slow_down_test()
				if pos["type"] not in self._waiters:
					self._waiters[pos["type"]] = []
				self._waiters[pos["type"]].append(strategy)
				self._requests.append(r := {
					"id": datetime.utcnow().isoformat(),
					"type": pos["type"],
					"asset": pos["asset"],
					"exchange": exchange,
					"open_price": pos["price"],
					"open_time": pos["time"],
					"strategy": strategy
				})
				self._send({"action": "request-fund", "asset": r["asset"], "id": r["id"]})

	def _check_close(self, exchange):
		if exchange.id not in self._positions:
			return
		for position in self._positions[exchange.id]:
			if position.status != Status.OPEN:
				continue
			if position.skip:
				if position.close_trigger:
					position.close()
				continue
			pos = position.strategy.check_close_wrapper(self._pair[exchange.id], position)
			if pos := Position.parse_request(pos, None, close=True):
				position.close(pos)

	def _check_pending(self, exchange):
		if exchange.id not in self._positions:
			return
		for position in self._positions[exchange.id]:
			if position.status in (Status.CLOSED, Status.REJECTED, Status.CANCELED):
				self._positions[exchange.id].remove(position)
				self._send({
					"action": "return-fund",
					"fund_id": position.fund["id"],
					"amount": position.fund["amount"],
					"open_time": self._pair[exchange.id].time
				})

	def _fund_accept(self, data):
		if "id" not in data:
			return
		for i in self._requests:
			if i["id"] == data["id"]:
				model = {
					"type": int(i["type"]),
					"amount": data["fund"]["amount"],
					"price": i["open_price"],
					"open_time": i["open_time"],
					"modified": datetime.utcnow().isoformat(),
					"pair_id": self._pair[i["exchange"].id].id,
					"fund_id": data["fund"]["id"],
					"exchange_id": i["exchange"].id,
					"strategy_id": i["strategy"].id
				}
				position = Position(
					self._config,
					model,
					self._pair[i["exchange"].id],
					data["fund"]["id"],
					i["exchange"],
					i["strategy"],
					self._sender,
					self.__buffer
				)
				if i["exchange"].id not in self._positions:
					self._positions[i["exchange"].id] = []
				self._positions[i["exchange"].id].append(position)
				if self._test:
					self._speed_up_test()
				self._waiters[i["type"]].remove(i["strategy"])
				if not len(self._waiters[i["type"]]):
					self._waiters.pop(i["type"])
				self._requests.remove(i)
				break

	def _fund_reject(self, data):
		if "id" not in data:
			return
		for i in self._requests:
			if i["id"] == data["id"]:
				self._requests.remove(i)
				if self._test:
					self._speed_up_test()
				self._waiters[i["type"]].remove(i["strategy"])
				if not len(self._waiters[i["type"]]):
					self._waiters.pop(i["type"])
				break

	def _open_position(self, data):
		strategy = None
		if "strategy_id" in data["position"]:
			for s in self._strategies:
				if s["model"].id == data["position"]["strategy_id"]:
					strategy = s["model"]
					break
		exchange = ExchangeModel.query.get(data["exchange_id"])
		if not exchange:
			return self._send({"action": "position-open-reject", "symbol": self._symbol})
		data["position"]["pair_id"] = self._pair[exchange.id].id
		position = Position(
			self._config,
			data["position"],
			self._pair[data["exchange_id"]],
			data["fund_id"],
			exchange,
			strategy,
			self._sender,
			self.__buffer
		)
		if exchange.id not in self._positions:
			self._positions[exchange.id] = []
		self._positions[exchange.id].append(position)

	def _close_position(self, data, cancel=False):
		if "id" not in data:
			return
		for exchange_id in self._positions:
			for position in self._positions[exchange_id]:
				if position.id == data["id"]:
					position.close({"price": data.get("price", None)}, cancel=cancel)
					break

	def _load_position(self, data):
		if "position" not in data or "id" not in data["position"]:
			return
		for position in self._positions:
			if position.id == data["position"]["id"]:
				return
		position = Position.load(data)
		if not position:
			return
		self._positions.append(position)

	def _update_position(self, data):
		if "position" not in data or "exchange_id" not in data:
			return
		for pos in self._positions[data["exchange_id"]]:
			if pos.id == data["position"]["id"]:
				self.__buffer.append({
					"status": data["position"]["status"]
				})
				pos.refresh()
				break

	def _load_graph(self, data):
		if "graph" not in data or "exchange_id" not in data or data["exchange_id"] not in self._pair:
			return
		self._pair[data["exchange_id"]].load(data["graph"])
		self._send({"action": "loaded", "symbol": self._symbol})

	def __load_strategy(self, strategy):
		try:
			self._strategies.append(Strategy.load(
				strategy,
				Loader.strategy(strategy.name, strategy.version)(),
				self.logger
			))
		except Exception:
			self.logger.warning(
				"Invalid strategy '%s - %s' (%s)" % (strategy.name, strategy.version, strategy.id)
			)

	def _lock(self, data):
		if self.__locked:
			return self._send({"action": "already-locked"})
		self.__locked = True
		self._send({"action": "locked", "symbol": self._symbol})

	def _unlock(self, data):
		if not self.__locked:
			return self._send({"action": "already-unlocked"})
		self.__locked = False
		self._send({"action": "unlocked", "symbol": self._symbol})

	def _trigger(self, data):
		actions = {
			"graph": self._load_graph,
			"candle": self._process,

			"fund-request-accept": self._fund_accept,
			"fund-request-reject": self._fund_reject,
			"funds-return-reject": self._error_process,
			"funds-return-accept": lambda data: None,

			"position-update": self._update_position,
			"position-open": self._open_position,
			"position-close": self._close_position,
			"position-cancel": lambda data: self._close_position(data, cancel=True),

			"lock": self._lock,
			"unlock": self._unlock,
			"stop": self.__stop,
		}
		action = actions.get(data["action"])
		if action:
			action(data)
		else:
			self.logger.warning("Invalid trigger operation - %s" % repr(data))

	def _slow_down_test(self):
		self._send({"action": "slow-down"}, target="main")

	def _speed_up_test(self):
		self._send({"action": "slow-down-cancel"}, target="main")

	def _error_process(self, data):
		self.logger.error("DECLINED REQUEST '%s' - %s" % (data["action"], dumps(data).decode()))

	@property
	def locked(self):
		return self.__locked
