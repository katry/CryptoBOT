from ..lib.logging import Logging
from ..lib.dedicated import Dedicated
from ..model import (
	Database,
	Test as TestModel,
	TestStatus,
	Fund as FundModel,
)


class Manager(Logging, Dedicated):
	_funds: dict = {}
	_test = None

	def __init__(self, config, sender, receiver, testing=False):
		Logging.__init__(self, config)
		self._config = config
		Database.init("%s/data.db" % config["workdir"])
		if not testing:
			self._load_funds()
		Dedicated.__init__(self, sender, receiver, init_message={"action": "ready"})

	def _stop(self, data=None):
		self._running = False
		self._send({"action": "stopped"})

	def _load_funds(self):
		self._funds = {}
		if self._test:
			funds_model = self._test.funds
		else:
			funds_model = FundModel.query.filter_by(active=True).all()
		for fund in funds_model:
			symbol = fund.asset.symbol
			if symbol not in self._funds:
				self._funds[symbol] = []
			self._funds[symbol].append(fund)

	def _fund_request(self, data):
		if "asset" not in data or not (id := data.get("id")):
			return self._send(
				{"action": "funds-request-reject", "message": "NO ASSET", "id": data.get("id")},
				target=data["sender"]
			)
		if data["asset"] not in self._funds:
			return self._send(
				{"action": "funds-request-reject", "message": "NO FUND", "id": id},
				target=data["sender"]
			)
		for fund in self._funds[data["asset"]]:
			if not fund.locked:
				fund.locked = True
				fund.update()
				return self._send({
						"action": "funds-request-accept",
						"fund": {"asset": data["asset"], **dict(fund)},
						"id": id
					}, target=data["sender"]
				)
		self._send(
			{"action": "funds-request-reject", "message": "FUND LOCKED", "id": id},
			target=data["sender"]
		)

	def _fund_return(self, data):
		if "fund" not in data or not (id := data.get("id")):
			return self._send(
				{"action": "funds-return-reject", "message": "NO ASSET", "id": data.get("id")},
				target=data["sender"]
			)
		if data["fund"]["asset"] not in self._funds:
			return self._send(
				{"action": "funds-return-reject", "message": "NO FUND", "id": id},
				target=data["sender"]
			)
		for fund in self._funds[data["fund"]["asset"]]:
			if fund.id == data["fund"]["id"] and fund.locked:
				fund.amount = data["fund"]["amount"]
				fund.locked = False
				fund.update()
				return self._send({"action": "funds-return-accept", "id": id})
		self._send(
				{"action": "funds-return-reject", "message": "NO FUND", "id": id},
				target=data["sender"]
			)

	def _init_test(self, data):
		if "id" not in data or not (id := data["id"]):
			self._send({"action": "test-reject", "message": "NO TEST ID"})
			return
		test = TestModel.query.filter_by(status=TestStatus.PENDING).filter_by(id=id).one_or_none()
		if not test:
			self._send({"action": "test-reject", "message": "TEST '%s' NOT FOUND" % id})
			return
		self._test = test
		self._load_funds()
		test.status = TestStatus.RUNNING
		test.update()
		self._send({"action": "test-accept"})

	def _finish_test(self, data):
		if "id" not in data or not (id := data["id"]):
			return self._send({"action": "test-finish-reject", "message": "NO TEST ID"})
		if not self._test or id != self._test.id:
			return self._send({"action": "test-finish-reject", "message": "TEST '%s' NOT ACTIVE" % id})
		if data.get("status") == TestStatus.CANCELED:
			self._test.status = TestStatus.CANCELED
		else:
			self._test.status = TestStatus.DONE
		self._test.update()
		self._test = None
		self._send({"action": "test-finish-accept"})

	def _trigger(self, data):
		actions = {
			"request-fund": self._fund_request,
			"return-fund": self._fund_return,
			"test-init": self._init_test,
			"test-finish": self._finish_test,
			"funds-reload": self._load_funds,
			"stop": self._stop,
		}
		action = actions.get(data["action"])
		if action:
			action(data)
		else:
			self.logger.warning("Invalid trigger operation - %s" % repr(data))
