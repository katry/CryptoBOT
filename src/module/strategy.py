from datetime import timedelta, datetime
from logging import Logger


class Strategy:
	__last_trade = None
	__delay = timedelta()

	def __init__(
		self, model, check_open, check_close, delay: timedelta,
		logger: Logger
	):
		self.__model = model
		self.__check_open = check_open
		self.__check_close = check_close
		self.__delay = delay
		self.__last_trade = datetime.now() - delay
		self.__logger = logger

	@classmethod
	def load(cls, model, strategy, logger):
		if (
			not hasattr(strategy, "check_open") or not hasattr(strategy, "check_close") or
			not callable(strategy.check_open) or not callable(strategy.check_close)
		):
			return None
		delay = strategy.delay if type(getattr(strategy, "delay", None)) == timedelta else timedelta()
		return cls(model, strategy.check_open, strategy.check_close, delay, logger)

	def check_open_wrapper(self, pair):
		if pair.time > self.__last_trade + self.__delay:
			return False
		try:
			result = self.__check_open(pair)
			if result:
				self.__last_trade = pair.time
		except Exception as e:
			self.__logger.warning("Strategy open error - %s" % e)
			result = False
		return result

	def check_close_wrapper(self, pair, position):
		try:
			return self.__check_close(pair, position)
		except Exception as e:
			self.__logger.warning("Strategy close error - %s" % e)
			return False

	@property
	def id(self):
		self.__model.id

	@property
	def external_id(self):
		self.__model.external_id

	@property
	def name(self):
		self.__model.name

	@property
	def version(self):
		self.__model.version

	@property
	def active(self):
		self.__model.active

	@property
	def priority(self):
		self.__model.priority

	@property
	def tests(self):
		self.__model.tests
