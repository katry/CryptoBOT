def Indicator(graph, length=10, source="close", offset=10):
	res = []
	for o in range(offset):
		base = graph[-length - offset + o][source]
		now = graph[-offset + o][source]
		res.append(now - base)
	return res
