ma = indicator("ma")


def Indicator(graph, periods=[12, 26, 9], length=0, ema=True, source="close"):
	if ema:
		st = ma.ema_list(graph, period=periods[0], length=length, source=source)
		lt = ma.ema_list(graph, period=periods[1], length=length, source=source)
	else:
		st = ma.sma_list(graph, period=periods[0], length=length, source=source)
		lt = ma.sma_list(graph, period=periods[1], length=length, source=source)
	macd = []
	for i in range(len(st)):
		macd.append(st[i] - lt[i])
	return {
		"macd": macd,
		"signal": ma.ema(macd, period=periods[2])
	}
