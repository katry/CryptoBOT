class Indicator:
	@staticmethod
	def ema(data, period):
		exp = 2 / (period + 1)
		ema = []
		for candle in data:
			if len(ema):
				ema.append(candle * exp + ema[-1] * (1 - exp))
				continue
			ema.append(candle)
		return ema

	@staticmethod
	def ema_list(interval, period=10, length=0, source="close"):
		gd = Indicator.slice_graph(interval, period, length)
		data = []
		for c in gd:
			data.append(c[source])
		return Indicator.ema(data, period)

	@staticmethod
	def sma(data, period=10):
		sma = []
		pv = []
		for candle in data:
			pv.append(candle)
			if len(pv) < period:
				continue
			sma.append(sum(pv) / period)
			pv = pv[1:]
		return sma

	@staticmethod
	def sma_list(interval, period=10, length=0, source="close"):
		gd = Indicator.slice_graph(interval, period, length)
		data = []
		for c in gd:
			data.append(c[source])
		return Indicator.sma(data, period)

	@staticmethod
	def slice_graph(graph, period, length):
		if length:
			return graph[-period - length + 1:]
		else:
			return graph
