def Indicator(graph, length=14, offset=20, source="close"):
	gd = graph[-length - offset:]
	rsi_list = []

	for o in range(offset):
		ngd = gd[o:o + length + 1]
		close = None
		profits = []
		losses = []
		for candle in ngd:
			if close:
				result = candle[source] - close
				if result > 0:
					profits.append(result)
				else:
					losses.append(-1 * result)
			close = candle[source]
		profit = sum(profits) / len(profits) if len(profits) else 1
		loss = sum(losses) / len(losses) if len(losses) else 1
		if loss == 0:
			rsi_list.append(rsi_list[-1] if len(rsi_list) else 50)
			continue
		result = 100 - (100 / (1 + (profit / loss)))
		rsi_list.append(result)
	return rsi_list
