ma = indicator("ma")


def Indicator(graph, period=20, length=1):
	stoch_list = []
	for index in range(length + 1):
		close = graph[-1]["close"]
		period_low = None
		period_high = None
		for i in graph[-period - (length - index):]:
			if period_low is None or i["low"] < period_low:
				period_low = i["low"]
			if period_high is None or i["high"] < period_high:
				period_high = i["high"]
		K = (close - period_low / period_high - period_low) * 100
		stoch_list.append({
			"K": K,
		})
		if len(stoch_list) >= period:
			stoch_list[-1]["D"] = ma.sma([i["K"] for i in stoch_list], period=period)
	if length == 1:
		return stoch_list[-1]
	else:
		return stoch_list[-length:]
