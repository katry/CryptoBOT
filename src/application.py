from re import sub
from os import path, makedirs
from orjson import loads, dumps
from .lib.validator import Validator


class Application:
	__schedule = {
			"interval": (str, lambda x, *a: x in ["hourly", "daily", "weekly"]),
			"weekday": (
				[str, "optional"],
				lambda x, *a: x in [
					"monday", "tuesday", "wednesday", "thursday",
					"friday", "saturday", "sunday"
				]
			),
			"hour": ([int, "optional"], lambda x, *a: x >= 0 and x < 24),
			"minute": ([int, "optional"], lambda x, *a: x >= 0 and x < 60)
		}
	_validator = {
		"debug": (bool, None),
		"autostart": (bool, None),
		"type": (str, lambda x, *a: x in ["trader", "notifier", "collector", "tester"]),
		"workdir": (str, lambda x, *a: path.isdir(x)),
		"allowed_modules": (list, lambda x, *a: 0 not in [1 if type(i) == str else 0 for i in x]),
		"server": ([dict, "optional"], {
			"host": (str, lambda x, *a: len(x) >= 1),
			"port": (int, lambda x, *a: x > 0 and x < 65536),
			"client_secrets": (
				list,
				lambda x, *a: 0 not in [1 if type(i) == str and len(i) else 0 for i in x]
			),
			"secret": (str, lambda x, *a: len(x) >= 10)
		}),
		"sync": ([dict, "optional"], {
			"type": (str, lambda x, *a: x in ["backup", "online"]),
			**__schedule   # type: ignore
		}),
		"archiving": ([dict, "optional"], __schedule),
		"backup": ([dict, "optional"], __schedule),
		"download": ([dict, "optional"], __schedule)
	}

	@staticmethod
	def run(workdir, **kwargs):
		config = Application.load_config(workdir)
		if not config:
			print(">>> Invalid configuration file '%s'", workdir)
			return
		commands = kwargs.pop("command")
		if commands[0] == "start":
			print("\n>>> Starting CryptoBOT in mode: %s\n" % config["type"].upper())
			if config["type"] in ["trader", "notifier"]:
				from .worker import Worker
				Worker(config)
			elif config["type"] == "collector":
				from .collector import Collector
				Collector(config, kwargs["exchange"], kwargs["nograph"])
			elif config["type"] == "tester":
				from .tester import Tester
				Tester(config)
		else:
			Application.command(config, commands, **kwargs)

	@staticmethod
	def command(config, commands, **kwargs):
		from . import script
		from .model import Database
		actions = {
			"check-archives": script.CheckValidity,
			"check-data": script.CheckValidity.checker,
			"unpack": script.Unpack,

			"strategy-list": script.Strategy.list,
			"strategy-register": script.Strategy.register,
			"strategy-enable": script.Strategy.enable,
			"strategy-disable": script.Strategy.disable,

			"indicator-list": script.Indicator.list,
			"indicator-register": script.Indicator.register,

			"asset-list": script.Asset.list,
			"asset-register": script.Asset.register,

			"pair-list": script.Pair.list,
			"pair-register": script.Pair.register,
			"pair-enable": script.Pair.enable,
			"pair-disable": script.Pair.disable,

			"fund-list": script.Fund.list,
			"fund-add": script.Fund.add,
			"fund-enable": script.Fund.enable,
			"fund-disable": script.Fund.disable,

			"exchange-list": script.Exchange.list,
			"exchange-enable": script.Exchange.enable,
			"exchange-disable": script.Exchange.disable,
			"exchange-set-fee": script.Exchange.set_fee,
			"exchange-set-api": script.Exchange.set_api,

			"settings-import": script.Settings.load,
			"settings-export": script.Settings.export
		}
		if commands[0] not in actions:
			return

		Database.init("%s/data.db" % config["workdir"])

		if commands[0] in ["strategy-enable", "strategy-disable"]:
			if len(commands) in [2, 3]:
				actions[commands[0]](config, *commands[1:])
			else:
				print(">>> Command '%s' takes one or two position parameters" % commands[0])

		elif commands[0] in [
			"strategy-register", "indicator-register", "exchange-set-fee", "exchange-set-api"
		]:
			if len(commands) == 3:
				actions[commands[0]](config, commands[1], commands[2])
			else:
				print(">>> Command '%s' takes exactly two position parameters" % commands[0])

		elif commands[0] in [
			"load-settings", "asset-register", "pair-disable", "fund-disable", "pair-enable",
			"fund-enable", "exchange-enable", "exchange-disable", "settings-export", "settings-import"
		]:
			if len(commands) == 2:
				actions[commands[0]](config, commands[1])
			else:
				print(">>> Command '%s' takes exactly one position parameter" % commands[0])

		elif commands[0] in ["pair-register", "fund-add"]:
			if len(commands) == 4:
				actions[commands[0]](config, commands[1], commands[2], commands[3])
			else:
				print(">>> Command '%s' takes exactly three position parameters" % commands[0])
		else:
			actions[commands[0]](config, **kwargs)

	@staticmethod
	def create_config(config_file, config, debug=False, config_name="robot.json", validator=None):
		config_file = Application.__validate_config_path(path.realpath(config_file), True, config_name)
		if config["workdir"][-1] == "/":
			config["workdir"] = config["workdir"][0:-1]
		if Validator.check(config, validator or Application._validator):
			f = open(config_file, "w")
			output = dumps(config).decode().replace(":", ": ")
			output = sub(r"([\{\[,])", r"\g<1>\n", output)
			output = sub(r"([\]\}])", r"\n\g<1>", output)
			indent_level = 0
			for i in output.split("\n"):
				o = [i.count("{"), i.count("[")]
				c = [i.count("}"), i.count("]")]
				if sum(o) < sum(c):
					indent_level -= 1
				f.write("%s%s\n" % (indent_level * "\t", i))
				if sum(o) > sum(c):
					indent_level += 1
			f.close()
			return config

	@staticmethod
	def load_config(config_file, config_name="robot.json", validator=None):
		config_file = Application.__validate_config_path(path.realpath(config_file), False, config_name)
		if not path.isfile(config_file):
			return
		try:
			f = open(config_file, "rb")
			config = loads(f.read())
			f.close()
		except Exception:
			return
		if Validator.check(config, validator or Application._validator):
			return config

	@staticmethod
	def __validate_config_path(pth, create, name):
		if path.isfile(pth):
			return pth
		if path.isdir(pth):
			if pth[-1] == "/":
				pth = pth[0:-1]
			if pth.split("/")[-1] == "config":
				return "%s/%s" % (pth, name)
			else:
				if create and not path.isdir("%s/config" % pth):
					makedirs("%s/config" % pth)
				return "%s/config/%s" % (pth, name)
		return ""
