from os import path, get_terminal_size, makedirs
from re import compile, match
from typing import Callable
from shutil import copy
from alembic import command
from .application import Application
from . import __version__


class System:
	def __init__(self, commands, message, revision, workdir, debug=False, local=False):
		self.local = local
		if not len(commands):
			return
		if commands[0] == "init":
			self.init(workdir, debug)
			return

		self.config = Application.load_config(workdir)
		self.alembic_cfg = System._alembic_config(self.config, debug, local)
		self.message = message
		self.revision = revision
		actions = {
			"update": self.update,
			"generate": self.generate,
		}
		if commands[0] not in actions:
			print("Invalid system action '%s'" % commands[0])
			return
		actions[commands[0]]()

	def init(self, workdir, debug=False):
		config = {
			"debug": debug,
			"autostart": True,
			"workdir": workdir,
			"allowed_modules": ["urllib"]
		}
		print("-" * System._term_width())
		print(">>> Initializing CryptoBOT\n  > WORKDIR: %s" % config["workdir"])
		m = "Enter service type [0]=Tester, [1]=Trader, [2]=Notifier, [3]=Collector"
		ct = self._valid_input(m, default="2", checker=lambda x: x in ["0", "1", "2", "3"])
		config["type"] = {"0": "tester", "1": "trader", "2": "notifier", "3": "collector"}[ct]
		m = "Enable server interface [y/n]"
		if self._valid_input(m, default="y", checker=lambda x: x.lower() in ["y", "n"]).lower() == "y":
			server = {}
			host = compile(r"^[0-9a-zA-Z\.-_]+$")
			port = compile(r"^[0-9]+$")
			m = "Enter server host"
			server["host"] = self._valid_input(m, default="0.0.0.0", checker=lambda x: match(host, x))
			m = "Enter server port"
			server["port"] = int(self._valid_input(
				m,
				default="6666",
				checker=lambda x: match(port, x) and int(x) > 0 and int(x) < 65536
			))
			m = "Enter (and remember) server secret key (at least 10 characters)"
			server["secret"] = self._valid_input(m, checker=lambda x: len(x) > 10)
			server["client_secrets"] = []
			nxt = "y"
			while nxt.lower() == "y":
				m = "Enter one of client secret keys"
				secret = self._valid_input(m, checker=lambda x: len(x) > 0)
				server["client_secrets"].append(secret)
				m = "Add another client secret key [y/n]"
				nxt = self._valid_input(m, checker=lambda x: x.lower() in ["y", "n"], default="n")
			config["server"] = server
			print("config-type", config["type"])
		if config["type"] == "collector":
			m = "Set up archiving [y/n]"
			a = self._valid_input(m, checker=lambda x: x.lower() in ["y", "n"], default="y")
			if a.lower() == "y":
				config["archiving"] = {}
				m = "Set archiving interval [hourly / daily / weekly]"
				config["archiving"]["interval"] = self._valid_input(m, checker=lambda x: x.lower() in ["hourly", "daily", "weekly"], default="hourly")
				if config["archiving"]["interval"] == "weekly":
					m = "Set weekday [monday / tuesday / wednesday / thursday / friday /saturday / sunday]"
					config["archiving"]["weekday"] = self._valid_input(m, checker=lambda x: x.lower() in ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"], default="sunday")
				if config["archiving"]["interval"] in ["weekly", "daily"]:
					m = "Set hour [0-23]"
					config["archiving"]["hour"] =  int(self._valid_input(m, checker=lambda x: x.isdigit() and int(x) >= 0 and int(x) < 24, default="0"))
				m = "Set minute [0-60]"
				config["archiving"]["minute"] =  int(self._valid_input(m, checker=lambda x: x.isdigit() and int(x) >= 0 and int(x) < 60, default="6"))

		self.config = Application.create_config(workdir, config, debug=debug)
		self.mkdir("%s/data" % self.config["workdir"])
		self.mkdir("%s/data/strategies" % self.config["workdir"])
		self.mkdir("%s/data/indicators" % self.config["workdir"])
		self.mkdir("%s/log" % self.config["workdir"])
		self.mkdir("%s/strategies" % self.config["workdir"])
		self.mkdir("%s/indicators" % self.config["workdir"])
		with open(self.config["workdir"] + "/indicators/__init__.py", "w") as fp:
			fp.close()
		with open(self.config["workdir"] + "/strategies/__init__.py", "w") as fp:
			fp.close()

		def create_indicator(name, version):
			src_path = "%s/module/indicators" % (path.realpath(__file__).replace("/system.py", ""))
			dv = version.replace(".", "_")
			makedirs("%s/indicators/%s/v%s" % (self.config["workdir"], name, dv))
			with open(self.config["workdir"] + "/indicators/%s/__init__.py" % name, "w") as fp:
				fp.close()
			with open(self.config["workdir"] + "/indicators/%s/v%s/__init__.py" % (name, dv), "w") as fp:
				fp.close()
			copy(
				src_path + "/%s.py" % name,
				self.config["workdir"] + "/indicators/%s/v%s/indicator.py" % (name, dv)
			)

		create_indicator("ma", "1.0.0")
		create_indicator("macd", "1.0.0")
		create_indicator("mom", "1.0.0")
		create_indicator("rsi", "1.0.0")
		create_indicator("stoch", "1.0.0")

		self.alembic_cfg = System._alembic_config(self.config, debug, self.local)
		self.update()

	def update(self):
		command.upgrade(self.alembic_cfg, 'head')
		file = open("%s/.CryptoBOT.ver" % self.config["workdir"], "w")
		file.write(__version__)
		file.close()

	def generate(self):
		if not self.revision:
			print("Specify version number by argument --revision")
			return
		kw = {
			"message": str(self.message) if self.message else "",
			"rev_id": self.revision,
			"autogenerate": True
		}
		command.revision(self.alembic_cfg, **kw)

	def _alembic_config(config, debug, local):
		from alembic.config import Config
		from os import path
		db_path = path.realpath("%s/data.db" % config["workdir"])
		config = path.realpath(__file__).replace("/system.py", "/migrations/config.ini")
		alembic_cfg = Config(config)
		alembic_cfg.set_main_option("sqlalchemy.url", "sqlite:///%s" % db_path)
		if local:
			pth = path.realpath(__file__).replace("system.py", "migrations")
			alembic_cfg.set_main_option("script_location", pth)
		else:
			alembic_cfg.set_main_option("script_location", "CryptoBOT:migrations")
		return alembic_cfg

	@staticmethod
	def mkdir(p):
		from os import path, makedirs
		if not path.isdir(p):
			makedirs(p)

	@staticmethod
	def _valid_input(message: str = "", checker: Callable = lambda x: True, default=None):
		print("-" * System._term_width())
		print(">>>", message)
		if default:
			print("  >", "Default [%s]" % default)
		val = input("<<< ") or default or ""
		if val and val[0] == "[" and val[-1] == "]":
			val = val[1:-1]
		if checker(val):
			return val
		else:
			print("\n>>> Invalid input. Try again!")
			return System._valid_input(message, checker, default)

	@staticmethod
	def _term_width():
		try:
			return get_terminal_size()[0]
		except Exception:
			return 20
