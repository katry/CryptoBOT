from os import path, walk, mkdir, rmdir, symlink, remove, listdir
from shutil import move
from .aggregate import AggregateArchives
from .backup import Backup
import tarfile


class Unpack:
	def __init__(self, config,
							archive_path=None, target_path=None, skip_symlinks=False, move=True,
							skip_aggregate=False, *args, **kwargs):
		self.config = config
		self.exchange = kwargs.get("exchange")
		if not archive_path:
			if archive_path:
				if not archive_path.endswith("packed/") and not archive_path.endswith("backup/"):
					print("ERROR: Archive path must ends with `packed/ or backup/`")
					return
				if not path.exists(archive_path) or path.isfile(archive_path):
					print("ERROR: Archive path must be an existing directory")
					return
				archive_path = archive_path
			else:
				archive_path = "%s/data/%s/packed/" % (config["workdir"], self.exchange)
		if not path.exists(archive_path):
			mkdir(archive_path)
		if not skip_aggregate:
			AggregateArchives(archive_path)
		temp = "%s/data/%s/temp/" % (config["workdir"], self.exchange)
		backup_path = "%s/data/%s/backup/" % (config["workdir"], self.exchange)
		if not path.exists(backup_path):
			mkdir(backup_path)
		if not path.exists(temp):
			mkdir(temp)
		if not target_path:
			target_path = "%s/data/%s/common/" % (config["workdir"], self.exchange)
		if not path.exists(target_path):
			mkdir(target_path)
		dirs = [
			f for f in listdir(temp.replace("temp/", "")) if not path.isfile(path.join(temp.replace("temp/", ""), f))
		]
		for dir in dirs:
			if dir.startswith("_"):
				self.clean(temp.replace("temp/", "") + dir + "/")
		self.clean(target_path)
		self.unpack(archive_path, temp, target_path, skip_symlinks)
		if move:
			Backup(archive_path, backup_path)

	def clean(self, target_path):
		dirs = [f for f in listdir(target_path) if not path.isfile(path.join(target_path, f))]
		for symbol in dirs:
			files = [f for f in listdir(target_path + symbol) if path.isfile(path.join(target_path + symbol + "/", f))]
			for file in files:
				if file.endswith("_stream.json"):
					remove(target_path + symbol + "/" + file)

	def unpack(self, archive_path, temp, target_path, skip_symlinks):
		for (dir_path, dir_name, file_names) in walk(archive_path):
			if dir_path.endswith("packed/") or dir_path.endswith("backup/"):
				for file_name in file_names:
					if not file_name.endswith(".tar.xz") and not file_name.endswith(".tar.gz") or "graph" in file_name:
						continue
					print("Unpacking: ", file_name)
					archive_number = file_name.split("_")[0]
					archive = tarfile.open(archive_path + file_name, "r:*")
					if "temp" not in archive.getnames():
						archive.extractall(temp)
					else:
						archive.extractall(temp.replace("/temp/", ""))
					archive.close()
					for (dir_path, dir_name, file_names) in walk(temp):
						for file_name in file_names:
							symbol = file_name.split("_")[0]
							new_name = "%s_%s" % (archive_number, file_name)
							if not path.exists(target_path + symbol):
								mkdir(target_path + symbol)
							target = target_path + symbol + "/" + new_name
							move(dir_path + file_name, target)
							self.create_symlinks(target, new_name, skip_symlinks)
		rmdir(temp)

	def create_symlinks(self, target, file_name, skip_symlinks):
		if skip_symlinks:
			return
		symbol = file_name.split("_")[1]
		pack_root = "%s/data/%s/" % (self.config["workdir"], self.exchange)
		for (dir_path, dir_name, file_names) in walk(pack_root):
			if not dir_path.endswith("%s/" % self.exchange):
				continue
			for dir in dir_name:
				if not dir.startswith("_"):
					continue
				with_suffix = "%s%s/%s_%s_graph.json" % (dir_path, dir, symbol, self.exchange)
				without_suffix = "%s%s/%s_graph.json" % (dir_path, dir, symbol)
				if path.exists(with_suffix) or path.exists(without_suffix):
					if not path.exists("%s%s/%s" % (dir_path, dir, symbol)):
						mkdir("%s%s/%s" % (dir_path, dir, symbol))
					link = "%s%s/%s/%s" % (dir_path, dir, symbol, file_name)
					if path.exists(link):
						remove(link)
					symlink(target, link)
