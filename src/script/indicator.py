from os import path, mkdir
from re import compile
from ..model import (
	Indicator as IndicatorModel
)


class Indicator:
	_name_check = compile("^[a-zA-Z][a-zA-Z0-9_-]+$")
	_version_check = compile(r"^\d+\.\d+\.\d+$")

	@staticmethod
	def list(config, versions=False, **kwargs):
		indicators = (
			IndicatorModel.query
			.order_by(IndicatorModel.name, IndicatorModel.version.desc())
			.all()
		)
		to_print = {}
		for indicator in indicators:
			if indicator.name not in to_print:
				to_print[indicator.name] = [indicator.version]
			else:
				to_print[indicator.name].append(indicator.version)
		print("### Indicator list")
		for k, v in to_print.items():
			print("%s\n - Versions:" % k)
			for item in v:
				print("  %s" % item)

	@staticmethod
	def register(config, name, version, **kwargs):
		if not Indicator._name_check.match(name):
			print(
				">>> Invalid name format '%s' should be alphanumeric with possible dash / underscore characters" % name
			)
			print("  > and must start with letter")
			return
		if not Indicator._version_check.match(version):
			print(">>> Invalid version format - '%s' does not match pattern '\\d+\\.\\d+\\.\\d+'" % version)
			return
		pth = "%s/indicators/%s/v%s/" % (config["workdir"], name, version.replace(".", "_"))
		if not path.isfile("%s/indicator.py" % pth):
			print("Indicator not found %s %s" % (name, version))
			return
		if (
			IndicatorModel.query
			.filter_by(name=name)
			.filter_by(version=version)
			.count()
		):
			print("Indicator already regstered '%s' - '%s'" % (name, version))
			return

		if not path.isdir("%s/data/indicators/%s" % (config["workdir"], name)):
			mkdir("%s/data/indicators/%s" % (config["workdir"], name))
		indicator = IndicatorModel()
		indicator.name = name
		indicator.version = version
		indicator.create()

		print("Indicator `%s` version `%s` registered" % (name, version))
