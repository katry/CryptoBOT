from ..model import Asset as AssetModel


class Asset:
	@staticmethod
	def list(config, **kwargs):
		assets = AssetModel.query.order_by(AssetModel.id).all()
		print("### Asset list")
		print(" ID | SYMBOL")
		for asset in assets:
			print(" %s - %s" % (asset.id, asset.symbol))

	@staticmethod
	def register(config, symbol: str, **kwargs):
		if AssetModel.query.filter_by(symbol=symbol.upper()).count():
			return print("Error asset `%s` already exists" % symbol)
		asset = AssetModel()
		asset.symbol = symbol.upper()
		asset.create()
		print("Asset `%s` registered" % symbol)
