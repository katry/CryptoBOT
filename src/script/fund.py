from ..model import (
	Asset as AssetModel,
	Exchange as ExchangeModel,
	Fund as FundModel
)


class Fund:
	@staticmethod
	def list(config, **kwargs):
		funds = FundModel.query.order_by(FundModel.id).order_by(FundModel.exchange_id).all()
		print("### Fund list")
		print(" ID - SYMBOL - AMOUNT | EXCHANGE | STATUS")
		for fund in funds:
			enabled = "Enabled" if fund.active else "Disabled"
			print(" %s - %s - %s | %s | %s " % (fund.id, fund.asset.symbol, fund.amount, fund.exchange.name, enabled))

	@staticmethod
	def add(config, symbol, exchange, amount: float, **kwargs):
		asset = AssetModel.query.filter_by(symbol=symbol).one_or_none()
		if not asset:
			print("Error: Symbol `%s` not found" % symbol)
			return
		exchange_model = ExchangeModel.query.filter_by(name=exchange).one_or_none()
		if not exchange_model:
			print("Error: Exchange `%s` not found" % exchange_model)
			return
		if amount <= 0:
			print("Error: Amount must be higher than zero")
			return
		fund = FundModel()
		fund.amount = amount
		fund.exchange_id = exchange_model.id
		fund.asset_id = asset.id
		fund.active = False
		fund.create()

	@staticmethod
	def enable(config, id, **kwargs):
		fund = FundModel.get(id)
		if not fund or fund.active:
			print("Error: Fund %s not disabled" % id)
			return
		fund.active = True
		fund.update()

	@staticmethod
	def disable(config, id, **kwargs):
		fund = FundModel.get(id)
		if not fund or not fund.active:
			print("Error: Fund %s not enabled" % id)
			return
		fund.active = False
		fund.update()
