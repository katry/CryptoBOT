from ..model import (
	Asset as AssetModel,
	Pair as PairModel
)


class Pair:
	@staticmethod
	def list(config, **kwargs):
		pairs = PairModel.query.order_by(PairModel.id).all()
		print("### Pair list")
		print(" ID | SYMBOL | STATUS")
		for pair in pairs:
			print(" %s - %s - %s" % (pair.id, pair.symbol, "Enabled" if pair.active else "Disabled"))

	@staticmethod
	def register(config, base, quote, symbol, **kwargs):
		base_asset = AssetModel.query.filter_by(symbol=base).one_or_none()
		if not base_asset:
			print("Base asset `%s` not found" % base)
			return
		quote_asset = AssetModel.query.filter_by(symbol=quote).one_or_none()
		if not quote_asset:
			print("Quote asset `%s` not found" % quote)
			return
		if PairModel.query.filter_by(symbol=symbol).count():
			print("Pair `%s` already exists" % symbol)
			return
		if (
			PairModel.query
			.filter_by(base_asset_id=base_asset.id)
			.filter_by(quote_asset_id=quote_asset.id)
			.count()
		):
			print("Pair with base asset `%s` and quote asset `%s` already exists" % (base, quote))
			return
		pair = PairModel()
		pair.symbol = symbol
		pair.base_asset_id = base_asset.id
		pair.quote_asset_id = quote_asset.id
		pair.active = False
		pair.create()

	@staticmethod
	def enable(config, symbol, **kwargs):
		pair = PairModel.query.filter_by(symbol=symbol).filter_by(active=False).one_or_none()
		if not pair:
			print("Error: Symbol `%s` is not disabled" % symbol)
			return
		pair.active = True
		pair.update()

	@staticmethod
	def disable(config, symbol, **kwargs):
		pair = PairModel.query.filter_by(symbol=symbol).filter_by(active=True).one_or_none()
		if not pair:
			print("Error: Symbol `%s` is not enabled" % symbol)
			return
		pair.active = False
		pair.update()
