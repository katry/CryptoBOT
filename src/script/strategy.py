from os import path, mkdir
from re import compile
from ..model import (
	Strategy as StrategyModel,
)


class Strategy:
	_name_check = compile("^[a-zA-Z][a-zA-Z0-9_-]+$")
	_version_check = compile(r"^\d+\.\d+\.\d+$")

	@staticmethod
	def list(config, **kwargs):
		strategies = (
			StrategyModel.query
			.order_by(StrategyModel.name)
			.all()
		)
		to_print = {}
		for strategy in strategies:
			if strategy.name not in to_print:
				to_print[strategy.name] = None
			if strategy.active:
				to_print[strategy.name] = strategy.version
		print("### Stategy list")
		for k, v in to_print.items():
			print("%s\n  - Active: %s" % (k, v if v else "-"))

	@staticmethod
	def enable(config, name, version=None, **kwargs):
		old = StrategyModel.query.filter_by(name=name).filter_by(active=True).one_or_none()
		if old:
			old.active = False
			old.update()
		if version:
			strategy = StrategyModel.query.filter_by(name=name).filter_by(version=version).one_or_none()
		else:
			strategy = (
				StrategyModel.query
				.filter_by(name=name)
				.order_by(StrategyModel.version.desc())
				.first()
			)
			active_model = StrategyModel.query.filter_by(name=name).filter_by(active=True).one_or_none()
			if active_model:
				active_model.active = False
				active_model.update()
		if not strategy:
			print("### Error: Strategy `%s` not found (ver. `%s`)" % (name, version or "*"))
		strategy.active = True
		strategy.update()

	@staticmethod
	def disable(config, name, version=None, **kwargs):
		if version:
			strategy = StrategyModel.query.filter_by(name=name).filter_by(version=version).one_or_none()
			if not strategy or not strategy.active:
				print("### Error: Strategy `%s` version `%s` is not active" % (name, version))
				return
		else:
			strategy = (
				StrategyModel.query
				.filter_by(name=name)
				.filter_by(active=True)
				.one_or_none()
			)
			if not strategy:
				print("### Error: Strategy `%s` is not active" % name)
				return
		strategy.active = False
		strategy.update()

	@staticmethod
	def register(config, name, version, **kwargs):
		if not Strategy._name_check.match(name):
			print(
				">>> Invalid name format '%s' should be alphanumeric with possible dash / underscore characters" % name
			)
			print("  > and must start with letter")
			return
		if not Strategy._version_check.match(version):
			print(">>> Invalid version format - '%s' does not match pattern '\\d+\\.\\d+\\.\\d+'" % version)
			return

		pth = "%s/strategies/%s/v%s" % (config["workdir"], name, version.replace(".", "_"))
		if not path.isfile("%s/strategy.py" % pth):
			print(">>> Strategy not found %s %s" % (name, version))
			return
		if (
			StrategyModel.query
			.filter_by(name=name)
			.filter_by(version=version)
			.count()
		):
			print(">>> Strategy already registered '%s' - '%s'" % (name, version))
			return

		if not path.isdir("%s/data/strategies/%s" % (config["workdir"], name)):
			mkdir("%s/data/strategies/%s" % (config["workdir"], name))
		strategy = StrategyModel()
		strategy.name = name
		strategy.version = version
		strategy.active = False
		strategy.priority = 0
		strategy.create()

		print("Strategy `%s` version `%s` created" % (name, version))
