from os import remove, listdir, path, mkdir
from shutil import move, copy


class Backup:
	def __init__(self, source_path, target_path):
		if not target_path:
			mkdir(target_path)
		self.clean(target_path)
		files = [f for f in listdir(source_path) if path.isfile(path.join(source_path, f))]
		for file in files:
			if file.endswith("aggregated.tar.xz"):
				move(source_path + file, target_path + file)
				print("Backup - %s" % file)
			if file.endswith("stream.tar.gz"):
				copy(source_path + file, target_path + file)
				print("Backup - %s" % file)

	def clean(self, target_path):
		files = [f for f in listdir(target_path) if path.isfile(path.join(target_path, f))]
		for file in files:
			if file.endswith("_stream.tar.gz"):
				remove(target_path + file)
