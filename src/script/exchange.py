from orjson import loads
from ..model import Exchange as ExchangeModel


class Exchange:
	@staticmethod
	def list(config, **kwargs):
		exchanges = ExchangeModel.query.order_by(ExchangeModel.id).all()
		print("### Exchange list")
		print(" ID | NAME | STATUS")
		for ex in exchanges:
			print(" %s - %s - %s" % (ex.id, ex.name, "Enabled" if ex.active else "Disabled"))

	@staticmethod
	def set_fee(config, name, fee, **kwargs):
		try:
			fee = float(fee)
			if fee < 0:
				raise Exception()
		except Exception:
			return print("Error: Positional parameter `fee` must be number >= 0")
		exchange = ExchangeModel.query.filter_by(name=name).one_or_none()
		if not exchange:
			return print("Error: Exchange not found `%s" % name)
		exchange.fee = fee
		exchange.update()

	@staticmethod
	def set_api(config, name, api, **kwargs):
		try:
			api = loads(api.encode())
		except Exception:
			return print("Error: Positional parameter `api` must be valid JSON")
		exchange = ExchangeModel.query.filter_by(name=name).one_or_none()
		if not exchange:
			return print("Error: Exchange not found `%s" % name)
		exchange.api = api
		exchange.update()

	@staticmethod
	def enable(config, name, **kwargs):
		exchange = ExchangeModel.query.filter_by(name=name).filter_by(active=False).one_or_none()
		if not exchange:
			return print("Error: Exchange `%s` is not disabled" % name)
		exchange.active = True
		exchange.update()

	@staticmethod
	def disable(config, name, **kwargs):
		exchange = ExchangeModel.query.filter_by(name=name).filter_by(active=True).one_or_none()
		if not exchange:
			return print("Error: Exchange `%s` is not enabled" % name)
		exchange.active = False
		exchange.update()
