from os import listdir, path, remove, mkdir, rmdir, walk, chdir
from shutil import move, copy
import tarfile
import re
from json import loads, dumps


class AggregateArchives:
	def __init__(self, dir):
		self.dir = dir if dir.endswith("/") else dir + "/"
		archives = [f for f in listdir(dir) if path.isfile(path.join(dir, f))]
		archives.sort()
		new_archive_files = []
		for archive in archives:
			if archive.endswith("stream.tar.xz") or archive.endswith("stream.tar.gz"):
				new_archive_files.append(archive)
				if len(new_archive_files) == 12:
					self.aggregate_archive(new_archive_files)
					for old_file in new_archive_files:
						remove(path.join(self.dir, old_file))
					new_archive_files = []

	def aggregate_archive(self, files):
		if not path.exists(self.dir + "temp/"):
			mkdir(self.dir + "temp/")
		regex = re.compile(r"^\d+.*")
		for file in files:
			archive = tarfile.open(self.dir + file, "r:*")
			timestamp = file.split("_")[0]
			if "temp" not in archive.getnames():
				archive.extractall(self.dir + "temp/")
			else:
				archive.extractall(self.dir)
			archive.close()
			for (dir_path, dir_names, file_names2) in walk(self.dir + "temp/"):
				for name in file_names2:
					if not regex.match(name):
						if not path.exists(self.dir + "temp/" + name.split("_")[0]):
							mkdir(self.dir + "temp/" + name.split("_")[0])
						move(self.dir + "temp/" + name, self.dir + "temp/" + name.split("_")[0] + "/" + timestamp + "_" + name)
		self.aggregate(self.dir + "temp/", no_number=True)
		self.json_to_archive(self.dir + "temp/", self.dir)
		for (dir_path, dir_names, file_names) in walk(self.dir + "temp/"):
			d = dir_path if dir_path.endswith("/") else dir_path + "/"
			for file_name in file_names:
				remove(d + file_name)
		for (dir_path, dir_names, file_names) in walk(self.dir + "temp/"):
			d = dir_path if dir_path.endswith("/") else dir_path + "/"
			for dir_name in dir_names:
				rmdir(d + dir_name)
		rmdir(self.dir + "temp/")

	def aggregate(self, dir, no_number=False):
		for (dir_path, dir_names, file_names) in walk(dir):
			for dir_name in dir_names:
				for (d_path, dir_names, file_names) in walk(dir_path + "/" + dir_name):
					file_names.sort()
					files = []
					for file_name in file_names:
						if not file_name.endswith("stream.txt") and not file_name.endswith("stream.json"):
							continue
						file_path = d_path if d_path.endswith("/") else d_path + "/"
						files.append(file_path + file_name)
						data = []
						if len(files) == 12:
							for file in files:
								if file.endswith("stream.txt"):
									data += self.load_txt(file)
								else:
									data += self.load_json(file)
							self.save_json(data, files[-1].replace("stream.", "stream_aggregated."))
							files = []

	def json_to_archive(self, dir, archive_dir):
		paths = []
		if not path.exists("/tmp/temp"):
			mkdir("/tmp/temp")
		for (dir_path, dir_names, file_names) in walk(dir):
			for dir_name in dir_names:
				paths.append(dir_path + dir_name + "/")
		if not paths:
			return
		for (dir_path, dir_names, file_names) in walk(paths[0]):
			for file_name in file_names:
				timestamp = file_name.split("_")[0]
				tar = tarfile.open(archive_dir + timestamp + "_stream_aggregated.tar.xz", "w:xz")
				names = []
				for p in paths:
					symbol = p.split("/")[-2]
					name = timestamp + "_" + symbol + "_stream_aggregated.json"
					if not path.exists(p + name):
						name = timestamp + "_" + symbol + "_binance_stream_aggregated.json"
					if path.exists(p + name):
						copy(p + name, "/tmp/temp/" + symbol + "_stream_aggregated.json")
						names.append(symbol + "_stream_aggregated.json")
				chdir("/tmp/temp")
				for name in names:
					tar.add(name)
				tar.close()
				for (dir_path, dir_names, file_names) in walk("/tmp/temp"):
					for file_name in file_names:
						remove(dir_path + "/" + file_name)
			rmdir("/tmp/temp")

	def load_json(self, file_name):
		data_list = []
		file = open(file_name, "r")
		# print("file:", file_name)
		raw = file.read()
		raw = (
			raw.replace("}{", "},{")
			.replace("][", "],[")
			.replace("}\"", "},\"")
			.replace("]\"", "],\"")
		)
		data = loads(raw)
		for item in data:
			data_list.append(item)
		file.close()
		remove(file_name)
		return data_list

	def save_json(self, data, file_name):
		file_name = file_name.replace(".txt", ".json")
		w = open(file_name, "w")
		w.write(dumps(data))
		w.close()

	def load_txt(self, file_name):
		txt = ""
		file = open(file_name, "r")
		txt += file.read()
		file.close()
		remove(file_name)
		while txt[0] != "{":
			txt = txt[1:]
		return loads("[" + txt + "]")
