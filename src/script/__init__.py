from orjson import loads, dumps
from ..lib.validator import Validator
from ..model import (
	Exchange as ExchangeModel,
	Indicator as IndicatorModel,
	Strategy as StrategyModel,
	Asset as AssetModel,
	Pair as PairModel,
	Fund as FundModel
)
from .unpack import Unpack  # noqa
from .validity import CheckValidity  # noqa
from .aggregate import AggregateArchives  # noqa
from .backup import Backup  # noqa
from .strategy import Strategy  # noqa
from .indicator import Indicator  # noqa
from .asset import Asset  # noqa
from .pair import Pair  # noqa
from .fund import Fund  # noqa
from .exchange import Exchange  # noqa


class Settings:
	__validator: dict = {
		"exchanges": ([list, "optional"], {
			"name": (str, None),
			"external_id": ([int, type(None)], lambda x, *a: x is None or x > 0),
			"fee": (float, None),
			"watch_intervals": (list, None),
			"graph_length": (int, lambda x, *a: x > 0),
			"active": (bool, None),
			"api": ([dict, type(None)], None)
		}),
		"assets": ([list, "optional"], lambda x, *a: len([i for i in x if type(i) == str])),
		"pairs": ([list, "optional"], {
			"base": (str, None),
			"quote": (str, None),
			"symbol": (str, None),
			"active": (bool, None),
		}),
		"funds": ([list, "optional"], {
			"external_id": ([int, type(None)], lambda x, *a: x is None or x > 0),
			"symbol": (str, None),
			"exchange": (str, None),
			"amount": (float, None),
			"locked": (bool, None),
			"active": (bool, None)
		}),
		"indicators": ([list, "optional"], {
			"name": (str, None),
			"external_id": ([int, type(None)], lambda x, *a: x is None or x > 0),
			"version": (str, None)
		}),
		"strategies": ([list, "optional"], {
			"name": (str, None),
			"external_id": ([int, type(None)], lambda x, *a: x is None or x > 0),
			"version": (str, None),
			"active": (bool, None),
			"priority": (int, None)
		}),
	}

	@staticmethod
	def load(config, file):
		try:
			f = open(file, "rb")
			data = loads(f.read())
			f.close()
		except Exception as e:
			return print("Error: %s\n- Failed to load settings file '%s'." % (e, file))

		if not Validator.check(data, Settings.__validator):
			return print("Error: Invalid setting import format.")

		not_implemented_exchanges = []
		for item in data.get("exchanges", []):
			exchange = ExchangeModel.query.filter_by(name=item["name"]).one_or_none()
			if not exchange:
				print("Warning: Exchange not found ``. should resolve this issue.")
				not_implemented_exchanges.append(item["name"])
				continue
			exchange.external_id = item["external_id"]
			exchange.fee = item["fee"]
			exchange.watch_intervals = item["watch_intervals"]
			exchange.graph_length = item["graph_length"]
			exchange.active = item["active"]
			exchange.api = item["api"]
			exchange.update()

		for item in data.get("assets", []):
			if (
				AssetModel.query
				.filter_by(symbol=item)
				.count()
			):
				print("Info: Skipping asset with symbol `%s` - already exists. " % item)
				continue
			asset = AssetModel()
			asset.symbol = item
			asset.create()
		for item in data.get("pairs", []):
			if (
				PairModel.query
				.filter_by(symbol=item["symbol"])
				.count()
			):
				print("Info: Skipping pair with symbol `%s` - already exists. " % item["symbol"])
				continue
			pair = PairModel()
			pair.base_asset_id = AssetModel.query.filter_by(symbol=item["base"]).one().id
			pair.quote_asset_id = AssetModel.query.filter_by(symbol=item["quote"]).one().id
			pair.symbol = item["symbol"]
			pair.active = item["active"]
			pair.create()
		for item in data.get("funds", []):
			if item["exchange"] in not_implemented_exchanges:
				continue
			fund = FundModel()
			fund.external_id = item["external_id"]
			fund.asset_id = AssetModel.query.filter_by(symbol=item["symbol"]).one().id
			fund.exchange_id = ExchangeModel.query.filter_by(name=item["exchange"]).one().id
			fund.amount = item["amount"]
			fund.locked = item["locked"]
			fund.active = item["active"]
			fund.create()
		for item in data.get("indicators", []):
			if (
				IndicatorModel.query
				.filter_by(name=item["name"])
				.filter_by(version=item["version"])
				.count()
			):
				continue
			indicator = IndicatorModel()
			indicator.external_id = item["external_id"]
			indicator.name = item["name"]
			indicator.version = item["version"]
			indicator.create()
		for item in data.get("strategies", []):
			if (
				StrategyModel.query
				.filter_by(name=item["name"])
				.filter_by(version=item["version"])
				.count()
			):
				continue
			strategy = StrategyModel()
			strategy.external_id = item["external_id"]
			strategy.name = item["name"]
			strategy.version = item["version"]
			strategy.active = item["active"]
			strategy.priority = item["priority"]
			strategy.create()
		print("Settings import successfull!")

	@staticmethod
	def export(config, file):
		export = {
			"exchanges": [],
			"indicators": [],
			"strategies": [],
			"assets": [],
			"pairs": [],
			"funds": []
		}
		for exchange in ExchangeModel.query.all():
			export["exchanges"].append({
				"name": exchange.name,
				"external_id": exchange.external_id,
				"fee": exchange.fee,
				"watch_intervals": exchange.watch_intervals,
				"graph_length": exchange.graph_length,
				"active": exchange.active,
				"api": exchange.api,
			})
		for indicator in IndicatorModel.query.all():
			export["indicators"].append({
					"name": indicator.name,
					"external_id": indicator.external_id,
					"version": indicator.version,
				})
		for strategy in StrategyModel.query.all():
			export["strategies"].append({
					"name": strategy.name,
					"external_id": strategy.external_id,
					"version": strategy.version,
					"active": strategy.active,
					"priority": strategy.priority
				})
		for asset in AssetModel.query.all():
			export["assets"].append(asset.symbol)
		for pair in PairModel.query.all():
			export["pairs"].append({
				"base": pair.base_asset.symbol,
				"quote": pair.quote_asset.symbol,
				"symbol": pair.symbol,
				"active": pair.active
			})
		for fund in FundModel.query.all():
			export["funds"].append({
				"external_id": fund.external_id,
				"symbol": fund.asset.symbol,
				"exchange": fund.exchange.name,
				"amount": fund.amount,
				"locked": fund.locked,
				"active": fund.active,
			})
		try:
			f = open(file, "wb")
			f.write(dumps(export))
			f.close()
			print("Settings export successfull!")
		except Exception:
			print("Error: Failed to export settings into file '%s'." % file)
