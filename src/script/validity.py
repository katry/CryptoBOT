from os import path, remove, mkdir, rmdir, listdir as ld, chdir
from orjson import loads, dumps
from datetime import datetime
from urllib.request import Request, urlopen
from time import sleep, time
from . import Unpack
from shutil import copy
import tarfile
import re


class CheckValidity:
	def __init__(self, config, **kwargs):
		self.config = config
		archive_path = kwargs.get("archive-dir", None)
		if archive_path:
			if not archive_path.endswith("packed/") and not archive_path.endswith("backup/"):
				print("ERROR: Archive path must ends with `packed/ or backup/`")
				return
			if archive_path.startswith("/") or archive_path.startswith("~"):
				archive_path = archive_path
			else:
				archive_path = "%s/%s" % (config["workdir"], archive_path)

			if not archive_path.endswith("/"):
				archive_path += "/"
		else:
			archive_path = "%s/data/%s/packed/" % (config["workdir"], kwargs.get("exchange"))
		temp = path.realpath(__file__).replace("src/script/validity.py", "data/temp/")
		if not path.exists(temp):
			mkdir(temp)
		Unpack(
			config, archive_path=archive_path, target_path=temp, skip_symlinks=True,
			move=False, skip_aggregate=True, **kwargs
		)
		repaired = CheckValidity.checker(
			config,
			temp=temp,
			ap=archive_path,
			**{
				"repair": kwargs.get("repair", False),
				"repair_integrity": kwargs.get("repair_integrity", False)
			}
		)
		if kwargs.get("repair") or kwargs.get("repair_integrity"):
			print("Packing repaired archives - %s." % str(repaired))
			self.pack(temp, archive_path, repaired)
		self.clean(temp)

	@staticmethod
	def checker(config, temp=None, ap=None, **kwargs):
		repair = kwargs.get("repair", False)
		repair_integrity = kwargs.get("repair_integrity")
		delta_weeks = kwargs.get("weeks", 0)
		if not kwargs.get("data"):
			base_path = "%s/data/temp/" % config["workdir"]
		else:
			tpl = (config["workdir"], kwargs.get("exchange"), kwargs["data"])
			base_path = "%s/data/%s/%s/" % tpl
		stream_paths = [base_path + f + "/" for f in ld(base_path) if not path.isfile(path.join(base_path, f))]
		repaired_timestamps = []

		for stream_path in stream_paths:
			stream_files = [f for f in ld(stream_path) if path.isfile(path.join(stream_path, f))]
			stream_files.sort()
			before_time = ""
			before_timestamp = 0
			skip_timestamp = False
			for stream_file in stream_files:
				r = re.compile(r"^(\d+).*$")
				if delta_weeks and int(r.match(stream_file).groups()[0]) - round(time()) + (delta_weeks * 3600 * 168) < 0:
					continue
				stream = open(stream_path + stream_file, "rb")
				data = CheckValidity.load_txt_or_json(stream_file, stream.read())
				data_list = []
				repaired = False
				for item in data:
					timestamp = round(item["data"]["k"]["t"] / 1000)
					item_time = str(datetime.fromtimestamp(timestamp))
					if skip_timestamp >= item["data"]["E"] and item["data"]["E"]:
						continue
					else:
						skip_timestamp = False
					if item_time != before_time:
						if before_timestamp and timestamp - before_timestamp != 60:
							print("TIME JUMP -", stream_file.split("_")[1], stream_file)
							if timestamp - before_timestamp < 0:
								print("CRITICAL - TIME INCONSISTENCY!", stream_file)
								repair = False
								if repair_integrity:
									print("repairing inconsitency")
									repaired_timestamps.append(stream_file.split("_")[0])
									repaired = True
									skip_timestamp = item["data"]["E"]
									continue
							elif repair:
								CheckValidity.repair(before_timestamp, timestamp, item, data_list)
								repaired_timestamps.append(stream_file.split("_")[0])
								repaired = True
							print(before_time, " - ", item_time)
						before_timestamp = timestamp
					before_time = item_time
					if repair or repair_integrity:
						data_list.append(item)
				stream.close()
				if repaired:
					print("Writing fixed data %dB -> %dB" % (len(data), len(data_list)))
					stream = open(stream_path + stream_file, "wb")
					stream.write(dumps(data_list))
					stream.close()
					repaired = False
			print("-----------PATH %s DONE - LAST-DATE: %s" % (stream_path.split("/")[-2], before_time))
		return repaired_timestamps

	@staticmethod
	def pack(unp_path, archive_path, repaired):
		stream_paths = [
			unp_path + f + "/" for f in ld(unp_path) if not path.isfile(path.join(unp_path, f))
		]
		tf = "/tmp/temporary_repaired_buffer/"
		mkdir(tf)
		for stream_path in stream_paths:
			stream_files = []
			for f in ld(stream_path):
				if path.isfile(path.join(stream_path, f)) and f.split("_")[0] in repaired:
					stream_files.append(f)

			for stream_file in stream_files:
				stream_dir = tf + stream_file.split("_")[0]
				if not path.exists(stream_dir):
					mkdir(stream_dir)
					mkdir(stream_dir + "/temp")
				copy(stream_path + stream_file, stream_dir + "/temp/" + re.sub(r"\d+_", "", stream_file))

		temp_paths = [tf + f + "/" for f in ld(tf) if not path.isfile(path.join(tf, f))]
		for temp_path in temp_paths:
			chdir(temp_path)
			tar = tarfile.open(archive_path + temp_path.split("/")[-2] + "_stream.tar.gz", "w:gz")
			tar.add("temp")
			tar.close()
			files = [f for f in ld("temp") if path.isfile(path.join("temp", f))]
			for file in files:
				remove(temp_path + "temp/" + file)
			rmdir("temp")
			print("Packing - ", file)
			rmdir(temp_path)
		rmdir(tf)

	def clean(self, base_path):
		stream_paths = [base_path + f + "/" for f in ld(base_path) if not path.isfile(path.join(base_path, f))]

		for stream_path in stream_paths:
			stream_files = [f for f in ld(stream_path) if path.isfile(path.join(stream_path, f))]
			for file in stream_files:
				remove(stream_path + file)
			rmdir(stream_path)
		rmdir(base_path)

	@staticmethod
	def load_txt_or_json(filename, data):
		if filename.endswith(".txt"):
			txt = data
			while txt[0] != b"{":
				txt = txt[1:]
			return loads(b"[%s]" % txt)
		else:
			return loads(data.replace(b"\x00", b"").replace(b"}{", b"},{"))

	@staticmethod
	def repair(before_timestamp, timestamp, item, data_list):
		repaired_timestamp = before_timestamp
		while timestamp - repaired_timestamp != 60:
			limit = int(min(1000, (timestamp - repaired_timestamp + 60) / 60))
			if limit < 1:
				break
			new_data = CheckValidity.get_kline_data(
				repaired_timestamp + 60,
				item["data"]["s"],
				limit=limit
			)
			for i in new_data:
				repaired_timestamp += 60
				if i["t"] != repaired_timestamp * 1000:
					continue
				CheckValidity.update_json(item, i, data_list)
		before_timestamp = repaired_timestamp

	@staticmethod
	def update_json(item, new_data, data_list):
		open_kline = CheckValidity.create_kline(item, new_data)
		open_kline["data"]["k"]["c"] = open_kline["data"]["k"]["o"]
		open_kline["data"]["k"]["h"] = open_kline["data"]["k"]["o"]
		open_kline["data"]["k"]["l"] = open_kline["data"]["k"]["o"]
		close_kline = CheckValidity.create_kline(item, new_data)
		low_kline = CheckValidity.create_kline(item, new_data)
		low_kline["data"]["k"]["c"] = open_kline["data"]["k"]["l"]
		high_kline = CheckValidity.create_kline(item, new_data)
		high_kline["data"]["k"]["c"] = open_kline["data"]["k"]["h"]
		data_list.append(open_kline)
		if float(open_kline["data"]["k"]["o"]) < float(open_kline["data"]["k"]["c"]):
			data_list.append(low_kline)
			data_list.append(high_kline)
		else:
			data_list.append(high_kline)
			data_list.append(low_kline)
		data_list.append(close_kline)

	@staticmethod
	def create_kline(item, new_data):
		return {
			"stream": item["stream"],
			"data": {
				"e": "kline",
				"E": 0,
				"s": item["data"]["s"],
				"k": new_data
			}
		}

	@staticmethod
	def get_kline_data(timestamp, symbol, attempt=0, limit=1):
		time_param = timestamp * 1000
		endpoint = "https://api.binance.com/api/v3/klines?symbol=%s&interval=1m&limit=%s&startTime=%s"
		try:
			req = Request(endpoint % (symbol, limit, time_param))
			with urlopen(req, timeout=1) as response:
				resp = loads(response.read())
		except Exception as e:
			props = (e, timestamp, attempt, symbol)
			print("Repair date exception: %s \n\t- Timestamp: %s | Attempt:  %s | Symbol: %s" % props)
			sleep(1)
			return CheckValidity.get_kline_data(timestamp, symbol, attempt + 1)
		rv = []
		for c in resp:
			rv.append({
				"t": c[0],
				"T": c[6],
				"o": float(c[1]),
				"c": float(c[4]),
				"h": float(c[2]),
				"l": float(c[3]),
				"v": float(c[5]),
				"q": float(c[7]),
				"n": float(c[8]),
				"V": float(c[9]),
				"Q": float(c[10])
			})
		return rv
