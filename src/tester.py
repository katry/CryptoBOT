from asyncio import get_event_loop
from multiprocessing import Process, Queue
from .model import (
	Exchange as ExchangeModel,
	Pair as PairModel,
	Test as TestModel,
	TestStatus as Status
)
from .lib.socketwrap import SocketWrap
from .module.exchanges.testing import TestingExchange
from .module.trader import Trader
from .worker import Worker


class Tester(Worker):
	__loop = None
	__test: TestModel | None = None
	__queue = []

	def __init__(self, config):
		super().__init__(config, autostart=False)
		self.logger.info("READY")

		if test := self.__next_pending_test:
			self.start(test)

	def _trigger(self, data):
		mp = super()._trigger(data, skip_log=True)
		actions = {
			"run": lambda r: self.start(r["test"]) if "test" in r else None,
			"stop": lambda r: self.stop(id=r["id"]) if "id" in r else self.stop(),
			"status": lambda r: self.get_status(r),
			"results": lambda r: self.results(r),
			"slow-down": lambda r: self._exchange.slow_down(r["alias"]),
			"slow-down-cancel": lambda r: self._exchange.speed_up(r["alias"])
		}
		action = actions.get(data["action"])
		if action:
			action(data)
		elif mp == -1:
			self.logger.warning("Invalid trigger operation - %s" % repr(data))

	def start(self, test):
		if self.__test:
			return
		exchange = ExchangeModel.query.filter_by(name="binance").one()
		if not (test := TestModel.check_or_create(test, exchange.id)):
			return
		exchange.api = {}
		self._exchange = TestingExchange(self._config, exchange)
		self.__test = test
		self.send({"action": "test-running", "id": test.id}, target="manager")
		payload = {"data": {"test_id": test.id}}
		self.send({"action": "broadcast", "payload": payload}, target="server")

		self._traders = {}
		pairs = PairModel.query.filter_by(active=True).all()
		for pair in pairs:
			send_queue = Queue()
			self._traders[pair["symbol"]] = {
				"process": Process(target=Trader, args=(
					self._config,
					pair.symbol,
					[self._exchange.id],
					self._queue,
					send_queue,
					test.id
				)),
				"queue": send_queue,
				"ready": False
			}
			self._traders[pair.symbol]["process"].start()

	def stop(self, id=None, finished=False, skip_queue=False):
		if not self.__test or (id and id != self.__test.external_id):
			return
		super().stop()
		self.__test.status = Status.CANCELED
		self.__test.update()
		self.send({"action": "test-stopped", "id": self.__test.id}, target="manager")
		payload = {"data": {"test_id": self.__test.id}}
		self.send({"action": "broadcast", "payload": payload}, target="server")
		if skip_queue:
			self.__test = None
			return
		if test := self.__next_pending_test:
			self.start(test)

	@SocketWrap(response_action="test-status-response", validator={"test_id": (int, None)})
	def get_status(self, data: dict):
		test = TestModel.query.filter_by(id=data["test_id"]).one_or_none()
		if not test:
			return False, "Test with id `%s` not found" % data["test_id"]
		return {"test_status": test.status}

	@SocketWrap(response_action="test-results-response", validator={"test_id": (int, None)})
	def results(self, data: dict):
		test = TestModel.query.filter_by(id=data["test_id"]).one_or_none()
		if not test:
			return False, "Test with id `%s` not found" % data["test_id"]

		response = {
			"test_status": test.status,
			"funds": [],
			"pairs": [],
			"positions": [],
			"strategies": [],
		}
		for fund in test.funds.all():
			response["response"]["funds"].append({
				"id": fund.id,
				"external_id": fund.external_id,
				"asset": fund.asset.symbol,
				"amount": fund.amount,
				"exchange": fund.exchange.name,
				"locked": fund.locked,
			})
		for pair in test.pairs.all():
			response["response"]["pairs"].append({
				"symbol": pair.symbol,
				"base_asset": pair.base_asset.symbol,
				"quote_asset": pair.quote_asset.symbol
			})
		for position in test.positions.all():
			response["response"]["positions"].append({
				"id": position.id,
				"type": position.type,
				"status": position.status,
				"amount": position.amount,
				"open_price": position.open_price,
				"close_price": position.close_price,
				"open_time": position.open_time,
				"close_time": position.close_time,
				"data": position.data,
				"pair": position.pair.symbol,
				"fund_id": position.fund_id,
				"strategy": {
					"id": position.strategy.id,
					"name": position.strategy.name,
					"version": position.strategy.version,
				}
			})
		for strategy in test.strategies.all():
			response["response"]["strategies"].append({
				"id": strategy.id,
				"name": strategy.name,
				"version": strategy.version,
				"priority": strategy.priority
			})
		return response

	def _send_candle(self, candle, symbol):
		if not candle and not symbol:
			self.stop(finished=True)
			return
		super()._send_candle(candle, symbol)

	def _ready(self, data):
		super()._ready(data)
		if (
			len([1 for t in self._traders if t["ready"]]) != len(self._traders.keys()) or
			not self.__test
		):
			return
		self.__test.status = Status.RUNNING
		self.__test.update()
		self.__loop = get_event_loop()
		self.__loop.run_until_complete(
			self._exchange.subscribe(self._traders.keys(), self._send_candle)
		)
		self.__test.status = Status.DONE
		self.__test.update()
		self.__test = None
		if test := self.__next_pending_test:
			self.start(test)

	@property
	def __next_pending_test(self):
		return (
			TestModel.query
			.filter_by(status=Status.PENDING)
			.order_by(TestModel.id)
			.one_or_none()
		)
