# CryptoBOT - project is deprecated

new project group: (WIP) https://gitlab.com/datrool

Programmable cryptocurrency trading bot
<br>

## Installation & initialization

<b>
	Installation will not work - stable version  has not been released yet.<br>
</b>
<i>
	If you want to try functionality just clone this repo 
	and instead of `CryptoBOT` command execute `run.py` script from project root.
</i>
<br><br>

Run installation command (requires python>=3.10):
```sh
$ pip install CryptoBOT
```

Create and directory where bot initialization will be created.
Run interactive initialization script:
```sh
$ CryptoBOT system init --workdir ~/cryptobot_init_directory
```
### Configuration help

#### Service types
- `tester` - instance intended for strategy testing
- `trader` instance intended for trading
- `notifier` - instance intended for sending notifications instead of trading (default)
- `collector` - instance intended for collecting data from exchange used in strategy testing

#### Server
Server can be enabled in tester, trader and notifier services.
Configuration with enabled server must contains secret and client secrets used for securing communication.

#### Schedulers
Schedulers for periodically running jobs. Config contains interval ('hourly', 'daily', 'weekly') and time pattern to match.
##### Scheduler list:
- `archiving` - job for collected data packing

## Updating
Run update script:
```sh
$ CryptoBOT system update --workdir ~/cryptobot_init_directory
```

## Launch
Run main script denpends on service type.
```sh
$ CryptoBOT --workdir ~/cryptobot_init_directory
```
<br>
<br>

## CLI Commands

### Commands for import and export settings:
- `settings-export` - export actual settings to file passed in parameter `${1}`
- `settings-import` - import settings from file passed in parameter `${1}`
<br>

### Commands to manage exchanges:
- `list` - list of exchanges inclues in format ID | NAME | STATUS (no parameters)
- `exchange-enable` - enables exchange by name given in positional parameter `${1}`
- `exchange-disable` - disables exchange by name given in positional parameter `${1}`
- `exchange-disable` - disables exchange by name given in positional parameter `${1}`
- `exchange-set-fee` - set exchange fee - `${1}` is exchange name and `${2}` is fee value (float)
- `exchange-set-api` - set exchange fee - `${1}` is exchange name and `${2}` is json object contains api keys (passed as string)
<br>

### Commands to manage indicators and strategies:
- `indicator-list` - list of indicators includes name and its versions (no parameters)
- `indicator-register` - register indicator saved in workdir indicators given paramaters name in `${1}` and version in `${2}`
- `strategy-list` - list of strategies includes name and possibly active version (no parameters)
- `strategy-register` - register strategy saved in workdir strategies given paramaters name in `${1}` and version in `${2}`
- `strategy-enable` - enable strategy named in `${1}`. Parameter `${2}` may contain version to enable (latest version choosen if not passed)
- `strategy-disable` - strategy named in `${1}`. Parameter `${2}` may contain version to disable (finded automatically if not passed)
<br>

### Commands to manage assets, pair and funds:
- `asset-list` - list of asset symbols includes ID | NAME (no parameters)
- `asset-register` - register asset symbol given in `${1}` parameter
- `pair-list` - list of pairs includes ID | SYMBOL | STATUS (no parameters)
- `pair-register` - register pair of base symbol in `${1}` quote symbol in `${2}` named as symbol in `${3}`
- `pair-enable` - enable disabled pair symbol given in `${1}`
- `pair-disable` - disable enabled pair symbol given in `${1}`
- `fund-list` - list of funds includes ID | SYMBOL | AMOUNT | EXCHANGE (no parameters)
- `fund-add` - create fund of symbol passed as symbol in `${1}` on exchange in passed as name in `${2}` with amount in `${3}`
- `fund-enable` - enable fund with id in `${1}`
- `fund-disable` - disable fund with id in `${1}`
<br>

### Commands to manage collected testing data:
- `check-archives` - check packed collector data validity
- `check-data` - check unpacked collector data validity
- `unpack` - aggregate and unpack collector packed trading data (no parameters)

##### `check-archives` and `check-data` named parameters:
- `repair` - try to repair outages in collected data
- `repair-integrity` - repair time sequence of collected data
- `archive-dir` - parh of collected data archives
- `data` - dataset directory to check
- `weeks` - last <i>n</i> weeks to check validity
<br>
<br>

## Strategy example
Create in strategy file in pattern `~/cryptobot_init_directory/strategies/__name__/__version__.py`. Where `__version__` must be in format like `1.0.0` and file must contain `Strategy` class with classmethods `check_open` and `check_close` to be loaded.
Follow format of example below:
```py
ma = indicator_loader("ma")  # indicator loader is automatically passed to the module global scope


class Strategy:
	_counter = 0

	@classmethod
	def check_open(cls, pair):
		cls._counter += 1
		if cls._counter % 2 == 0:
			return {
				"type": "long",
				"price": 0.121
			}
		else:
			return False

	@classmethod
	def check_close(cls, pair, position):
		cls._counter += 1
		if cls._counter % 2 == 0:
			return {
				"price": 0.131
			}
		else:
			return False
```

Run CLI script:
```sh
$ CryptoBOT strategy-register name 1.0.0  ~/cryptobot_init_directory
```
<br>

## Indicator example
Create in indicator file in pattern `~/cryptobot_init_directory/indicators/__name__/__version__.py`. Where `__version__` must be in format like `1.0.0` and file should contain Indicator callable (class or function) to be loaded, if module does not contain `Indicator` object whole module will be loaded.
```py
ma = indicator_loader("ma")  # indicator loader is automatically passed to the module global scope


def Indicator(graph, periods=[12, 26, 9], length=0, ema=True, source="close"):
	if ema:
		st = ma.ema_list(graph, period=periods[0], length=length, source=source)
		lt = ma.ema_list(graph, period=periods[1], length=length, source=source)
	else:
		st = ma.sma_list(graph, period=periods[0], length=length, source=source)
		lt = ma.sma_list(graph, period=periods[1], length=length, source=source)
	macd = []
	for i in range(len(st)):
		macd.append(st[i] - lt[i])
	return {
		"macd": macd,
		"signal": ma.ema(macd, period=periods[2])
	}
```


Run CLI script:
```sh
$ CryptoBOT indicator-register name 1.0.0 --workdir ~/cryptobot_init_directory
```

## Strategy / indicator modules sandboxing (experimental)
To increase security, loaded modules are slightly limited in functionality:
- can only read / write files in its sandboxed directory
- cannot run new processes
- cannot import following modules: `multiprocessing`, `sys`, `subprocess`, `pip`, `importlib`, `setuptools`, `builtins`
- importing `os` module imports only strictly limited version described below
- json modules are wrapped to work only with sandboxed directory too (json, orjson, ujson)
- any other modules you want to use import be set in `allowed_modules` list in your configuration -  default configuration contains only `urllib` (for api requests)
- two and more dots relative inports are restricted
- all rules above are applied for imported modules recursively

### `os` module
Module contains only limited functions for IO operations in sandboxed directory:
- os.listdir
- os.mkdir
- os.makedirs
- os.rmdir
- os.remove
- os.removedirs
- os.lstat
- os.stat
- os.mknod
- os.path
- os.path.isdir
- os.path.isfile
- os.path.exists

### sandboxing TODO
- implement `secimport` (in revision with merged `**kwargs` passed to native `__import__` functionality)
- testing
- improve functionality
- async execution

<br>
<br>
<br>

Created by Patrik Katreňák and released under General Public License v3.0.
